<?php

/**
 * Template Name: Home Page Template
 * Template Post Type: page
 */

$page_on_front_ID = get_option( 'page_on_front' );


$block1_title = get_post_meta( $page_on_front_ID, 'block1_title', true );
$block1_text  = get_post_meta( $page_on_front_ID, 'block1_text', true );

$block2_title = get_post_meta( $page_on_front_ID, 'block2_title', true );
$block2_text  = get_post_meta( $page_on_front_ID, 'block2_text', true );

$block3_1_title = get_post_meta( $page_on_front_ID, 'block3_1_title', true );
$block3_1_text  = get_post_meta( $page_on_front_ID, 'block3_1_text', true );

$block4_title = get_post_meta( $page_on_front_ID, 'block4_title', true );
$block4_text  = get_post_meta( $page_on_front_ID, 'block4_text', true );
$block4_motto = get_post_meta( $page_on_front_ID, 'block4_motto', true );

get_header();

?>
    <h1 class="main_title"><?php echo get_the_title( $page_on_front_ID ) ?></h1>
    <div class="bl_inovation">
        <div class="bl_inovation__corrected">
            <div class="inovation__texts">
                <p class="bl_inovation__text"><?php echo $block1_title; ?></p>
                <p class="bl_inovation__text"><?php echo $block1_text; ?></p>
            </div>
        </div>

        <picture class="decor_man">
            <source media="(max-width: 340px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/man_hero_min_320.png">
            <source media="(max-width: 600px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/man_hero_380.png">
            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="man">
        </picture>

        <picture class="decor_woman">
            <source media="(max-width: 340px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/woman_hero_min_320.png">
            <source media="(max-width: 600px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/woman_hero_380.png">
            <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="woman">
        </picture>

        <picture class="decor_coins__left-1">
            <source media="(min-width: 1200px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/main-chips-left-1.png">
            <img src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-1.png" alt="coins left first">
        </picture>

        <picture class="decor_coins__left-2">
            <source media="(min-width: 1200px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/main-chips-left-2.png">
            <img src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-2.png" alt="coins left second">
        </picture>

        <picture class="decor_coins__right-1">
            <source media="(min-width: 1200px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/main-chips-right.png">
            <img src="<?php echo get_template_directory_uri() ?>/image/main-chips-right.png" alt="coins right first">
        </picture>

        <div class="mainPage_video">
            <video class="bl_inovation_video" loop muted autoplay
                   poster="<?php echo get_template_directory_uri() ?>/image/poster-netgame.jpg">
                <source src="<?php echo get_template_directory_uri() ?>/image/topbanner.webm" type="video/webm">
                <source src="<?php echo get_template_directory_uri() ?>/image/topbanner.mp4"
                        type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
            </video>
        </div>

    </div>

    <div class="bl_games">
        <div class="container corrected_top">
            <ul class="bl_games__wrapper">
				<?php
				$current_page     = 1;
				$quantity_on_page = 10;
				echo get_header_games( $current_page, $quantity_on_page );
				?>
            </ul>

            <img class="decor_coins__left-3"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-3.png" alt="coin left third">
            <img class="decor_coins__left-4"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-4.png"
                 alt="coin left fourth">
            <img class="decor_coins__left-5"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-5.png" alt="coin left fifth">


            <img class="decor_coins__right-2"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-4.png"
                 alt="coin right second">
            <img class="decor_coins__right-3"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-5.png"
                 alt="coin right third">
            <img class="decor_coins__right-4"
                 src="<?php echo get_template_directory_uri() ?>/image/main-chips-right-4.png"
                 alt="coin right fourth">

			<?php $args_count = array(
				'post_type'      => 'games',
				'posts_per_page' => - 1,
			);

			$query_count = new WP_Query( $args_count );

			if ( $query_count->post_count > $quantity_on_page ) { ?>
                <input type="hidden" id="current-page" value="<?php echo $current_page ?>">
                <input type="hidden" id="quantity-on-page" value="<?php echo $quantity_on_page ?>">
                <button class="btn_lookAtGame btn_lookAtGame__allGames js-more_games" type="button"
                        aria-label="button add new games of company">
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="dot"></span>
                    <span class="btn_lookAtGame__text">More games</span>
                </button>
			<?php }
			wp_reset_query(); ?>
        </div>
    </div>
    <section class="bl_entertainment">
        <h2 class="title_support"><?php echo $block2_title; ?></h2>
        <p class="text_underTitle bl_entertainment__text"><?php echo $block2_text; ?></p>

        <div class="bl_lastGames">
			<?php $query  = new WP_Query(
				[
					'post_type'     => 'games',
					'category_name' => 'latest_games',
					'meta_key'      => 'realeased',
					'orderby'       => 'meta_value_date',
					'order'         => 'DESK'

				]
			);
			$latest_games = [];
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					$latest_games[ get_the_ID() ] = date( 'Y-m-d', strtotime( get_post_custom_values( 'realeased' )[0] ) );
				}
			};
			asort( $latest_games );
			wp_reset_query();
			foreach ( $latest_games as $id => $date ) {
				$game                = get_post( $id );
				$latest_games_img_id = get_post_custom_values( 'home_page_last_game_images', $game->ID )[0];
				$img                 = wp_get_attachment_image_src( $latest_games_img_id, 'full', true )[0];
				$img_webP            = str_replace( array( 'png', 'jpg' ), 'webp', $img );
				if ( ! empty( get_post_custom_values( 'last_short_description', $game->ID )[0] ) ) {
					$short_description = get_post_custom_values( 'last_short_description', $game->ID )[0];
				} else {
					$short_description = 'Дополнительное поле last_short_description';
				}


				?>
                <div class="bl_lastGames__item">
                    <p class="bl_lastGames__name"><?php echo $game->post_title ?></p>
                    <time class="bl_lastGames__time"
                          datetime="<?php echo date( 'Y-m-d', strtotime( $date ) ) ?>"><?php echo date( 'F Y', strtotime( $date ) ) ?></time>

                    <p class="bl_lastGames__shortDescription"><?php echo $short_description ?></p>
                    <picture class="bl_lastGames__img">
						<?php if ( class_exists( '\Netgeme\WebPImg' ) ) {
							echo ( new \Netgeme\WebPImg( $latest_games_img_id ) )->getWeb();
						} ?>
                        <img class="bl_lastGames__img" src="<?php echo $img ?>" alt="<?php echo $game->post_title ?>">
                    </picture>
                </div>
				<?php
			}
			?>

        </div>
    </section>

    <section class="about_games active" id="section-benefits">
        <h2 class="title_support about_games__title">Why NetGame?</h2>
        <div class="container">
            <div class="about_games__full">

                <div class="about_games__level l1">
                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-high-quality"></div>
                        </div>
                        <p class="about_games__name">Constantly evolving High-quality slots</p>
                    </div>
                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-performance"></div>
                        </div>
                        <p class="about_games__name">Astonishing Art quality & Performance</p>
                    </div>
                </div>
                <div class="about_games__level l2">
                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-20games"></div>
                        </div>
                        <p class="about_games__name">20+ new slots/2020</p>
                    </div>

                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-features"></div>
                        </div>
                        <p class="about_games__name">Unique Math & Features</p>
                    </div>

                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-currencies"></div>
                        </div>
                        <p class="about_games__name">All currencies</p>
                    </div>
                </div>
                <div class="about_games__level l3">
                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-customers"></div>
                        </div>
                        <p class="about_games__name">Diversified content to suit all customer tastes</p>
                    </div>

                    <div class="about_games__item">
                        <div class="about_games__rectangle">
                            <div class="about_games__icon icon-traffic"></div>
                        </div>
                        <p class="about_games__name">Works with almost all internet connections</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_sidebar( 'leadform' ); ?>

    <section class="bl_advantages">

        <div class="bl_advantages__textFull">
            <h2 class="title_support bl_advantages__title"><?php echo $block3_1_title; ?></h2>
            <p class="bl_advantages__text"><?php echo $block3_1_text; ?></p>
        </div>

        <picture class="bl_advantages__background">
            <source media="(max-width: 450px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/advantages/background1_min.jpg">
            <source media="(max-width: 768px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/advantages/background1_middle.jpg">
            <source media="(min-width: 1200px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/advantages/background1.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/image/advantages/background1.jpg"
                 alt="advantages_fullScreen">
        </picture>

        <div class="bl_advantages__slot">

            <!--            <video class="bl_advantages__video" loop muted autoplay>-->
            <!--                <source src="-->
			<?php //echo get_template_directory_uri() ?><!--/image/Cloverstones_promo_min.webm" type="video/webm">-->
            <!--                <source src="-->
			<?php //echo get_template_directory_uri() ?><!--/image/Cloverstones_promo_min.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>-->
            <!--            </video>-->

            <picture class="bl_advantages__img">
                <source media="(max-width: 450px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/advantages/game_min.jpg">
                <source media="(max-width: 768px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/advantages/game_middle.jpg">
                <source media="(min-width: 1200px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/advantages/game.jpg">
                <img src="<?php echo get_template_directory_uri() ?>/image/advantages/game.jpg" alt="Advantages image">
            </picture>
        </div>

        <picture class="decor-mooshroom">
            <source media="(min-width: 1200px)" srcset="<?php echo get_template_directory_uri() ?>/image/mooshroom.webp" type="image/webp">
            <img src="<?php echo get_template_directory_uri() ?>/image/mooshroom.png" width="698" height="1174" loading="lazy" alt="mooshroom">
        </picture>

    </section>

    <section class="bl_career">
        <div class="container">
            <h2 class="title_support bl_career__title"><?php echo $block4_title; ?></h2>
            <p class="bl_career__text"><?php echo $block4_text; ?></p>
            <p class="bl_career__motto"><?php echo $block4_motto; ?></p>
            <a class="btn_look btn_look__career" href="/career" aria-label="visit page open vacancies in company">View vacancies</a>

            <picture class="decor_coins__a">
                <source media="(min-width: 1200px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/decor_coins__a.png">
                <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__a.png" alt="decor coins">
            </picture>

            <picture class="decor_coins__a_left2">
                <source media="(min-width: 1200px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/main-chips-left-5.png">
                <img src="<?php echo get_template_directory_uri() ?>/image/main-chips-left-5.png" alt="decor coins">
            </picture>

            <picture class="decor_coins__b">
                <source media="(min-width: 1200px)"
                        srcset="<?php echo get_template_directory_uri() ?>/image/decor_coins__b.png">
                <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__b.png" alt="decor coins">
            </picture>
        </div>
    </section>
<?php $args_count = array(
	'post_type'      => 'partners',
	'posts_per_page' => - 1,
);

$query_partners = new WP_Query( array(
	'post_type'      => 'partners',
	'posts_per_page' => - 1,
) );
if ( $query_partners->have_posts() ) { ?>
    <section class="bl_partners">
        <div class="container">
            <h2 class="title_support bl_partners__title">Our partners</h2>
            <div class="bl_partners__slider">
				<?php while ( $query_partners->have_posts() ) {
					$query_partners->the_post();
					$partner     = get_post();
					$partner_url = ( ! empty( get_post_meta( $partner->ID, 'url_partner', true ) ) ) ? get_post_meta( $partner->ID, 'url_partner', true ) : '#';
					$partner_img = wp_get_attachment_image_src( get_post_thumbnail_id( $partner->ID ), 'full', true )[0]; ?>
                    <button class="bl_partners__item" type="button"
                            data-href="<?= $partner_url ?>" aria-label="<?= $partner->post_title ?>">
                        <img class="bl_partners__image" width="275" height="175" loading="lazy"
                             src="<?=$partner_img?>"
                             alt="<?= $partner->post_name ?>">
                    </button>
				<?php }
				wp_reset_query(); ?>
            </div>
        </div>
    </section>
<?php } ?>
<?php get_sidebar( 'contacts' ); ?>

<?php $front_content = trim( get_post( $page_on_front_ID )->post_content );
if ( ! empty( $front_content ) ) { ?>
    <section class="context_mainPage">
        <div class="bl_ceo context_mainPage__wrapper">
			<?php echo $front_content ?>
        </div>
        <button class="btn_showMoreInfo" type="button"
                aria-label="button show more information about NetGame Entertainment company">See more
        </button>
    </section>
<?php }
get_footer();
