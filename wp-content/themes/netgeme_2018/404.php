<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">

    <title>Netgame Entertainment NV</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/image/favicon/favicon.ico"
          type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() ?>/image/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo get_template_directory_uri() ?>/image/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo get_template_directory_uri() ?>/image/favicon/apple-touch-icon-114x114.png">

    <style>

        @font-face {
            font-family: "Proxima Nova";
            src: url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Regular/Proxima-Nova-Regular.woff2) format("woff2"),
            url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Regular/Proxima-Nova-Regular.woff) format("woff"),
            url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Regular/Proxima-Nova-Regular.ttf) format("truetype");
            font-weight: 400;
            font-style: normal
        }

        @font-face {
            font-family: "Proxima Nova Semi Bold";
            src: url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Semibold/Proxima-Nova-Semibold.woff2) format("woff2"),
            url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Semibold/Proxima-Nova-Semibold.woff) format("woff"),
            url(<?php echo get_template_directory_uri() ?>/fonts/Proxima-Nova/Proxima-Nova-Semibold/Proxima-Nova-Semibold.ttf) format("truetype");
            font-weight: 600;
            font-style: normal
        }

        .body {
            position: relative;
            margin: 0;
            height: 100%;
            width: 100%;
            background-color: #1E1C20;
            color: #fff;
            font-family: "Proxima Nova", sans-serif;
        }

        .df-center {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center;
        }


        .bg_image {

            width: 100%;
            font-size: 0;
        }

        .bg_image img {
            width: 100%;
            object-fit: contain;
        }


        .wrapper {
            position: relative;
            width: 100%;
            height: 100vh;
            overflow: hidden;
        }

        .wrapperBlock {
            position: absolute;
            top: 48%;
            left: 50%;
            transform: translate(-50%, -50%);

            max-width: 380px;

            z-index: 1;
        }


        .text_opss {
            font-weight: 600;
            font-size: 59px;
            margin: 0;
            text-shadow: 0 3px 0 #434343;

            opacity: 0;


        }

        .text_opss {
            animation: 1s animationShowing 1s linear;
            animation-fill-mode: forwards;
        }


        .text_main {
            font-weight: normal;
            font-size: 34px;
            color: #00E676;
            margin: 0;

            opacity: 0;

        }

        .text_main {
            animation: 1s animationShowing 2s linear;
            animation-fill-mode: forwards;
        }


        .text_add {
            max-width: 300px;
            font-size: 16px;
            opacity: 0;

        }

        .text_add {
            animation: 1s animationShowing 3s linear;
            animation-fill-mode: forwards;
        }


        .link_home {
            position: relative;
            margin-top: 26px;
            width: 214px;
            height: 56px;
            background: #00E676;
            border-radius: 44px;
            text-align: center;

            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;

            color: #07070A;
            font-size: 16px;
            font-weight: 600;
            text-decoration: none;
            transition: .1s ease-in;
            z-index: 11;
            opacity: 0;

        }

        .link_home {
            animation: 1s animationShowing 4s linear;
            animation-fill-mode: forwards;
        }


        @keyframes animationShowing {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }


        .link_home:hover,
        .link_home:focus {
            background-color: #66FFA6;
        }


        .decor_gun {
            position: absolute;
            bottom: -3%;
            z-index: 3;
        }

        .decor_gun__right {
            right: 0;

        }

        .decor_gun__left {
            left: -2%;
            bottom: -8%;
            transform: scale(-1, 1);
        }

        .decor_gun__left:before {
            content: "";
            position: absolute;
            top: -5%;
            right: 60%;
            width: 150px;
            height: 214px;

            transform: translate(-50%, -50%);

            background: url("<?php echo get_template_directory_uri() ?>/image/404_page/smoke.png") 50% 50% no-repeat;
            z-index: -1;
        }

        @media only screen and (max-width: 1600px) {

            .text_opss {
                font-size: 50px;
            }

            .text_main {
                font-size: 29px;
            }

            .link_home {
                margin-top: 10px;
                width: 198px;
                height: 48px;
                font-size: 14px;
            }

            .decor_gun {
                bottom: -13%;
            }


        }

        @media only screen and (max-width: 1470px) {

            .decor_gun__left:before {
                top: -18%;
                right: 48%;
            }

        }

        @media only screen and (max-width: 1280px) {

            .wrapperBlock {
                top: 54%;
            }

            .decor_gun {
                bottom: -17%;
            }

            .decor_gun__left:before {
                top: -17%;
                right: 36%;
            }
        }

        @media only screen and (max-width: 991px) {

            .wrapperBlock {
                top: 50%;
            }

        }

        @media only screen and (max-width: 768px) {
            .decor_gun {
                bottom: -12%;
            }

            .decor_gun__left {
                left: -2%;
            }


            .decor_gun__left:before {
                width: 86px;
                height: 85px;
                background: url("<?php echo get_template_directory_uri() ?>/image/404_page/smoke_768.png");

            }

            .decor_gun__left:before {
                top: -8%;
                right: 55%;
            }

            .link_home {
                margin-top: 4px;
                width: 187px;
                height: 48px;
            }

            .text_opss {
                font-size: 44px;
            }

            .text_main {
                font-size: 26px;
            }

        }

        @media only screen and (max-width: 600px) {
            .decor_gun__left {
                transform: scale(-1, 1) rotate(10grad);
                bottom: -13%;
                left: -5%;
            }

            .text_add {
                font-size: 15px;
            }


        }

        @media only screen and (max-width: 480px) {


            .body {
                background-color: #000;
            }

            .decor_gun {
                bottom: -12%;
            }


            .decor_gun__left {
                left: -13%;
            }

            .decor_gun__right {
                right: -12%;
            }

            .df-center {
                justify-content: flex-start;
            }


            .wrapperBlock {
                top: 43%;
            }

        }


        @media only screen and (max-width: 380px) {


            .decor_gun {
                bottom: -2%;
            }

            .decor_gun__left {
                transform: scale(-1, 1) rotate(18grad);
            }

            .decor_gun__right {
                right: -16%;
                transform: rotate(11grad);
            }

            .text_add {
                font-size: 12px;
            }

        }

        @media only screen and (max-width: 360px) {
            .decor_gun {
                bottom: -18%;
            }
        }

    </style>
</head>

<body class="body">



<div class="wrapper df-center">
    <picture class="bg_image">
        <source media="(max-width: 380px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_320.jpg">
        <source media="(max-width: 480px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_480.jpg">
        <source media="(max-width: 600px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_600.jpg">
        <source media="(max-width: 991px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_991.jpg">
        <source media="(max-width: 1280px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_1280.jpg">
        <source media="(max-width: 1470px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404_1470.jpg">
        <source srcset="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404.jpg">
        <img src="<?php echo get_template_directory_uri() ?>/image/404_page/bg_404.jpg"
             alt="backGround-page-404-NetGame-Entertainment">
    </picture>

    <picture class="decor_gun decor_gun__left">
        <source media="(max-width: 768px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_768.png">
        <source media="(max-width: 1280px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_1280.png">
        <source media="(max-width: 1470px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_1470.png">
        <source srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun.png">
        <img src="<?php echo get_template_directory_uri() ?>/image/404_page/gun.png" alt="gun-NetGame-Entertainment">
    </picture>

    <picture class="decor_gun decor_gun__right">
        <source media="(max-width: 768px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_768.png">
        <source media="(max-width: 1280px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_1280.png">
        <source media="(max-width: 1470px)"
                srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun_1470.png">
        <source srcset="<?php echo get_template_directory_uri() ?>/image/404_page/gun.png">
        <img src="<?php echo get_template_directory_uri() ?>/image/404_page/gun.png" alt="gun-NetGame-Entertainment">
    </picture>


    <div class="wrapperBlock df-center">
        <p class="text_opss">OOPS!</p>
        <h1 class="text_main">Error code: 404</h1>
        <p class="text_add">One of our guys made a bad shot and broke this page.</p>
        <a class="link_home" href="/">Back to the safe place</a>
    </div>
</div>

</body>

</html>