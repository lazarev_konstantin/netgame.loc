</main>
</div>

<footer class="footer" roll="contentinfo">
    <?php wp_footer(); ?>
    <script>
        var onloadCallback = function() {
            var divcaptcha = document.getElementsByClassName('g-recaptcha');
            for (var i = 0; i < divcaptcha.length; i++) {
                grecaptcha.render(divcaptcha[i], {
                    'sitekey': '<?php echo SITEKEY_RECAPTCHA ?>'
                });
            }
        };
    </script>
    <script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>

    <?php /*
    <button class="bl_map" type="button" aria-label="button view company location address">
        <picture class="bl_map__image">
            <source media="(max-width: 380px)" srcset="<?php echo get_template_directory_uri() ?>/image/map_380.jpg">
            <source media="(max-width: 450px)" srcset="<?php echo get_template_directory_uri() ?>/image/map_450.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/image/map.jpg" alt="Image NetGame Entertainment map">
        </picture>
        <span class="bl_map__marker"></span>
    </button>
    */?>
    <section class="footer_bottom">
        <div class="bl_businessInfo">
            <a class="bl_businessInfo__logo" href="/" rel="nofollow" aria-label="page NetGame Entertainment">
                <svg class="logo_netgamenv__min" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 290 250"
                     width="118" height="96">

                    <symbol id="netgamenv_logo__min">
                        <path fill="#00ff68"
                              d="M60.7 237h2v-11.8l11.9 12.5v-16.5h-2v11.7l-11.9-12.5V237zM46 237h8.5v-1.8H48v-6h6.4v-1.8H48V223h6.5v-1.8H46V237zm41.4-14.1h-3.8v14h-2v-14h-3.8v-1.8h9.5v1.8h.1zm4.5 14.1h8.5v-1.8h-6.5v-6h6.4v-1.8h-6.4V223h6.5v-1.8h-8.5V237zm23.4-11.4c0 2.3-1.5 4.2-3.8 4.4l5 6.9h-2.4l-4.7-6.7h-.8v6.7h-2V221h2.4c1.4 0 2.9 0 4.2.7 1.4.9 2.1 2.4 2.1 3.9zm-6.1-2.7h-.5v5.7h.6c2 0 4.2-.4 4.2-2.9s-2.4-2.8-4.3-2.8zm16.1 0h3.8v-1.8h-9.5v1.8h3.8v14h2v-14h-.1zm19.2 14.1l-1.8-4h-7.3l-1.8 4h-2.1l7.6-16.5 7.6 16.5h-2.2zm-5.4-12.2l-2.8 6.4h5.6l-2.8-6.4zm13.7-3.7h-2V237h2v-15.9zm7.1 15.9h-2v-16.6l11.9 12.5v-11.7h2v16.5l-11.9-12.5V237zm26.3.3l4.8-10.8 1.9 10.5h2l-3.2-16.5-5.4 12.5-5.4-12.5-3.2 16.5h2l1.9-10.5 4.6 10.8zm23-.3h-8.5v-15.9h8.5v1.8h-6.5v4.4h6.4v1.8h-6.4v6h6.5v1.9zm4.4 0h2v-11.8l11.9 12.5v-16.5h-2v11.7l-11.9-12.5V237zm27.7-14.1h-3.8v14h-2v-14h-3.8v-1.8h9.5v1.8h.1z"/>
                        <path id="NetGame-text__min" class="netgamenv_white"
                              d="M20.6,201.6h4.9v-29.2L55,203.3v-40.8h-4.9v29l-29.5-30.9V201.6z M61.6,201.6h21.1v-4.5
			H66.5v-14.8h15.8v-4.5H66.5v-11h16.2v-4.5H61.6V201.6z M112.9,166.9h-9.3v34.8h-4.9v-34.8h-9.3v-4.5H113L112.9,166.9L112.9,166.9z
			 M137.4,181.8v4.5h10.2c-0.2,6.4-6.8,11.7-12.9,11.7c-8.5,0-15.3-7.8-15.3-16c0-8.8,6.9-15.9,15.7-15.9c4.8,0,9.5,2.5,12.4,6.4
			l3.4-3.3c-3.9-4.8-9.9-7.6-16-7.6c-11.3,0-20.4,9.3-20.4,20.5c0,10.9,8.9,20.3,19.9,20.3s18.6-8.3,18.6-19v-1.6L137.4,181.8
			L137.4,181.8z M188.2,201.6l-4.5-9.8h-18l-4.5,9.8h-5.3l18.8-40.9l18.8,40.9H188.2z M175.6,171.4l-7,15.9h14L175.6,171.4z
			 M219.4,202.4l11.8-26.7h0.1l4.6,25.9h5.1l-8-40.9l-13.5,30.9L206,160.7l-8,40.9h5.1l4.6-25.9h0.1L219.4,202.4z M268.6,201.6
			h-21.1v-39.2h21.1v4.5h-16.2v11h15.8v4.5h-15.8v14.8h16.2V201.6z"/>
                        <g class="netgament_element__bottom">
                            <path class="netgamenv_harlequin" d="M145.7 117.5l25.6 25.9h-51.2l25.6-25.9z"/>
                            <path class="netgamenv_lime" d="M145.7 117.5v25.9h-25.6l25.6-25.9z"/>
                        </g>
                        <symbol id="netgament_element__min">
                            <path class="netgamenv_harlequin" d="M145.1 9L190 54.4l-44.8 45.4-44.9-45.4L145.1 9z"/>
                            <path class="netgamenv_malachite" d="M145.1 54.4H190l-44.8 45.4-44.9-45.4h44.8z"/>
                            <path class="netgamenv_lime" d="M145.1 9v90.8l-44.8-45.4L145.1 9z"/>
                            <path class="netgamenv_mint" d="M124.1 54.4h21v45.4l-44.8-45.4h23.8z"/>
                            <path class="netgamenv_white"
                                  d="M134.1 54.4s3.3-1.1 6.6-4.4c3.3-3.3 4.4-6.6 4.4-6.6s1.2 3.5 4.4 6.6c3.2 3.2 6.6 4.4 6.6 4.4s-3.3.8-6.6 4.4c-3.3 3.6-4.4 6.6-4.4 6.6s-.6-2.2-3.9-6.1c-3.3-3.8-7.1-4.9-7.1-4.9z"/>
                            <path fill="#000"
                                  d="M145.1 25.8l28.6 28.6L145.1 83l-28.6-28.6 28.6-28.6zm0 8.5L125 54.4l20.1 20.1 20.1-20.1-20.1-20.1z"/>
                        </symbol>
                        <use xlink:href="#netgament_element__min" x="0" y="0"/>
                        <use xlink:href="#netgament_element__min" x="101" y="85" transform="scale(0.77)"/>
                        <use xlink:href="#netgament_element__min" x="-13" y="85" transform="scale(0.77)"/>
                    </symbol>
                    <use xlink:href="#netgamenv_logo__min" x="0" y="0"/>
                </svg>
            </a>
            <div class="bl_businessInfo__wrapper">
                <p class="bl_businessInfo__text">For business inquiries</p>
                <a class="bl_businessInfo__text bl_businessInfo__link" href="mailto:<?php echo FOOTER_EMAIL ?>" aria-label="send message on NetGame Entertainment"><?php echo FOOTER_EMAIL ?></a>
                <p class="bl_businessInfo__text">All rights reserved</p>
                <p class="bl_businessInfo__text">© 2015-<?php echo date('Y') ?></p>
                <a class="bl_businessInfo__text bl_businessInfo__link" rel="nofollow" target="_blank" href="/privacy-policy" aria-label="Visit page Privacy Policy of NetGame Entertainment">Privacy Policy</a>
                <a class="bl_businessInfo__text bl_businessInfo__link" rel="nofollow"  href="/responsible-gaming" aria-label="Responsible gaming in NetGame Entertainment">Responsible gaming</a>
            </div>
        </div>

        <div class="bl_copyright" >
            <h2 class="bl_copyright__title">ALL RIGHTS RESERVED</h2>
            <p class="bl_copyright__text">netgame logos and graphic material is the company's intellectual property and
                may not be copied, reproduced, distributed or displayed without the written consent of netgame
                Entertainment.</p>
        </div>

        <div class="bl_corporation">
            <div class="bl_soc">
                <a class="bl_soc__item icon-fb" target="_blank" rel="noreferrer" href="<?php echo FB_HREF ?>" aria-label="Visit page NetGame Entertainment in Facebook"></a>
                <a class="bl_soc__item icon-tw" target="_blank" rel="noreferrer" href="<?php echo TW_HREF ?>" aria-label="Visit page NetGame Entertainment in Twitter"></a>
                <a class="bl_soc__item icon-linkedIn" target="_blank" rel="noreferrer" href="<?php echo LINKED_HREF ?>" aria-label="Visit page NetGame Entertainment in LinkedIn"></a>
                <a class="bl_soc__item icon-inst" target="_blank" rel="noreferrer" href="<?php echo INST_HREF ?>" aria-label="Visit page NetGame Entertainment in Instagram"></a>
                <a class="bl_soc__item icon-youtube" target="_blank" rel="noreferrer" href="<?php echo YOUYUBE_HREF ?>" aria-label="Visit page NetGame Entertainment in Youtube"></a>
            </div>
            <div class="bl_ourLicense">
                <div class="curacao-license">
                    <div id="apg-39dca2f8-e06a-4176-bfaf-4308b18e042d"
                         data-apg-seal-id="39dca2f8-e06a-4176-bfaf-4308b18e042d" data-apg-image-size="128"
                         data-apg-image-type="basic-small"></div>
                    <script type="text/javascript"
                            src="https://39dca2f8-e06a-4176-bfaf-4308b18e042d.snippet.antillephone.com/apg-seal.js"></script>
                </div>
                <div class="icon-gameLab"></div>
            </div>
        </div>


    </section>


</footer>
</div>


<div class="mobileWrapper"></div>

<!--POPUP-->
<div class="bl_popup hidden">
    <button class="btn_close__iframe hidden" type="button" aria-label="button close iframe"></button>
    <div class="bl_popup__wrapper">

    </div>
</div>


<?php

$cookiesClass = '';

if (empty($_COOKIE['CookiesConfirm'])) {
    $cookiesClass = 'cookiesNotConfirmedYet'
    ?>

    <!--    COOKIES POLITIC  -->
    <div class="politic_ofCookies <?php echo $cookiesClass ?>">
        <div class="politic_ofCookies__wrapper">
            <p class="politic_ofCookies__text">NetGame Entertainment uses cookies to ensure that we give you the best
                experience on our website.
                Using this website means you are agree with <a href="/privacy-policy" rel="nofollow" target="_blank"  aria-label="Visit page Privacy Policy of NetGame Entertainment">Privacy Policy</a>.</p>
            <div class="politic_ofCookies__btn-wrapper">
                <button class="politic_ofCookies__close" aria-label="Button close popup"></button>
                <button class="politic_ofCookies__btn js-cookies" type="button" aria-label="Button I agree with NetGame Entertainment uses cookies Privacy Policy">I agree</button>
            </div>
        </div>
    </div>
    <!--    COOKIES POLITIC END  -->
<?php } ?>



</body>

</html>

