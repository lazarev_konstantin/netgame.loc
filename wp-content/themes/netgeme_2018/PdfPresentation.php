<?php


namespace Netgeme;


use Exception;

class PdfPresentation {

	/**
	 * @var string
	 */
	protected $tableName = 'wp_pdf_presentation';

	/**
	 * @return string
	 */
	public function getTableName() {
		return $this->tableName;
	}

	/**
	 * @var string
	 */
	protected $pdfFile;
	/**
	 * @var string
	 */
	protected $email;
	/**
	 * @var string
	 */
	protected $hash = null;

	/**
	 * @var string
	 */
	protected $link;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $pageForDownloads = '/pdf';

	/**
	 * @var int
	 */
	protected $canDownloads = 3;


	/**
	 * PdfPresentation constructor.
	 *
	 * @param string $pdfFileInThemeFolder
	 *
	 * @throws Exception
	 */
	public function __construct( $pdfFileInThemeFolder = 'pdf/Presentation.pdf' ) {
		if ( file_exists( get_stylesheet_directory() . '/' . $pdfFileInThemeFolder ) ) {
			$this->pdfFile = get_stylesheet_directory() . '/' . $pdfFileInThemeFolder;
		} else {
			throw new Exception( 'Pdf file presentation don\'t find!' );
		}

	}

	/**
	 * @param string $email
	 *
	 * @return PdfPresentation
	 * @throws Exception
	 */
	public function setEmail( $email ) {
		if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			$this->email = $email;
		} else {
			throw new Exception( 'Email ' . $this->email . ' incorrect!' );
		}

		return $this;
	}

	/**
	 * @param string $name
	 *
	 * @return PdfPresentation
	 */
	public function setName( $name ) {
		$this->name = (string) trim( $name );
		$this->name = preg_replace( '/[^a-zA-Zа-яА-ЯёЁ\s\-\x{4e00}-\x{9fa5}]/u', '', $this->name );

		return $this;
	}

	/**
	 * @throws Exception
	 */
	protected function saveContact() {
		global $wpdb;
		$user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $this->tableName WHERE mail = %s ", $this->email
		), OBJECT );
		if ( ! is_null( $user ) ) {
			throw new Exception( 'Email ' . $this->email . ' already exists' );
		}
		if ( empty( $this->email ) ) {
			throw new Exception( 'Email can\'t be empty' );
		}
		$this->generatingLink();
		$wpdb->flush();
		$newUser = $wpdb->insert(
			$this->tableName,
			array(
				'mail' => $this->email,
				'name' => $this->name,
				'ip'   => $_SERVER['REMOTE_ADDR'],
				'hash' => $this->hash
			),
			array( '%s', '%s', '%s', '%s' )
		);
		if ( ! $newUser ) {
			error_log( $wpdb->last_error );
			throw new Exception( 'Save error' );
		}
	}

	/**
	 * @throws Exception
	 */
	public function sendMail() {
		if ( empty( $this->email ) ) {
			throw new Exception( 'Email can\'t be empty' );
		}
		try {
			$this->saveContact();
		} catch ( Exception $exception ) {
			throw new Exception( $exception->getMessage() );
		}
		$PHPMailer = new \PHPMailer\PHPMailer\PHPMailer( true );
		try {
			$PHPMailer->CharSet = 'utf-8';
			$PHPMailer->isSMTP();
			$PHPMailer->SMTPDebug  = 0;
			$PHPMailer->Host       = SMTP_HOST;
			$PHPMailer->Username   = GENERAL_EMAIL_LOGIN;
			$PHPMailer->Password   = GENERAL_EMAIL_PAS;
			$PHPMailer->Port       = SMTP_PORT;
			$PHPMailer->SMTPSecure = SMTP_SECURE;
			$PHPMailer->SMTPAuth   = true;
			//Recipients
			$PHPMailer->setFrom( GENERAL_EMAIL, 'Support NetgameNV' );
			$PHPMailer->addAddress( $this->email, $this->name );
			$PHPMailer->addReplyTo( get_option( 'pdf_email', 'office@netgamenv.com' ), "Presentation" );
			//Content
			$PHPMailer->isHTML( true );
			$PHPMailer->Subject = 'Presentation from ' . get_site_url();
			if ( file_exists( __DIR__ . '/PDF-mail-html.php' ) ) {
				$welcomeText = ( ! empty( $this->name ) ) ? 'Hi, ' . $this->name . '!' : 'Hello!';
				$link        = $this->link;
				require_once __DIR__ . '/PDF-mail-html.php';
			} else {
				$PHPMailer->Body = ( ! empty( $this->name ) ) ? 'Hello, ' . $this->name . '!' : 'Hello!';
				$PHPMailer->Body .= '<br><a href="' . $this->link . '" target="_blank">Your presentation link</a>';
			}

			$PHPMailer->send();

		} catch ( \PHPMailer\PHPMailer\Exception $exception ) {
			throw new Exception( $exception->getMessage() );
		}
	}

	/**
	 * @param bool $reset_counter
	 *
	 * @throws Exception
	 */
	public function reSandEmail( $reset_counter = true ) {
		if ( ! is_user_logged_in() ) {
			throw new Exception( 'Access denied' );
		}
		if ( $reset_counter ) {
			$this->resetCounterDownloads();
		}
		global $wpdb;
		$user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $this->tableName WHERE mail = %s ", $this->email
		), OBJECT );
		if ( is_null( $user ) ) {
			throw new Exception( 'User don\'t find' );
		}
		$this->name = $user->name;
		$this->generatingLink( null, $user->hash );
		$this->sendMail();
	}

	/**
	 * @return $this
	 * @throws Exception
	 */
	protected function resetCounterDownloads() {
		global $wpdb;
		$user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $this->tableName WHERE mail = %s ", $this->email
		), OBJECT );
		if ( is_null( $user ) ) {
			throw new Exception( 'User don\'t find' );
		}
		$user_update = $wpdb->update( $this->tableName,
			array( 'count_download' => 0 ),
			array( 'id' => $user->id )
		);
		if ( $user_update === false ) {
			error_log( $wpdb->last_error );
			throw new Exception( 'Error update' );
		}

		return $this;
	}

	/**
	 * @param string $page
	 *
	 * @return string
	 */
	protected function generatingLink( $page = null, $hash = null ) {
		if ( ! empty( $hash ) ) {
			$this->hash = $hash;
		} else {
			$this->hash = $this->makeHash();
		}
		if ( ! empty( $page ) ) {
			$this->pageForDownloads = $page;
		}
		$this->link = get_home_url() . $this->pageForDownloads . '?key=' . $this->hash;

		return $this->link;
	}

	/**
	 * @param $hash string
	 *
	 * @return string
	 * @throws Exception
	 */
	public function getFile( $hash ) {
		if ( is_user_logged_in() ) {
			return $this->pdfFile;
		}
		global $wpdb;
		$wpdb->flush();
		$user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $this->tableName WHERE hash = %s ", $hash
		), OBJECT );
		if ( is_null( $user ) ) {
			throw new Exception( 'User dont find with hash ' . $hash );
		} else {
			if ( $user->count_download > ( $this->canDownloads - 1 ) ) {
				throw new Exception( 'Key used more then ' . $this->canDownloads . ' times' );
			}
			$count = ++ $user->count_download;

			$wpdb->update( $this->tableName,
				array( 'count_download' => $count ),
				array( 'id' => $user->id )
			);
		}

		return $this->pdfFile;
	}

	/**
	 * @param int $length
	 *
	 * @return string
	 */
	protected function makeHash( $length = 50 ) {
		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$numChars        = strlen( $permitted_chars );
		$hash            = '';
		for ( $i = 0; $i < $length; $i ++ ) {
			$hash .= substr( $permitted_chars, rand( 1, $numChars ) - 1, 1 );
		}

		return $hash;
	}


}