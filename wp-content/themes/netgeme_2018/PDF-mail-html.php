<?php

$email= get_option( 'pdf_email', 'office@netgamenv.com' );
/**
 * @var $welcomeText string
 * @var $link string
 * @var $PHPMailer \PHPMailer\PHPMailer\PHPMailer
 */
if(!isset($welcomeText)){
	$welcomeText = 'Hello!';
}
if(!isset($link)){
	$link = '#!';
}
$PHPMailer->Body ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Netgame Entertainment Presentation</title>
  <!-- <style> -->
</head>

<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background-image: url("'. get_template_directory_uri() .'/image/Netgame_Presentation/White-background-transparent.png"); background-position-x: 50%; background-position-y: 100%; background-repeat: no-repeat; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%; border: none; box-sizing: border-box; color: #000; font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; min-height: 100%; min-width: 100%;">
  <style>
       
    @media only screen and (max-width: 596px) {
      .hide-for-large {
        display: block !important;
        width: auto !important;
        overflow: visible !important;
        max-height: none !important;
        font-size: inherit !important;
        line-height: inherit !important;
      }
    }
    
    @media only screen and (max-width: 596px) {
      table.body table.container .hide-for-large,
      table.body table.container .row.hide-for-large {
        display: table !important;
        width: 100% !important;
      }
    }
    
    @media only screen and (max-width: 596px) {
      table.body table.container .callout-inner.hide-for-large {
        display: table-cell !important;
        width: 100% !important;
      }
    }
    
    @media only screen and (max-width: 596px) {
      table.body table.container .show-for-large {
        display: none !important;
        width: 0;
        mso-hide: all;
        overflow: hidden;
      }
    }
    
    @media only screen and (max-width: 596px) {
      table.body img {
        width: auto;
        height: auto;
      }
      table.body center {
        min-width: 0 !important;
      }
      table.body .container {
        width: 95% !important;
      }
      table.body .columns,
      table.body .column {
        height: auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 16px !important;
        padding-right: 16px !important;
      }
      table.body .columns .column,
      table.body .columns .columns,
      table.body .column .column,
      table.body .column .columns {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      table.body .collapse .columns,
      table.body .collapse .column {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
   
      td.small-6,
      th.small-6 {
        display: inline-block !important;
        width: 50% !important;
      }
    }
     
    
    @media only screen and (max-width: 596px) {
      .text_main {
        max-width: 360px;
      }
      .text {
        margin-bottom: 0;
      }
      table.body {
        background-image: none;
      }
      .text_bottom {
        font-size: 11px;
        line-height: 16px;
      }
      .image_logoN {
        padding-left: 20%;
        padding-right: 28px;
      }
      .licence_icons {
        display: none;
      }
    }
    
    @media only screen and (max-width: 450px) {
      .image_logoN {
        padding-left: 10%;
      }
      .banner_desc {
        display: none;
      }
      .banner_mobile_450 {
        display: block;
      }
    }
    
    @media only screen and (max-width: 380px) {
      .text_main {
        font-size: 19px;
      }
      .image_logoN {
        padding-left: 5%;
        padding-right: 10px;
      }
    }
    
    @media only screen and (max-width: 340px) {
      .banner_mobile_450 {
        display: none;
      }
      .banner_mobile_320 {
        display: block;
      }
    }
  </style>
  <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden;"></span>
  <table class="body" style="Margin: 0; margin: 0 auto;  background-color:#e4ecf3; ">
    <tbody>
      <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
          <center data-parsed="" style="min-width: 580px; width: 100%;">

            <table align="center" class="container all_mail float-center" style="Margin: 0 auto; background: #fff; background-color: #ffffff; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: center; vertical-align: top; width: 740px;">
              <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                    <table class="wrapper header" align="center" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">

                            <table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                              <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                  <th class="small-12 large-12 columns first last" valign="middle" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0; padding-right: 0; text-align: left; width: 588px;">
                                    <table style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                      <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                          <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">
                                            <img class="banner_desc" alt="Netgame Entertainment Presentation" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/top/netgame.jpg" height="332px" style="-ms-interpolation-mode: bicubic; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto; ">
                                        
                                          </th>
                                          <th class="expander" style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>

                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td height="26px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 26px; font-weight: normal; hyphens: auto; line-height: 26px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>

                    <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">


                        </tr>
                      </tbody>
                    </table>
                    <table class="wrapper main" align="center" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; max-width: 580px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <center data-parsed="" style="min-width: 580px; width: 100%;">
                              <h1 class="text_main" style="Margin: 0; Margin-bottom: 10px; color: #07070A; font-family: Helvetica, Arial, sans-serif; font-size: 20px; font-weight: 900; letter-spacing: -0.02em; line-height: 32px; margin: 0; margin-bottom: 10px; padding: 0; text-align: center; text-transform: uppercase; word-wrap: normal;">Netgame Entertainment - what is next..</h1>
                              <p class="text" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 15px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">'.$welcomeText.'</p>
                              <p class="text" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 15px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">Great! Since you’ve got this email, you wanted to know more about who we are and what our product is. To make a long story short, please find an attached link to our presentation. Do not hesitate to contact us if you have
                                any questions or want to have a word with us about the partnership matters.</p>

                              <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                  <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td height="12px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 12px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                              <p class="text text_bold" style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 26px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">Netgame Team wishes you to keep calm and stay on top of our news!</p>
                              <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                  <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td height="40px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 40px; font-weight: normal; hyphens: auto; line-height: 40px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>

                            </center>

                          </td>
                        </tr>
                      </tbody>
                    </table>                   
                    <center data-parsed="" style="min-width: 580px; width: 100%; border=0;">
                      <img alt="Netgame Entertainment game" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/games/netgame.games.png" height="181px" style="-ms-interpolation-mode: bicubic; clear: both; display: block; width: 100%; outline: none; text-decoration: none; border: none;">
                    </center>
                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td height="36px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 36px; font-weight: normal; hyphens: auto; line-height: 36px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>
                    <a class="button_netgame" href="'.  $link.'" style="Margin: 0; color: #2199e8; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none;">
                      <center data-parsed="" style="min-width: 580px; width: 100%; border=0;">
                        <img alt="button get Netgame Entertainment Presentation" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/button.png" align="center" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;">
                      </center>
                    </a>
                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td height="40px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 40px; font-weight: normal; hyphens: auto; line-height: 40px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>

                    <table roll="footer" class="wrapper secondary" align="center" style="background-color: #010101; border-collapse: collapse; border-spacing: 0; color: #ffffff; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                      <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                              <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                  <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                                </tr>
                              </tbody>
                            </table>
                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                              <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                  <th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 274px;">
                                    <table style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                      <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                          <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">
                                            <a href="https://www.netgamenv.com/" target="_blank" style="Margin: 0; color: #2199e8; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none;"><img class="image_logoN" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/Netgame_logo.png" alt="Netgame Entertainment logo" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 25px; text-decoration: none; width: auto;"></a>
                                            <p class="text_bottom text_bottom__1" style="Margin: 0; Margin-bottom: 10px; align-items: center; color: #FFFFFF; font-family: Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; line-height: 24px; margin: 0; margin-bottom: 10px; padding: 0; padding-top: 16px; text-align: left; text-transform: uppercase;">For business inquiries</p>
                                            <p class="text_bottom" style="Margin: 0; Margin-bottom: 10px; align-items: center; color: #FFFFFF; font-family: Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal; line-height: 24px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; text-transform: uppercase;"><a class="link_bottom" href="mailto:'.  $email.'" style="Margin: 0; color: #00E77D; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none;">'.  $email.'</a></p>
                                          </th>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                  <th class="licence_icons small-hidden large-6 columns last" align="center" style="Margin: 0 auto; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 274px;">
                                    <table style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                      <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                          <th style="Margin: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left;">
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                              <tbody>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                  <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 0 none; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <img class="image_license" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/basic-large-valid-seal.png" alt="Netgame Entertainment License" style="-ms-interpolation-mode: bicubic; clear: both; display: inline-block; max-width: 100%; outline: none; padding-left: 140px; padding-right: 16px; text-decoration: none; width: auto;">
                                            <span class="decor_line" style="background: #FFFFFF; display: inline-block; height: 47px; opacity: 0.3; width: 1px;"></span>
                                            <img class="image_gli" src="'. get_template_directory_uri() .'/image/Netgame_Presentation/GLI.png" alt="Gaming Labs Certified logo" style="-ms-interpolation-mode: bicubic; clear: both; display: inline-block; max-width: 100%; outline: none; padding-left: 18px; text-decoration: none; width: auto;">
                                          </th>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </center>
        </td>
      </tr>
    </tbody>
  </table>



  <!-- prevent Gmail on iOS font size manipulation -->
  <div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>

</body>

</html>';

