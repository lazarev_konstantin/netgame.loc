jQuery(function ($) {

    var body = $('body');


    // id блока в ктором находиться весь HTML       additional_game_images


    /*
     * действие при нажатии на кнопку загрузки изображения
     * вы также можете привязать это действие к клику по самому изображению
     */
    body.on('click', '.upload_image_button', function () {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this),
            curImg = button.parent(".group_buttons").siblings('.js-game_img'),
            attachment_id = button.parent(".group_buttons").siblings(".attachment_id");
        wp.media.editor.send.attachment = function (props, attachment) {
            curImg.attr("src", attachment.url);
            attachment_id.val(attachment.id);

            // $(button).parent().prev().attr('src', attachment.url);
            // $('#IdAdditionalImage').val(attachment.id);
            // $('#SizeAdditionalImage').html('Размеры изображения: '+ attachment.width + 'X' + attachment.height +' px');
            // $('#AddAdditionalImage').html('Для вставки в код использывать GetAdditionalImage($id_post) где $id_post id поста');
            // wp.media.editor.send.attachment = send_attachment_bkp;
        };
        wp.media.editor.open(button);
        return false;
    });

    /*
     * удаляем значение произвольного поля
     * если быть точным, то мы просто удаляем value у input type="hidden"
     */

    body.on('click', '.remove_image_button', function () {


        // НАДО НЕ УДАЛЯТЬ ОДИН ДИВ ДЛЯ СЛАЙДЕРА, все остальные можно удалять

        var button = $(this),
            curImg = button.parent(".group_buttons").siblings(".js-game_img"),
            attachment_id = button.parent(".group_buttons").siblings(".attachment_id"),
            no_image_src = $('#no_image_src').val(),
            parent = button.parent(".group_buttons").parent(),
            slider_game_quantity = $("#additional_game_images").find(".slider_game").length;
        var r = confirm("Уверены?");
        if (r === true) {
            curImg.attr('src', no_image_src);
            attachment_id.val('');
            if (parent.hasClass('slider_game')) {
                if (slider_game_quantity > 1) {
                    parent.remove();
                }
            }
        }
        return false;
    });

    $('.slider_game_add').click(function () {
        $(".slider_game:last").after($(".slider_game:last").clone());
        $(".slider_game:last").find('.attachment_id').val('');
        $(".slider_game:last").find('.js-game_img').attr('src', $('#no_image_src').val());
    });

    $('.resend_pdf').on("click", function () {
        var mail = $(this).data('mail');
        var reset = $(this).data('reset');
        var fd = new FormData;
        var bat = $(this)
        fd.append('action', 'resend_pdf_presentation');
        fd.append('mail', mail);
        fd.append('reset', reset);
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            dataType: 'json',
            data: fd,
            processData: false,
            contentType: false,
            method: 'post',
            success: function (response) {
                if (response.success) {
                    alert(response.html)
                } else {
                    alert(response.error)
                }
            },
            error: function (r) {
                alert(r);
            },
            complete: function () {
                bat.remove()
            }
        });
    });

    $('.copy-mails-pdf-button').on("click", function () {
        var copyText = document.getElementById("copy-mails-pdf");
        copyText.select();
        document.execCommand("Copy");
        alert('Скопировано в буфер');
    });
});