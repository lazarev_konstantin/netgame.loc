
$(document).ready(function () {


    var ajaxscript = {ajax_url: '/wp-admin/admin-ajax.php'};


    ///// Contact  Form
    var body = $("body"),
        active = "active",
        hidden = "hidden",
        btnCloseForm = $(".btn_closeComplete"),
        timeShowErrorMessage = 4500,
        errorClassEffect = "error_fild",
        statusContactForm = false,
        statusName = false,
        statusEmail = false,
        statusTel = false,
        statusMessage = false,
        statusCV = false;


    var errorMessage = {
        shortName: "Very short name <br> please enter your full name",
        emptyName: "You must write name",
        longName: "Too long name <br> please enter your real name",
        emptyFild: "This field must be filled",
        emptyEmail: "You must write email",
        errorEmail: "Invalid email",
        errorNumber: "invalid phone number <br> write only numbers",
        errorMessage: "please give a more detailed answer",
        errorLink: "it's not a link to portfolio"
    };


    body.on("click", ".btn_closeComplete", function () {
        $(".bl_complete").remove();
    });


    /// checked corrected name

    $(".contactForm_name").on("blur", function () {
        var fild = $(this),
            valueLength = fild.val().length;

        if (valueLength === 0) {
            showFildError(fild, errorMessage.emptyFild);
        } else if (valueLength < 2) {
            showFildError(fild, errorMessage.shortName);
        } else if (valueLength > 40) {
            showFildError(fild, errorMessage.longName);

            setTimeout(function () {
                fild.val('');
            }, 3000);

        } else {
            statusName = true;
            return statusName;
        }

        setTimeout(function () {
            clearFildError(fild)
        }, 3000);
    });

    /// checked corrected email

    $(".contactForm_email").on("blur", function () {
        var fild = $(this),
            fildValue = fild.val(),
            valueLength = fild.val().length;
        var checkReg = isValidEmailAddress(fildValue);

        if (valueLength === 0) {
            showFildError(fild, errorMessage.emptyFild)
        } else if (checkReg !== true) {
            showFildError(fild, errorMessage.errorEmail);

        } else {
            statusEmail = true;
            return statusEmail;
        }
        setTimeout(function () {
            clearFildError(fild)
        }, 3000);
    });

    //// checked corrected message

    $(".contactForm_message").on("blur", function () {
        var fild = $(this),
            valueLength = fild.val().length;

        if (valueLength === 0) {
            showFildError(fild, errorMessage.emptyFild);
        } else if (valueLength < 4) {
            showFildError(fild, errorMessage.errorMessage);
        } else {
            statusMessage = true;
            return statusMessage;
        }
        setTimeout(function () {
            clearFildError(fild)
        }, 3000);

    });

    //// checked corrected phone number

    $(".contactForm_tel").bind("change keyup input click", function () {
        if (this.value.match(/[^0-9+#*]/g)) {
            this.value = this.value.replace(/[^0-9+#*]/g, '');
        }

    });

    $(".contactForm_tel").on("blur", function () {

        var fild = $(this),
            value = fild.val(),
            valueLength = value.length;


        var checkReg = isValidTelephone(value);

        if (valueLength === 0) {
            showFildError(fild, errorMessage.emptyFild);
        } else if (checkReg === false) {
            showFildError(fild, errorMessage.errorNumber);
        } else {
            statusTel = true;
            return statusTel;
        }

        setTimeout(function () {
            clearFildError(fild)
        }, 3000);

    });


    function isValidTelephone(phoneNumber) {
        /// регулярка только на цифры и спец знаков
        var pattern = new RegExp(/[\d+#*]/);
        return pattern.test(phoneNumber);
    }

    //////// checked CV LINK

    $(".contactForm_cvLink").on("blur", function () {
        var fild = $(this),
            value = fild.val(),
            valueLength = value.length;
        if (valueLength > 120) {
            showFildError(fild, errorMessage.longName);

            setTimeout(function () {
                fild.val('');
            }, 3000);

        } else {
            statusCV = true;
            return statusCV;
        }

        setTimeout(function () {
            clearFildError(fild)
        }, 3000);

    });


    ////// checked file
    var btnLoadCV = $(".btn_resetCV");

    $(".contactForm_cvLoad").change(function () {
        var fileName = $(this).val();
        fileName = fileName.split(/\\+/);


        btnLoadCV.addClass(active).text(fileName[fileName.length - 1]);
    });


    btnLoadCV.on("click", function () {
        $(".contactForm_cvLoad").val("");
        $(this).removeClass(active);
    });


    ///////// SUBMIT FORM


    // $(".contactForm_full").submit(function (e) {
    //
    //     e.preventDefault();
    //
    //     // check if the input is valid
    //
    //
    //     // FOR formForBusiness
    //     if ($(this).hasClass("js-formForBusiness")) {
    //         if ((statusName === true) && (statusEmail === true) && (statusMessage === true)) status = true;
    //         return status;
    //     }
    //
    //     /// ТУТ идет отправка формы
    //     // if (status === true) {
    //     //     $(".contactForm_full").submit();
    //     // }
    //
    //
    // });

    function showFildError(fild, errorText) {
        fild.addClass(errorClassEffect);
        fild.siblings(".text_error").addClass(active).html(errorText);
    }


    function clearFildError(fild) {
        fild.removeClass(errorClassEffect);
        fild.siblings(".text_error").removeClass(active).html("");
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(
            /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
        );
        return pattern.test(emailAddress);
    }



    function checkEmptyInput(errorField, errorText) {
        errorField.addClass("active").text(errorText);

        setTimeout(function () {
            errorField.removeClass("active");
        }, 3500);
    }



    //////////////


    $('.js-submit-contacts-form-button').click(function () {

        var data = $('#contacts_form').serialize();
        data = data + '&action=contacts_form';
        $.ajax({
            url: ajaxscript.ajax_url,
            dataType: 'json',
            data: data,
            method: 'post',
            beforeSend: function () {
                $('.js-sucsessContactForm').empty();
                $('.contacts_form_result').empty();
            },
            success: function (response) {
                if (response.success) {

                    var complete_html = '      <div class="bl_complete">           <button class="btn_closeComplete" type="button"></button>' +
                        '                <div class="bl_complete__wrapper">' +
                        '                    <p class="bl_complete__text1">your message sent</p>' +
                        '                    <p class="bl_complete__text2">successfully!</p>' +
                        '                    <p class="bl_complete__text3">We will contact you as soon as possible</p>' +
                        '                </div></div>';
                    $('.js-sucsessContactForm').html(complete_html);
                    grecaptcha.reset();
                } else {
                    grecaptcha.reset();
                    $('.contacts_form_result').html(response.error);
                }
            },
            error: function (r) {
                $('.contacts_form_result').html('Something wrong');
            },
            complete: function () {
                setTimeout(function () {
                    $('.js-sucsessContactForm').empty();
                    $('.contacts_form_result').empty();
                    $("#contacts_form")[0].reset();
                    grecaptcha.reset();
                }, 5000);
            }

        });
    });

    $('.sendingCV_form_button').click(function () {

        var data = $('#sendingCV_form').serializeArray();
        var fd = new FormData;
        var portfolio_CV = $("#portfolio_CV").prop('files')[0];
        $.each(data, function () {
            fd.append(this.name, this.value);
        });
        fd.append('portfolio_CV', portfolio_CV);
        fd.append('action', 'sendingCV_form');
        $.ajax({
            url: ajaxscript.ajax_url,
            dataType: 'json',
            data: fd,
            processData: false,
            contentType: false,
            method: 'post',
            beforeSend: function () {
                $('.js-sucsessContactForm').empty();
                $('.sendingCV_form_result').empty();
            },
            success: function (response) {
                if (response.success) {
                    var complete_html = '      <div class="bl_complete">           <button class="btn_closeComplete" type="button"></button>' +
                        '                <div class="bl_complete__wrapper">' +
                        '                    <p class="bl_complete__text1">your message sent</p>' +
                        '                    <p class="bl_complete__text2">successfully!</p>' +
                        '                    <p class="bl_complete__text3">will will contact you as soon as possible</p>' +
                        '                </div></div>';
                    $('.js-sucsessContactForm').html(complete_html);
                    // $('.sendingCV_form_result').html(response.html);
                    grecaptcha.reset();
                } else {
                    grecaptcha.reset();
                    $('.sendingCV_form_result').html(response.error);
                }
            },
            error: function (r) {
                $('.sendingCV_form_result').html('Something wrong');
            },
            complete: function () {
                setTimeout(function () {
                    $('.sendingCV_form_result').empty();
                    $('.js-sucsessContactForm').empty();
                    $("#sendingCV_form")[0].reset();
                    grecaptcha.reset();
                }, 5000);
            }

        });
    });




    // LEAD FORM SEND


    $(".btn_showLeadForm").on("click", function () {
        $(".bl_leadForm").addClass(active);
    });

    $(".btn_close__leadForm").on("click", function () {
        $(".popup_leadForm").addClass(hidden);
        $(".bl_leadForm").removeClass(active);
    });


    $(".js-leadForm").on("submit", function (e) {

        e.preventDefault();

        var errorField;

        if ($(this).find(".contactForm_name").val().length === 0) {

            errorField = $(this).find(".contactForm_name").next();

            checkEmptyInput(errorField, errorMessage.emptyName);

            return false
        }

        if ($(this).find(".contactForm_email").val().length === 0) {

            errorField = $(this).find(".contactForm_email").next();

            checkEmptyInput(errorField, errorMessage.emptyEmail);

            return false
        }


        var popupLeadForm = $(".popup_leadForm");
        var animationBlock = $(".popup_leadForm__animation");


        var data = $('#lead_form').serializeArray();
        var fd = new FormData;
        $.each(data, function () {
            fd.append(this.name, this.value);
        });
        fd.append('action', 'pdf_presentation_form');
        $.ajax({
            url: ajaxscript.ajax_url,
            dataType: 'json',
            data: fd,
            processData: false,
            contentType: false,
            method: 'post',
            beforeSend: function () {
            },
            success: function (response) {
                if (response.success) {
                    var userName = $(".js-leadForm .contactForm_name").val();
                    var complete_html = '<div class="popup_leadForm__wrapper popup_success ">\n' +
                        '            <p class="popup_leadForm__phrase"><span>Thanks</span><span class="popup_leadForm__userName">' + userName + '</span></p>\n' +
                        '            <p class="popup_leadForm__text">Сatch your personal presentation in a few minutes.</p>\n' +
                        '        </div>';

                    $(".popup_leadForm").removeClass('hidden');
                    popupLeadForm.append(complete_html);

                    // $('.js-sucsessContactForm').html(complete_html); /// Зачем это ?
                    grecaptcha.reset();
                } else {
                    var errorText = response.error;
                    animationBlock.addClass("error_form");
                    var complete_html = '<div class="popup_leadForm__wrapper popup_error">\n' +
                        '            <p class="popup_leadForm__phrase">Oops..</p>\n' +
                        '            <p class="popup_leadForm__text">Sorry, there is a technical hiccup due to a server connect issue. Keep calm & Please try again later.</p>\n' +
                        '        </div>';


                    $(".popup_leadForm").removeClass('hidden');
                    popupLeadForm.append(complete_html);


                    console.log(errorText);
                    grecaptcha.reset();

                }
            },
            error: function () {

            },
            complete: function () {
                setTimeout(function () {
                    grecaptcha.reset();
                    animationBlock.removeClass("error_form");
                    $(".popup_leadForm__wrapper").remove();
                    popupLeadForm.addClass("hidden");
                }, 7000);
            }

        });


    });



});

