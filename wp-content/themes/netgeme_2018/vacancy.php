<?php
/**
 * Template Name: Vacancy Template Page
 * Template Post Type: page
 */
get_header();
$vacancy_page = get_post();
$my_query_args = array(
    'post_type' => 'vacancy',
    'posts_per_page' => -1
);
$pageCareer_title = get_post_custom_values('pageCareer_title')[0];
$pageCareer_text = get_post_custom_values('pageCareer_text')[0];
$my_query = new WP_Query($my_query_args); ?>

    <div class="top_element top_career">
        <h1 class="title_customPage"><?php echo $pageCareer_title ?></h1>
        <p class="career_motto"><?php echo $pageCareer_text ?></p>
        <picture class="mainImage_career">
            <source media="(max-width: 450px)" srcset="<?php echo get_template_directory_uri() ?>/image/career/careerTop_full_min.webp" type="image/webp">
            <source media="(max-width: 450px)" srcset="<?php echo get_template_directory_uri() ?>/image/career/careerTop_full_min.jpg">
            <source srcset="<?php echo get_template_directory_uri() ?>/image/career/careerTop_full.webp" type="image/webp">
            <img src="<?php echo get_template_directory_uri() ?>/image/career/careerTop_full.jpg"  alt="NetGame Entertainment career picture">
        </picture>
    </div>
    <picture class="decor_coins__a">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__a.png" alt="decor coins">
    </picture>

    <picture class="decor_coins__b">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__b_min.png" alt="decor coins">
    </picture>


    <div class="bl_careerSearch">
        <div class="container">
            <h2 class="bl_careerSearch__title">We are looking for</h2>
            <ul class="bl_careerSearch__full">
                <?php
                if ($my_query->have_posts()) {
                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        $ID = get_the_ID();
                        $post = get_post($ID);
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($ID), 'thumbnail-size', true)[0];
                        $content = strip_tags($post->post_content);
                        $rez_content = mb_substr($content, 0, 115);
                        $rez_content .= ' ...';
                        if (!empty(get_post_custom_values('city_job', $ID)[0])) {
                            $city = get_post_custom_values('city_job', $ID)[0];
                        } else {
                            $city = 'Дополнительное поле city_job';
                        }
                        if (!empty(get_post_custom_values('short_description', $ID)[0])) {
                            $short_description = get_post_custom_values('short_description', $ID)[0];
                        } else {
                            $short_description = 'Дополнительное поле short_description';
                        }
                        ?>
                        <li class="bl_careerSearch__item">
                            <div class="bl_careerSearch__desc">
                                <p class="bl_careerSearch__name"><?php echo $post->post_title; ?></p>
                                <p class="bl_careerSearch__city"><?php echo $city ?></p>
                                <p class="bl_careerSearch__shortDesc"><?php echo $short_description ?></p>
                            </div>
                            <a class="bl_careerSearch__link" href="<?php echo get_permalink() ?>">More info</a>
                        </li>
                        <?php
                    }
                }
                wp_reset_query();
                ?>
            </ul>
            <div class="bl_careerSearch__add">
                <h2 class="bl_careerSearch__topic">Are you proficient in something else?</h2>
                <p class="bl_careerSearch__text">If you don’t see the perfect fit here and still want to let us know about your unique skill set- let us know. Send your CV to <a href="mailto:<?php echo HR_EMAIL ?>"><?php echo HR_EMAIL ?></a> and we’ll review it to see if we can find something for you</p>
                <?php /* echo $vacancy_page->post_content */ ?>
            </div>
        </div>
    </div>
<?php get_sidebar('contacts'); ?>

<?php get_footer();

