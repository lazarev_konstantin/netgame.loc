<?php
// Вставлять в нижнюю часть footer.php  Если нужен баннер, об конференции
$cookiesInvitationActive = '';

if (!empty($_COOKIE['CookiesInvitationBanner'])) {

    $cookiesInvitationActive = 'active';
} ?>
<!-- Invitation Banner-->
<div class="invitation_banner <?php echo $cookiesInvitationActive ?> ">
    <button class="btn_invitation_banner <?php echo $cookiesInvitationActive ?>" type="button"></button>
    <button class="btn_close close_invitation_banner" type="button"></button>

    <a class="invitation_banner__link <?php echo $cookiesInvitationActive ?> " href="<?php echo get_home_url(); ?>/#contactForm">
        <picture class="invitation_banner__picture invitation_banner__picture_big ">
            <source media="(max-width: 768px)" srcset="<?php echo get_template_directory_uri(); ?>/image/invitation/invitation_banner__min.png">
            <source media="(min-width: 769px)" srcset="<?php echo get_template_directory_uri(); ?>/image/invitation/invitation_banner.png">
            <img src="<?php echo get_template_directory_uri(); ?>/image/invitation/invitation_banner.png"
                 alt="invitation banner">
        </picture>

        <picture class="invitation_banner__picture invitation_banner__picture_min active">
            <source srcset="<?php echo get_template_directory_uri(); ?>/image/invitation/invitation_banner__min.png">
            <img src="<?php echo get_template_directory_uri(); ?>/image/invitation/invitation_banner__min.png"
                 alt="invitation banner">
        </picture>

    </a>
</div>