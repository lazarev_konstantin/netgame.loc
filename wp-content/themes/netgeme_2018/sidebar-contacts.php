<section id="contactForm" class="form_contact">
    <h2 class="title_support form_contact__title">Contact us</h2>
    <div class="form_contact__full">
        <div class="form_contact__blur"></div>
        <div class="form_contact__wrapper">
            <div class="form_contact__left">
                <p class="form_contact__phrase">Let's do amazing things together!</p>
                <p class="form_contact__desc">Please drop us a line so we can discuss partnership opportunities.</p>

                <div class="form_contact__info">
                    <p class="form_contact__feedback icon-mail"><a href="mailto:<?php echo GENERAL_EMAIL ?>"><?php echo GENERAL_EMAIL ?></a></p>
                    <p class="form_contact__feedback icon-skype"><a href="skype:<?php echo GENERAL_SKYPE ?>?chat"><?php echo GENERAL_SKYPE ?></a></p>
                    <p class="form_contact__feedback icon-phone"><a href="tel:<?php echo GENERAL_PHONE ?>"><?php echo GENERAL_PHONE ?></a></p>
                    <p class="form_contact__feedback no-icon">Office hours: 10:00 - 19:00 GMT +03:00</p>
                </div>
            </div>
            <div class="form_contact__right">
                <p class="form_contact__phrase">Get in touch</p>
                <form class="contactForm_full" method="post" id="contacts_form">
                    <label class="contactForm_label">
                        <input class="contactForm_name" type="text" name="name" id="name" title="Name"
                               placeholder="name">
                        <span class="text_error"></span>
                    </label>
                    <label class="contactForm_label">
                        <input class="contactForm_email" type="email" name="mail" title="Email"
                               placeholder="email">
                        <span class="text_error"></span>
                    </label>
                    <label class="contactForm_label bl_textarea">
                            <textarea class="contactForm_message" name="massage" id="massage"
                                      placeholder="message"
                                      title="Email"></textarea>
                        <span class="text_error"></span>
                    </label>
                    <div class="contactForm_security">
                        <div class="bl_capcha">
                            <div class="g-recaptcha"></div>
                        </div>
                        <button class="contactForm_submit js-submit-contacts-form-button" type="button">Send message
                        </button>
                    </div>
                </form>
                <div class="contacts_form_result"></div>
                <div class="js-sucsessContactForm"></div>
            </div>
        </div>
    </div>
</section>
<div class="toContactForm">
    <div class="toContactForm_leprechaun"></div>
    <button class="toContactForm_button" type="button" aria-label="button connect with NetGame Entertainment company">
        <span>C</span><span>o</span><span>n</span><span>t</span><span>a</span><span>c</span><span>t</span><span>&#32;</span><span>U</span><span>s</span>
    </button>
</div>