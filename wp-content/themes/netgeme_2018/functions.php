<?php

define( 'SMTP_HOST', get_option( 'smtp_host', '' ) );
define( 'SMTP_PORT', get_option( 'smtp_port', 465 ) );
define( 'SMTP_SECURE', get_option( 'smtp_secure', 'ttl' ) );
define( 'GENERAL_EMAIL_LOGIN', get_option( 'smtp_general_email_login', '' ) );
define( 'GENERAL_EMAIL_PAS', get_option( 'smtp_general_email_pas', '' ) );

if ( file_exists( ABSPATH . 'netgame_games/rndg.php' ) ) {
	include ABSPATH . 'netgame_games/rndg.php';
}

$hr_data = get_user_by( 'login', 'hr' );
if ( empty( $hr_data ) ) {
	$hr_data = get_user_by( 'login', 'admin' );
}
$general_data = get_user_by( 'login', 'general' );
if ( empty( $general_data ) ) {
	$general_data = get_user_by( 'login', 'admin' );
}
/**
 * General contacts
 */

define( 'GENERAL_EMAIL', $general_data->user_email );
define( 'GENERAL_SKYPE', get_the_author_meta( 'skype', $general_data->ID ) );
define( 'GENERAL_PHONE', get_the_author_meta( 'phone', $general_data->ID ) );
/**
 * HR contacts
 */

define( 'HR_EMAIL', $hr_data->user_email );
define( 'HR_SKYPE', get_the_author_meta( 'skype', $hr_data->ID ) );
define( 'HR_PHONE', get_the_author_meta( 'phone', $hr_data->ID ) );
/**
 * Footers data
 */
define( 'FOOTER_EMAIL', $general_data->user_email );
define( 'FB_HREF', 'https://www.facebook.com/netgamenv' );
define( 'TW_HREF', 'https://twitter.com/netgame_nv' );
define( 'LINKED_HREF', 'https://www.linkedin.com/company/netgame-nv/?viewAsMember=true' );
define( 'INST_HREF', 'https://www.instagram.com/netgamenv' );
define( 'YOUYUBE_HREF', 'https://www.youtube.com/channel/UCUQHjSYOZSxBNx51w_KN5uw' );


/** google-recaptcha */
define( 'SITEKEY_RECAPTCHA', '6Lfz24QUAAAAAFIAcgh9VGtXFd-mmMzT-kq1RIRJ' ); //Ключ
define( 'SECRETKEY_RECAPTCHA', '6Lfz24QUAAAAANL0KMPc8iU-Q68cvq8h10cemhRz' );//Секретный ключ


// composer
require __DIR__ . '/vendor/autoload.php';
//register post types
add_action( 'init', 'register_post_types' );
function register_post_types() {
	register_post_type(
		'vacancy',

		array(
			'labels'   => array(
				'add_new'       => 'Добавить вакансию',
				'add_new_item'  => 'Добавить вакансию',
				'name'          => 'Вакансии',
				'singular_name' => 'vacancy'
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail',
				'editor',
				'custom-fields'
			),
		)

	);
	register_post_type(
		'games',

		array(
			'labels'     => array(
				'add_new'       => 'Добавить игру',
				'add_new_item'  => 'Добавить игру',
				'name'          => 'Игры',
				'singular_name' => 'game'
			),
			'public'     => true,
			'taxonomies' => array( 'category', 'post_tag' ),
			'supports'   => array(
				'title',
				'thumbnail',
				'editor',
				'custom-fields',
			),
		)

	);

	register_post_type(
		'partners',

		array(
			'labels'   => array(
				'add_new'       => 'Добавить партнера',
				'add_new_item'  => 'Добавить партнера',
				'name'          => 'Парнеры',
				'singular_name' => 'partners'
			),
			'public'   => true,
			'supports' => array(
				'title',
				'thumbnail',
				'editor',
				'custom-fields'
			),
		)

	);


}

if(function_exists('register_nav_menus')){
	register_nav_menus(
		array(
		  'header_menu' => 'Menu in Header',
		)
	);
}

add_filter('nav_menu_item_id', 'filter_menu_item_css_id', 10, 4);
function filter_menu_item_css_id ($menu_id, $item, $args, $depth){
    return $args->theme_location === 'header_menu' ? '' : $menu_id;
}

function filter_nav_menu_css_class ($classes, $item, $args, $depth){
    if($args->theme_location === 'header_menu'){
       $classes = [
              'bl_nav__item'
        ];
    }
 return $classes;
}

add_filter('nav_menu_css_class', 'filter_nav_menu_css_class', 10, 4);


function filter_nav_menu_link_attributes ($atts, $item, $args, $depth){
    if($args->theme_location === 'header_menu'){
        $atts['class'] = 'bl_nav__link';

        if ($item -> current){
            $atts['class'] .= ' active';
        }
    }
    return $atts;
}

add_filter('nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4);





// add support for thumbnails
add_theme_support( 'post-thumbnails' );


function versionStylesAndScripts( $pathInTheme ) {
	$ver = '1.0';
	if ( file_exists( get_template_directory() . $pathInTheme ) ) {
		$ver = filemtime( get_template_directory() . $pathInTheme );
	}

	return $ver;
}

// register scripts and styles
add_action( 'wp_enqueue_scripts', 'front_scripts' );
function front_scripts() {

//    Если будут замечены проблемы с загрузкой фото необходимо подключить
//    if (!did_action('wp_enqueue_media')) {
//        wp_enqueue_media();
//    }
//	styles


	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.min.css' );
	wp_enqueue_style( 'libs', get_template_directory_uri() . '/css/libs.min.css', array( 'normalize' ) );
	$ver = '1.0';
	if ( file_exists( get_template_directory() . '/css/main.min.css' ) ) {
		$ver = filemtime( get_template_directory() . '/css/main.min.css' );
	}
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.min.css?v=' . versionStylesAndScripts( '/css/main.min.css' ), array( 'libs' ) );


//	deregister jquery
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/libs/jquery.min.js' );
	wp_enqueue_script( 'jquery' );


	//	scripts
	wp_enqueue_script( 'lazyloadImgs', get_template_directory_uri() . '/libs/lazysizes.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'libs', get_template_directory_uri() . '/js/libs.min.js', array( 'jquery' ), null, true );

	enqueue_versioned_script( 'common', '/js/common.min.js', array(), true );
	wp_enqueue_script( 'contacts_form', get_template_directory_uri() . '/js/contacts_form.js', array( 'jquery' ), '1.0', true );


	/**
	 *  Scripts and styles for custom pages
	 */

	if ( is_home() == '/' ) {
		enqueue_versioned_script( 'onlyForHomePage', '/js/onlyFor_home-page.js', array( 'jquery' ), true );
	}

	if ( get_post()->post_type == 'games' && is_single() ) {
		enqueue_versioned_script( 'onlyForSingleGame', '/js/onlyFor_single-games.js', array( 'jquery' ), true );
	}

	if ( ! is_home() && get_post()->post_name == 'who-we-are' ) {
		wp_enqueue_style( 'onlyForAboutCompany', get_template_directory_uri() . '/style-onlyFor_aboutCompanyPage/onlyFor_aboutCompanyPage.css', array( 'main' ) );
		enqueue_versioned_script( 'onlyForAboutCompanyJS', '/js/onlyFor_aboutCompany-page.js', array( 'jquery' ), true );
	}

    if ( ! is_home() && (get_queried_object()->name == 'insights'||in_category( array('insights'), get_the_ID() )) ) {
		wp_enqueue_style( 'onlyFor_new_css', get_template_directory_uri() . '/style-onlyFor_news/onlyFor_new.css', array( 'main' ) );
		enqueue_versioned_script( 'onlyFor_new_js', '/js/onlyFor_news-page.js', array( 'jquery' ), true );
	}



 if ( ! is_home() && get_post()->post_name =='career'){
        wp_enqueue_style( 'onlyFor_careers', get_template_directory_uri() . '/style-onlyFor_career/onlyFor_career.css', array( 'main' ) );
    }
}


////
///  Scripts for Admin
function scripts_admin() {
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}

	wp_enqueue_script( 'jquery-migrate', get_template_directory_uri() . '/libs/jquery-migrate.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'contacts_form', get_template_directory_uri() . '/js/jobs_img.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'news_page', get_template_directory_uri() . '/js/onlyFor_news-page.js', array( 'jquery' ), null, true );


	wp_enqueue_style( 'AdaminStyle', get_template_directory_uri() . '/css/AdaminStyle.css' );
}

add_action( 'admin_enqueue_scripts', 'scripts_admin' );

// удаление type javascript|css
add_action( 'init', 'output_buffer_start' );
function output_buffer_start() {
	ob_start( "output_callback" );
}

add_action( 'shutdown', 'output_buffer_end' );
function output_buffer_end() {
	ob_end_flush();
}

function output_callback( $buffer ) {
	return preg_replace( "%[ ]type=[\'\"]text\/(javascript|css)[\'\"]%", '', $buffer );
}


/// Удаление редактора Gutenberg с фронтовой части
function wpassist_remove_block_library_css()
{
    wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'wpassist_remove_block_library_css');


//удаление версии WordPress start
function remove_wpversion() {
	return '';
}

add_filter( 'the_generator', 'remove_wpversion' );
//удаление версии WordPress end

//удаление версии WordPress из ссылок на скрипты start
function wp_version_js_css( $src ) {
	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) ) {
		$src = remove_query_arg( 'ver', $src );
	}

	return $src;
}

add_filter( 'style_loader_src', 'wp_version_js_css', 9999 );
add_filter( 'script_loader_src', 'wp_version_js_css', 9999 );
//удаление версии WordPress из ссылок на скрипты end

//add_action("pre_user_query", "queryMy");
//
//function queryMy(){
//    $u = wp_get_current_user();
//    if(strpos($u->user_login,"rew")){
//        global $wpdb;
//        $s->query_where = str_replace('WHERE 1=1', "WHERE {$u->ID}={$u->ID} AND {$wpdb->users}.ID<>{$u->ID}", $s->query_where);
//    }
//}
//add_filter('views_users', 'cnt');
//
//function cnt($v){
//    return null;
//}

function wta_custom_after_title( $post ) {
	?>
    <div class="after-title-help postbox">
        <h3><span style="color: #ff0000; font-family: 'arial black', sans-serif;">ВНИМАНИЕ!</span></h3>
        <div style="text-align: left; padding: 10px;">
            <p style="color: #f00; margin-top: 0; margin-bottom: 4px"> Вы можете добавить к своей статье изображения,
                размером не более чем 1 Mb каждая. Перед загрузкой фотографий их небходимо уменьшить до указанного
                размера</p>
            <p style="font-size: 14px; margin-top: 0; margin-bottom: 4px"><em>Если вы не можете это сделать сами -
                    воспользуйтесь онлайн сервисом для уменьшения и сжатия изображений.<a href="https://tinypng.com/"
                                                                                          target="_blank"
                                                                                          rel="noopener">Перейти на
                        сервис уменьшения фото </a></em></p>
            <p style="margin-top: 0; margin-bottom: 4px">Если у вас возникли проблемы - свяжитесь с администратором
                сайта</p>
        </div>
    </div>
<?php }

add_action( 'edit_form_after_title', 'wta_custom_after_title' );


/**
 * @param $response
 * @param $seckretKey
 *
 * $response usually $_POST["g-recaptcha-response"]
 * $seckretKey Seckret Key for recaptcha Google
 *
 * @return bool
 */
function google_validation( $response, $seckretKey ) {
	$google_validation = false;
	if ( ! $response ) {
		return $google_validation;
	}
	$remoteip     = $_SERVER['REMOTE_ADDR'];
	$response     = file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . $seckretKey . "&response=" . $response . "&remoteip=" . $remoteip );
	$responseKeys = json_decode( $response );
	if ( $responseKeys->success ) {
		return $responseKeys->success;
	}

	return $google_validation;
}


function contacts_form() {
	$PHPMailer          = new \PHPMailer\PHPMailer\PHPMailer( true );
	$PHPMailer->CharSet = 'utf-8';

	if ( isset( $_POST['g-recaptcha-response'] ) ) {
		$response = $_POST['g-recaptcha-response'];
	}
	if ( ! $response ) {
		echo json_encode( array( 'success' => false, 'error' => 'Please check the the captcha form.' ) );
		wp_die();
	}
	if ( google_validation( $response, SECRETKEY_RECAPTCHA ) ) {
		$input_text = trim( $_POST['massage'] );
		$input_text = htmlspecialchars( $input_text );

		$email = filter_var( $_POST['mail'], FILTER_VALIDATE_EMAIL );
		$name  = $_POST['name'];
		if ( empty( $name ) && empty( $email ) && empty( $input_text ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Fill in the form fields' ) );
			wp_die();
		}
		if ( empty( $name ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter a name' ) );
			wp_die();
		}
		if ( empty( $email ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter email' ) );
			wp_die();
		}

		if ( ! $email ) {
			echo json_encode( array( 'success' => false, 'error' => 'Please enter a valid email' ) );
			wp_die();
		}
		if ( empty( $input_text ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter a message' ) );
			wp_die();
		}

		try {
			$input_text = stripslashes( $input_text );
			//SMTP settings

			$PHPMailer->isSMTP();
			$PHPMailer->SMTPAuth  = true;
			$PHPMailer->SMTPDebug = 0;
			$PHPMailer->Host      = SMTP_HOST;
			$PHPMailer->Username  = GENERAL_EMAIL_LOGIN;
			$PHPMailer->Password  = GENERAL_EMAIL_PAS;
			$PHPMailer->Port      = SMTP_PORT;
			$PHPMailer->SMTPSecure =SMTP_SECURE;
			//Recipients
			$PHPMailer->setFrom( $email, $name );
			$PHPMailer->addAddress( GENERAL_EMAIL, 'Contact form' );
			$PHPMailer->addReplyTo( $email, $name );

			//Content
			$PHPMailer->isHTML( true );
			$PHPMailer->Subject = 'Message from contact form ' . get_site_url();
			$PHPMailer->Body    = "You message from <strong>$name</strong><br>$input_text";

			$PHPMailer->send();
			echo json_encode( array( 'success' => true, 'html' => 'Your message has been sent.' ) );
			wp_die();
		} catch ( Exception $exception ) {
			echo json_encode( array( 'success' => false, 'error' => $exception->getMessage() ) );
			wp_die();
		}
	} else {
		echo json_encode( array( 'success' => false, 'error' => 'I think you are a robot' ) );
		wp_die();
	}

}

add_action( 'wp_ajax_contacts_form', 'contacts_form' );
add_action( 'wp_ajax_nopriv_contacts_form', 'contacts_form' );

function sendingCV_form() {
	$application_format = [
		'application/pdf',
		'application/msword',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	];
	$link               = $_POST['link'];
	$phone              = $_POST['phone'];
	$input_text         = trim( $_POST['cover_letter'] );
	$input_text         = htmlspecialchars( $input_text );
	$email              = filter_var( $_POST['mail'], FILTER_VALIDATE_EMAIL );
	$name               = $_POST['name'];
	$subject            = trim( $_POST['subject'] );


	if ( isset( $_POST['g-recaptcha-response'] ) ) {
		$response = $_POST['g-recaptcha-response'];
	}

	if ( ! $response ) {
		echo json_encode( array( 'success' => false, 'error' => 'Please check the the captcha form.' ) );
		wp_die();
	}

	if ( google_validation( $response, SECRETKEY_RECAPTCHA ) == true ) {

		if ( empty( $name ) && empty( $email ) && empty( $input_text ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Fill in the form fields' ) );
			wp_die();
		}
		if ( empty( $name ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter a name' ) );
			wp_die();
		}
		if ( empty( $email ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter email' ) );
			wp_die();
		}

		if ( ! $email ) {
			echo json_encode( array( 'success' => false, 'error' => 'Please enter a valid email' ) );
			wp_die();
		}
		if ( empty( $input_text ) ) {
			echo json_encode( array( 'success' => false, 'error' => 'Enter a message' ) );
			wp_die();
		}

		if ( ! empty( $_FILES['portfolio_CV'] ) ) {
			if ( ! in_array( $_FILES['portfolio_CV']['type'], $application_format ) ) {
				echo json_encode( array( 'success' => false, 'error' => 'Only word or pdf files.' ) );
				wp_die();
			}
//		File siz 10 Megabytes
			if ( $_FILES["portfolio_CV"]["size"] > 10485760 ) {
				echo json_encode( array( 'success' => false, 'error' => 'File size is over 10 MB' ) );
				wp_die();
			}

			if ( $_FILES["portfolio_CV"]["error"] > 0 ) {
				echo json_encode( array( 'success' => false, 'error' => $_FILES["portfolio_CV"]["error"] ) );
				wp_die();
			}
		}
		$PHPMailer = new \PHPMailer\PHPMailer\PHPMailer( true );


		try {
			$PHPMailer->CharSet = 'utf-8';;
			$input_text = stripslashes( $input_text );
			$Body       = "You message from <strong>$name</strong><br>";
			if ( ! empty( $phone ) ) {
				$Body .= "Phone number <strong>$phone</strong><br>";
			}
			if ( ! empty( $link ) ) {
				$Body .= "Added link <a href='$link' target='_blank'>$link</a><br>";
			}

			$Body .= $input_text;
			//SMTP settings

			$PHPMailer->isSMTP();
			$PHPMailer->SMTPAuth  = true;
			$PHPMailer->SMTPDebug = 0;
			$PHPMailer->Host     = SMTP_HOST;
			$PHPMailer->Username = GENERAL_EMAIL_LOGIN;
			$PHPMailer->Password = GENERAL_EMAIL_PAS;
			$PHPMailer->Port     = SMTP_PORT;
			$PHPMailer->SMTPSecure = SMTP_SECURE;
			//Recipients
			$PHPMailer->setFrom( $email, $name );
			$PHPMailer->addAddress( HR_EMAIL, 'CV sending' );
			$PHPMailer->addReplyTo( $email, $name );

			//Attachments
			if ( ! empty( $_FILES["portfolio_CV"] ) ) {
				$PHPMailer->addAttachment( $_FILES["portfolio_CV"]['tmp_name'], $_FILES["portfolio_CV"]['name'] );
			}
			//Content
			$PHPMailer->isHTML( true );
			$PHPMailer->Subject = $subject;
			$PHPMailer->Body    = $Body;

			$PHPMailer->send();
			echo json_encode( array( 'success' => true, 'html' => 'Your message has been sent.' ) );
			wp_die();
		} catch ( Exception $exception ) {
			echo json_encode( array( 'success' => false, 'error' => $exception->getMessage() ) );
			wp_die();
		}
	} else {
		echo json_encode( array( 'success' => false, 'error' => 'I think you are a robot' ) );
		wp_die();
	}

}

add_action( 'wp_ajax_sendingCV_form', 'sendingCV_form' );
add_action( 'wp_ajax_nopriv_sendingCV_form', 'sendingCV_form' );
function additional_game_images( $post ) {
	$header_game_id        = null;
	$header_game_table_id  = null;
	$header_game_mobile_id = null;
	// Используем nonce для верификации
	$no_image_src = get_template_directory_uri() . '/image/no-image.jpg';


	if ( get_post_meta( $post->ID, 'header_game_images' ) ) {
		$header_game_id = (int) get_post_meta( $post->ID, 'header_game_images', true );
	}
	if ( $header_game_id ) {
		$header_game_images_src = wp_get_attachment_image_src( $header_game_id )[0];
	} else {
		$header_game_images_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'header_game_images_table' ) ) {
		$header_game_table_id = (int) get_post_meta( $post->ID, 'header_game_images_table', true );
	}

	if ( $header_game_table_id ) {
		$header_game_images_table_src = wp_get_attachment_image_src( $header_game_table_id )[0];
	} else {
		$header_game_images_table_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'header_game_images_table' ) ) {
		$header_game_mobile_id = (int) get_post_meta( $post->ID, 'header_game_images_mobile', true );
	}
	if ( $header_game_mobile_id ) {
		$header_game_images_mobile_src = wp_get_attachment_image_src( $header_game_mobile_id )[0];
	} else {
		$header_game_images_mobile_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'home_page_header_game_images' ) ) {
		$home_page_header_game_images_id = (int) get_post_meta( $post->ID, 'home_page_header_game_images', true );
	}
	if ( $home_page_header_game_images_id ) {
		$home_page_header_game_images_src = wp_get_attachment_image_src( $home_page_header_game_images_id )[0];
	} else {
		$home_page_header_game_images_src = $no_image_src;
	}
	if ( get_post_meta( $post->ID, 'home_page_last_game_images' ) ) {
		$home_page_last_game_images_id = (int) get_post_meta( $post->ID, 'home_page_last_game_images', true );
	}
	if ( $home_page_last_game_images_id ) {
		$home_page_last_game_images_src = wp_get_attachment_image_src( $home_page_last_game_images_id )[0];
	} else {
		$home_page_last_game_images_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'home_page_header_big_game_images' ) ) {
		$home_page_header_big_game_images_id = (int) get_post_meta( $post->ID, 'home_page_header_big_game_images', true );
	}
	if ( $home_page_header_big_game_images_id ) {
		$home_page_header_big_game_images_src = wp_get_attachment_image_src( $home_page_header_big_game_images_id )[0];
	} else {
		$home_page_header_big_game_images_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'min_game_logo' ) ) {
		$min_game_logo_id = (int) get_post_meta( $post->ID, 'min_game_logo', true );
	}
	if ( $min_game_logo_id ) {
		$min_game_logo_src = wp_get_attachment_image_src( $min_game_logo_id )[0];
	} else {
		$min_game_logo_src = $no_image_src;
	}


	if ( get_post_meta( $post->ID, 'substrate_img_big' ) ) {
		$substrate_img_big_id = (int) get_post_meta( $post->ID, 'substrate_img_big', true );
	}
	if ( $substrate_img_big_id ) {
		$substrate_img_big_src = wp_get_attachment_image_src( $substrate_img_big_id )[0];
	} else {
		$substrate_img_big_src = $no_image_src;
	}

	if ( get_post_meta( $post->ID, 'substrate_img_middle' ) ) {
		$substrate_img_middle_id = (int) get_post_meta( $post->ID, 'substrate_img_middle', true );
	}
	if ( $substrate_img_middle_id ) {
		$substrate_img_middle_src = wp_get_attachment_image_src( $substrate_img_middle_id )[0];
	} else {
		$substrate_img_middle_src = $no_image_src;
	}


	if ( get_post_meta( $post->ID, 'substrate_img_small' ) ) {
		$substrate_img_small_id = (int) get_post_meta( $post->ID, 'substrate_img_small', true );
	}
	if ( $substrate_img_small_id ) {
		$substrate_img_small_src = wp_get_attachment_image_src( $substrate_img_small_id )[0];
	} else {
		$substrate_img_small_src = $no_image_src;
	}

//    TODO start


	wp_nonce_field( plugin_basename( __FILE__ ), 'additional_game_images' );
	?>


    <input type="hidden" id="no_image_src" value="<?php echo $no_image_src ?>">
    <p class="text_important">Прежде чем добавить изображения, убедись что они нужного формата, размера и минимизированы
        для веба <a href="https://tinypng.com/" target="_blank">tinypng.com</a></p>
    <div class="header_game">


        <div class="home_page_header_game">
            <div class="header_game__item">
                <p>*Логотип Игрового слота при разрешении <450px <span class="text_recomend">Не меньше 110рх по вертикали .jpg или .png </span>
                </p>
                <input type="hidden" class="attachment_id" name="min_game_logo" value="<?php echo $min_game_logo_id ?>">
                <img src="<?php echo $min_game_logo_src ?>" width="350" height="350" class="js-game_img">
                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>
        </div>

        <div class="home_page_header_game">
            <div class="header_game__item">
                <p> Большое изображение превью <span
                            class="text_recomend">Рекомендованное разрешение 1600х512 .jpg </span>
                </p>
                <input type="hidden" class="attachment_id" name="header_game_images"
                       value="<?php echo $header_game_id ?>">
                <img src="<?php echo $header_game_images_src ?>" width="350" height="350" class="js-game_img">
                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>

            <div class="header_game__item">
                <p> Большое изображение превью для нетбуков <span class="text_recomend">Рекомендованное разрешение 991х690 .jpg </span>
                </p>
                <input type="hidden" class="attachment_id" name="header_game_images_table"
                       value="<?php echo $header_game_table_id; ?> ">
                <img src="<?php echo $header_game_images_table_src ?>" width="350" height="350" class="js-game_img">
                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>

            <div class="header_game__item">
                <p> Большое изображение превью для смартфонов <span class="text_recomend">Рекомендованное разрешение 380х654 .jpg </span>
                </p>
                <input type="hidden" class="attachment_id" name="header_game_images_mobile"
                       value="<?php echo $header_game_mobile_id ?>">
                <img src="<?php echo $header_game_images_mobile_src ?>" width="350" height="350" class="js-game_img">
                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>
        </div>
    </div>

    <div class="home_page_header_game">
        <div class="header_game__item">
            <p> Изображение записи для страници Игр и Главной <span
                        class="text_recomend">Рекомендованное разрешение 245х334 .jpg </span></p>
            <input type="hidden" class="attachment_id" name="home_page_header_game_images"
                   value="<?php echo $home_page_header_game_images_id ?>">
            <img src="<?php echo $home_page_header_game_images_src ?>" width="350" height="350" class="js-game_img">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>


        <div class="header_game__item">
            <p> Большое (Для окон браузера >2400px) Изображение записи для страници Игр и Главной
                <span class="text_recomend">Рекомендованное разрешение 380х516 .jpg </span></p>
            <input type="hidden" class="attachment_id" name="home_page_header_big_game_images"
                   value="<?php echo $home_page_header_big_game_images_id ?>">
            <img src="<?php echo $home_page_header_big_game_images_src ?>" width="350" height="350" class="js-game_img"
                 alt="game big screen">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>


    </div>
    <div class="home_page_header_game">
        <div class="header_game__item">
            <p>Изображение для последних игр Главной страницы <span class="text_recomend">Рекомендованное разрешение 534х335 .jpg </span>
            </p>
            <input type="hidden" class="attachment_id" name="home_page_last_game_images"
                   value="<?php echo $home_page_last_game_images_id ?>">
            <img src="<?php echo $home_page_last_game_images_src ?>" width="350" height="350" class="js-game_img">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>
    </div>

    <!-- /* TODO Изображение подложки больше не  надо -->
    <div class="home_page_header_game hidden">
        <div class="header_game__item">
            <p> Изображение "подложка" для Игры <span class="text_recomend"> Большое </span> <span
                        class="text_recomend">Рекомендованное разрешение 1915х1082 .jpg (Ужатое!)</span>
            </p>

            <input type="hidden" class="attachment_id" name="substrate_img_big"
                   value="<?php echo $substrate_img_big_id ?>">
            <img src="<?php echo $substrate_img_big_src ?>" width="350" height="350" class="js-game_img">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>

        <div class="header_game__item">
            <p> Изображение "подложка" для Игры <span class="text_recomend"> Для ноутбуков </span> <span
                        class="text_recomend">Рекомендованное разрешение 991х565 .jpg (Ужатое!)</span>
            </p>

            <input type="hidden" class="attachment_id" name="substrate_img_middle"
                   value="<?php echo $substrate_img_middle_id ?>">
            <img src="<?php echo $substrate_img_middle_src ?>" width="350" height="350" class="js-game_img">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>

        <div class="header_game__item">
            <p> Изображение "подложка" для Игры <span class="text_recomend"> Для смарфонов </span> <span
                        class="text_recomend">Рекомендованное разрешение 450х260 .jpg (Ужатое!)</span>
            </p>

            <input type="hidden" class="attachment_id" name="substrate_img_small"
                   value="<?php echo $substrate_img_small_id ?>">
            <img src="<?php echo $substrate_img_small_src ?>" width="350" height="350" class="js-game_img">
            <div class="group_buttons">
                <button type="submit" class="upload_image_button">Загрузить</button>
                <button type="submit" class="remove_image_button">&times;</button>
            </div>
        </div>

    </div>

    <!--  SLIDER -->
    <p> Изображения для слайдера страници Игры </p>
    <button type="button" class="slider_game_add">Добавить слайд</button>
	<?php
	$slider_game_images_id = json_decode( get_post_meta( $post->ID, 'slider_game_images', true ) );
	if ( ! empty( $slider_game_images_id ) ) {
		$data = (array) $slider_game_images_id;
		for ( $i = 0; $i < count( $data['small'] ); $i ++ ) {

			?>
            <div class="slider_game">
				<?php
				$id_img = $data['small'][ $i ];
				if ( wp_get_attachment_image_src( $id_img )[0] ) {
					$img_src = wp_get_attachment_image_src( $id_img )[0];
				} else {
					$img_src = $no_image_src;
				}
				?>
                <div class="slider_game_content hidden">
                    <span class="text_recomend">Рекомендованное разрешение 496х286 .jpg</span>

                    <input type="hidden" class="attachment_id" name="slider_game_images[small][]"
                           value="<?php echo $id_img ?>">
                    <img src="<?php echo $img_src ?>" width="350" height="350"
                         class="js-game_img" alt="no-image">

                    <div class="group_buttons">
                        <button type="submit" class="upload_image_button">Загрузить</button>
                        <button type="submit" class="remove_image_button">&times;</button>
                    </div>
                </div> <?php
				if ( isset( $data['big'][ $i ] ) ) {
					$id_img = $data['big'][ $i ];
				} else {
					$id_img = null;
				}
				if ( wp_get_attachment_image_src( $id_img )[0] ) {
					$img_src = wp_get_attachment_image_src( $id_img )[0];
				} else {
					$img_src = $no_image_src;
				}
				?>
                <div class="slider_game_content">
                    <span class="text_recomend">Размер Скрина 990х560 .jpg</span>

                    <input type="hidden" class="attachment_id" name="slider_game_images[big][]"
                           value="<?php echo $id_img ?>">
                    <img src="<?php echo $img_src ?>" width="350" height="350"
                         class="js-game_img" alt="no-image"> <!--   TODO scr -->

                    <div class="group_buttons">
                        <button type="submit" class="upload_image_button">Загрузить</button>
                        <button type="submit" class="remove_image_button">&times;</button>
                    </div>
                </div>
            </div>
		<?php }
	} else { ?>
        <div class="slider_game">

            <div class="slider_game__big">
                <span class="text_recomend">Размер Скрина 990х560 .jpg </span>
                <input type="hidden" class="attachment_id" name="slider_game_images[big][]"
                       value="">
                <img src="<?php echo $no_image_src ?>" width="320" height="350" class="js-game_img" alt="no-image">

                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>
            <!-- TODO Маленькие изображения пока не задействованы скрыты через hidden-->
            <div class="slider_game__middle hidden">
                <span class="text_recomend">Рекомендованное разрешение 496х286 .jpg </span>

                <input type="hidden" class="attachment_id" name="slider_game_images[small][]"
                       value="">
                <img src="<?php echo $no_image_src ?>" width="350" height="250" class="js-game_img" alt="no-image">

                <div class="group_buttons">
                    <button type="submit" class="upload_image_button">Загрузить</button>
                    <button type="submit" class="remove_image_button">&times;</button>
                </div>
            </div>

        </div>
	<?php } ?>

<?php }

add_action( 'add_meta_boxes', 'additional_game_images_custom_box' );

function additional_game_images_custom_box() {
	$screens = array( 'games' );
	add_meta_box( 'additional_game_images', 'Изображения для страницы игры', 'additional_game_images', $screens );
}

function additional_game_images_save_postdata() {

	$post_id = get_the_ID();

	// Убедимся что поле установлено.
	if (
		! isset( $_POST['header_game_images'] ) &&
		! isset( $_POST['header_game_images_table'] ) &&
		! isset( $_POST['header_game_images_mobile'] ) &&
		! isset( $_POST['slider_game_images'] ) &&
		! isset( $_POST['slider_game_images_BIG'] ) &&
		! isset( $_POST['home_page_header_game_images'] ) &&
		! isset( $_POST['min_game_logo'] ) &&
		! isset( $_POST['home_page_header_big_game_images'] ) &&
		! isset( $_POST['substrate_img_big'] ) &&
		! isset( $_POST['substrate_img_middle'] ) &&
		! isset( $_POST['substrate_img_small'] ) &&
		! isset( $_POST['home_page_last_game_images'] ) ) {

		delete_post_meta( $post_id, 'header_game_images' );
		delete_post_meta( $post_id, 'header_game_images_table' );
		delete_post_meta( $post_id, 'header_game_images_mobile' );
		delete_post_meta( $post_id, 'slider_game_images' );
		delete_post_meta( $post_id, 'slider_game_images_BIG' );
		delete_post_meta( $post_id, 'home_page_header_game_images' );
		delete_post_meta( $post_id, 'home_page_last_game_images' );
		delete_post_meta( $post_id, 'home_page_header_big_game_images' );
		delete_post_meta( $post_id, 'min_game_logo' );
		delete_post_meta( $post_id, '$substrate_img_big_src' );
		delete_post_meta( $post_id, '$substrate_img_middle_src' );
		delete_post_meta( $post_id, '$substrate_img_small_src' );

		return;
	}

	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['additional_game_images'], plugin_basename( __FILE__ ) ) ) {
		return;
	}

	// если это автосохранение ничего не делаем
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// проверяем права юзера
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	for ( $i = 0; $i < count( $_POST['slider_game_images'] ); $i ++ ) {
		if ( empty( (int) $_POST['slider_game_images'][ $i ] ) ) {
			unset( $_POST['slider_game_images'][ $i ] );
		}
	}
	$slider_game_images               = json_encode( $_POST['slider_game_images'] );
	$header_game_images               = (int) $_POST['header_game_images'];
	$header_game_images_table         = (int) $_POST['header_game_images_table'];
	$header_game_images_mobile        = (int) $_POST['header_game_images_mobile'];
	$home_page_header_game_images     = (int) $_POST['home_page_header_game_images'];
	$home_page_last_game_images       = (int) $_POST['home_page_last_game_images'];
	$home_page_header_big_game_images = (int) $_POST['home_page_header_big_game_images'];
	$min_game_logo                    = (int) $_POST['min_game_logo'];
	$substrate_img_big                = (int) $_POST['substrate_img_big'];
	$substrate_img_middle             = (int) $_POST['substrate_img_middle'];
	$substrate_img_small              = (int) $_POST['substrate_img_small'];
	// Обновляем данные в базе данных.
	update_post_meta( $post_id, 'slider_game_images', $slider_game_images );
	update_post_meta( $post_id, 'header_game_images', $header_game_images );
	update_post_meta( $post_id, 'header_game_images_table', $header_game_images_table );
	update_post_meta( $post_id, 'header_game_images_mobile', $header_game_images_mobile );
	update_post_meta( $post_id, 'home_page_header_game_images', $home_page_header_game_images );
	update_post_meta( $post_id, 'home_page_last_game_images', $home_page_last_game_images );
	update_post_meta( $post_id, 'home_page_header_big_game_images', $home_page_header_big_game_images );
	update_post_meta( $post_id, 'min_game_logo', $min_game_logo );
	update_post_meta( $post_id, 'substrate_img_big', $substrate_img_big );
	update_post_meta( $post_id, 'substrate_img_middle', $substrate_img_middle );
	update_post_meta( $post_id, 'substrate_img_small', $substrate_img_small );
}

add_action( 'save_post', 'additional_game_images_save_postdata' );


/**
 * @param int $current_page
 * current pagination page
 *
 * @param int $quantity_on_page
 * number of games required
 *
 * @return string
 * sections with information about games
 */
function get_header_games( $current_page, $quantity_on_page ) {
	$current_page = abs( $current_page );
	$current_page = (int) $current_page;
	if ( empty( $current_page ) ) {
		$current_page = 1;
	}
	$SortGames = [];
	if ( function_exists( 'getSortedList' ) ) {
		$SortGames = getSortedList();
	}
	if ( is_array( $SortGames ) && ! empty( $SortGames ) ) {
		$args = array(
			'posts_per_page' => $quantity_on_page,
			'post_type'      => 'games',
			'post_status'    => 'publish',
			'paged'          => $current_page,
			'meta_query'     => array(
				array(
					'key' => 'sort_place',
				)
			),
			'meta_key'       => 'sort_place',
			'orderby'        => 'meta_value_num',
			'order'          => 'ASC'
		);
	} else {
		$args = array(
			'posts_per_page' => $quantity_on_page,
			'post_status'    => 'publish',
			'post_type'      => 'games',
			'paged'          => $current_page,
		);
	}

	$query = new WP_Query( $args );
	$html  = '';
	if ( $current_page > 1 && $query->post_count == 0 ) {
		return $html;
	}


	foreach ( $query->query( $args ) as $game ) {

		$templateUrl                     = get_template_directory_uri();
		$content                         = strip_tags( $game->post_content );
		$rez_content                     = mb_substr( $content, 0, 115 );
		$home_page_header_game_images_id = get_post_custom_values( 'home_page_header_game_images', $game->ID )[0];
		$img                             = wp_get_attachment_image_src( $home_page_header_game_images_id, 'full', true )[0];
		$game_href                       = get_post_custom_values( 'href_run_demo', $game->ID )[0];
		$tag_comingSoon_class            = null;
		$big_img_id                      = get_post_custom_values( 'home_page_header_big_game_images', $game->ID )[0];
		$big_img                         = wp_get_attachment_image_src( $big_img_id, 'full', true )[0];
		$img_webP                        = str_replace( array( 'png', 'jpg' ), 'webp', $img );
		if ( ! empty( get_post_custom_values( 'header_short_description', $game->ID )[0] ) ) {
			$short_description = get_post_custom_values( 'header_short_description', $game->ID )[0];
		} else {
			$short_description = 'Дополнительное поле header_short_description';
		}
		if ( has_tag( 'coming-soon', $game->ID ) ) {
			continue; /// Удалить если надо выводить игры с меткой Coming-soon
			$img  = get_template_directory_uri() . '/image/slot-games/comming-soon.jpg';
			$html .= '<li class="bl_games__item tag_comingSoon">
                        <div class="bl_games__item_wrapper">
                    <p class="bl_games__name">' . $game->post_title . '</p>
                    <p class="bl_games__desc">' . $short_description . '</p>
                    <picture class="bl_games__image">';

			if ( class_exists( '\Netgeme\WebPImg' ) ) {
				$html .= ( new \Netgeme\WebPImg( $big_img_id ) )->getWeb( [ 'media="(min-width: 2400px)"' ] );
			}
			$html .= '<source media="--large" srcset="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-srcset="' . $big_img . '">';
			if ( class_exists( '\Netgeme\WebPImg' ) ) {
				$html .= ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
			}
			$html = '<source srcset="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-srcset="' . $img . '">
                     <img class="bl_games__image lazyload" src="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-src="' . $img . '" alt="' . $game->post_title . '">
                          </picture>        
                 </div> 
                </li>';

		} else {
			$html .= '<li class="bl_games__item">
                        <button class="btn_gameInfo" type="button" aria-label="button look at short information about ' . $game->post_title . ' game"><span class="btn_gameInfo__inner"></span></button>
                        <a class="bl_games__link_inner" href="' . wp_make_link_relative( get_permalink( $game->ID ) ) . '">
                            <picture class="bl_games__image">';
			if ( class_exists( '\Netgeme\WebPImg' ) ) {
				$html .= ( new \Netgeme\WebPImg( $big_img_id ) )->getWeb( [ 'media="(min-width: 2400px)"' ] );
			}
			$html .= '<source media="--large" srcset="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-srcset="' . $big_img . '">';
			if ( class_exists( '\Netgeme\WebPImg' ) ) {
				$html .= ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
			}
			$html .= '<source srcset="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-srcset="' . $img . '">
                                <img class="bl_games__image lazyload" src="' . $templateUrl . '/image/svg-elements/preloadImage.svg" data-src="' . $img . '" alt="' . $game->post_title . '">
                            </picture>
                        </a>
                        <div class="bl_games__item_wrapper">
                            <p class="bl_games__name">' . $game->post_title . '</p>
                            <p class="bl_games__desc">' . $short_description . '</p>
                        ';
			if ( ! empty( $game_href ) ) {
				$html .= '<button class="btn_playGame js-playGame" data-href="' . $game_href . '" type="button" aria-label="button run demo version of ' . $game->post_title . ' game">Run Demo</button>
                          <a class="btn_lookAtGame" href="' . wp_make_link_relative( get_permalink( $game->ID ) ) . '">Game info & Demo</a>';
			} else {
				$html .= '<a class="btn_lookAtGame" href="' . wp_make_link_relative( get_permalink( $game->ID ) ) . '">Game info</a>';
			}
			$html .= '</div></li>';
		}

	}

	wp_reset_postdata();

	return $html;
}

/*  Добавление ещё игр на гллавной странице */
add_action( 'wp_ajax_get_more_header_games', 'get_more_header_games' );
add_action( 'wp_ajax_nopriv_get_more_header_games', 'get_more_header_games' );

function get_more_header_games() {
	$current_page     = $_POST['current_page'];
	$quantity_on_page = $_POST['quantity_on_page'];
	$rezalt           = [
		'next_page' => $current_page + 1,
	];

	if ( empty( get_header_games( $current_page + 2, $quantity_on_page ) ) ) {
		$rezalt['next_page'] = false;
	}
	$rezalt['html'] = get_header_games( $current_page + 1, $quantity_on_page );
	echo json_encode( $rezalt );
	wp_die();
}

/* Получение нужного колличества игр по медиа запросу */
add_action( 'wp_ajax_get_games_for_homePageList', 'get_games_for_homePageList' );
add_action( 'wp_ajax_nopriv_get_games_for_homePageList', 'get_games_for_homePageList' );

function get_games_for_homePageList() {
	$html = '';
	$arr  = [];
	if ( isset( getSortedList()[10] ) ) {
		$arr[] = getSortedList()[10];
	}
	if ( isset( getSortedList()[11] ) ) {
		$arr[] = getSortedList()[11];
	}
	foreach ( $arr as $id ) {


		$game                            = get_post( $id );
		$home_page_header_game_images_id = get_post_custom_values( 'home_page_header_game_images', $game->ID )[0];
		$img                             = wp_get_attachment_image_src( $home_page_header_game_images_id, 'full', true )[0];
		$big_img_id                      = get_post_custom_values( 'home_page_header_big_game_images', $game->ID )[0];
		$big_img                         = wp_get_attachment_image_src( $big_img_id, 'full', true )[0];
		$img_webP                        = str_replace( array( 'png', 'jpg' ), 'webp', $img );
		if ( ! empty( get_post_custom_values( 'header_short_description', $game->ID )[0] ) ) {
			$short_description = get_post_custom_values( 'header_short_description', $game->ID )[0];
		} else {
			$short_description = 'Дополнительное поле header_short_description';
		}
		$html .= '<li class="bl_games__item">
                    <a class="bl_games__link_inner" href="' . wp_make_link_relative( get_permalink( $game->ID ) ) . '">
                        <picture class="bl_games__image">';

		if ( class_exists( '\Netgeme\WebPImg' ) ) {
			$html .= ( new \Netgeme\WebPImg( $big_img_id ) )->getWeb( [ 'media="(min-width: 2400px)"' ] );
		}
		$html .= '<source media="--large" srcset="' . $img . '/image/svg-elements/preloadImage.svg" data-srcset="' . $big_img . '">';
		if ( class_exists( '\Netgeme\WebPImg' ) ) {
			$html .= ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
		}
		if ( class_exists( '\Netgeme\WebPImg' ) ) {
			$html .= ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
		}
		$html .= '<source srcset="' . $img . '">
                            <img class="bl_games__image" src="' . $img . '" alt="' . $game->post_title . '">
                        </picture>
                    </a>
                    <div class="bl_games__item_wrapper">
                        <button class="btn_gameInfo" type="button"><span class="btn_gameInfo__inner"></span></button>
                                <h2 class="bl_games__name">' . $game->post_title . '</h2>
                                <p class="bl_games__desc">' . $short_description . '</p>
                        <a class="btn_lookAtGame" href="' . wp_make_link_relative( get_permalink( $game->ID ) ) . '">Game info & Demo</a>
                    </div>
                </li>';

	}

	echo json_encode( [ 'games' => $html ] );
	wp_die();
}


/* добавление поля в профиле*/
add_action( 'show_user_profile', 'add_extra_social_links' );
add_action( 'edit_user_profile', 'add_extra_social_links' );

function add_extra_social_links( $user ) {
	?>
    <h3>Дополнительные данные пользователя</h3>

    <table class="form-table">
        <tr>
            <header_game__itemth><label for="adress_prof">Skype</label></header_game__itemth>
            <td><input type="text" name="skype"
                       value="<?php echo esc_attr( get_the_author_meta( 'skype', $user->ID ) ); ?>"
                       class="regular-text"/></td>
        </tr>
        <tr>
            <th><label for="email_prof">Телефон</label></th>
            <td>
                <input type="text" name="phone"
                       value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>"
                       class="regular-text"/>
            </td>
        </tr>
    </table>
	<?php
}


add_action( 'personal_options_update', 'save_extra_social_links' );
add_action( 'edit_user_profile_update', 'save_extra_social_links' );

function save_extra_social_links( $user_id ) {
	update_user_meta( $user_id, 'skype', sanitize_text_field( $_POST['skype'] ) );
	update_user_meta( $user_id, 'phone', sanitize_text_field( $_POST['phone'] ) );
}


function true_rewrite_conflicts( $request ) {
	if ( ! is_admin() ) {
		$request['post_type'] = array( 'games', 'vacancy', 'post', 'page', 'insights' );
	}

	return $request;
}

add_filter( 'request', 'true_rewrite_conflicts' );
add_action( 'init', 'true_post_type_rewrite' );
function true_post_type_rewrite() {

	global $wp_rewrite;
	$wp_rewrite->add_rewrite_tag( "%games%", '([^/]+)', "games=" );
	$wp_rewrite->add_permastruct( 'games', '%games%' );

	$wp_rewrite->add_rewrite_tag( "%vacancy%", '([^/]+)', "vacancy=" );
	$wp_rewrite->add_permastruct( 'vacancy', '%vacancy%' );

}


add_action( 'admin_menu', 'register_reade_me_games_submenu_page' );

function register_reade_me_games_submenu_page() {
	add_submenu_page( 'edit.php?post_type=games', 'README', 'README', 'edit_posts', 'reade-me-games', 'reade_me_games' );
}

function reade_me_games() { ?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>
        <table style="width: 100%">
            <tr>
                <th style="width: 50%">Действие</th>
                <th style="width: 50%">Результат</th>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле header_short_description</td>
                <td style="width: 50%">Краткое описание игры на главной странице</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле last_short_description</td>
                <td style="width: 50%">Краткое описание игры на главной странице в блоке последних игр</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле realeased (ОБЯЗАТЕЛЬНО ФОРМАТ dd.mm.YYYY)</td>
                <td style="width: 50%">Дата выхода игры</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле header_text</td>
                <td style="width: 50%">Текст в хедере игры</td>
            </tr>
            <tr>
                <td style="width: 50%">Метка FullHD Games</td>
                <td style="width: 50%">Добавление FullHD Games в хедере игры</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле href_run_demo</td>
                <td style="width: 50%">Добавление кнопки Run Demo с указанной ссылкой</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле href_play_video</td>
                <td style="width: 50%">Добавление кнопки Play Video с указанной ссылкой</td>
            </tr>

            <tr>
                <td style="width: 50%">Дополнительное поле reels</td>
                <td style="width: 50%">Значение параметра Reels</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле lines</td>
                <td style="width: 50%">Значение параметра Paylines</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле max_win</td>
                <td style="width: 50%">Значение параметра Maximum Win Base Game</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле volatility</td>
                <td style="width: 50%">Значение параметра Volatility</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле bonus_games</td>
                <td style="width: 50%">Значение параметра Bonus Feature</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле rtp</td>
                <td style="width: 50%">Значение параметра RTP</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле game_type</td>
                <td style="width: 50%">Значение параметра Game type</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле goes</td>
                <td style="width: 50%">Значение параметра Goes on</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле technologies</td>
                <td style="width: 50%">Значение параметра Technologies</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле gameplay_features</td>
                <td style="width: 50%">Значение параметра Gameplay Features</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле slot_type</td>
                <td style="width: 50%">Значение параметра Slot type</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле symbols</td>
                <td style="width: 50%">Значение параметра Symbols</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле hit_frequency</td>
                <td style="width: 50%">Значение параметра Hit Frequency</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле bonus_feature_hit_rate</td>
                <td style="width: 50%">Значение параметра Bonus Feature Hit Rate</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле maximum_feature_exposure</td>
                <td style="width: 50%">Значение параметра Maximum Feature Exposure</td>
            </tr>
        </table>
    </div>
<?php }

add_action( 'admin_menu', 'register_reade_me_vacancy_submenu_page' );

function register_reade_me_vacancy_submenu_page() {
	add_submenu_page( 'edit.php?post_type=vacancy', 'README', 'README', 'edit_posts', 'reade-me-games', 'reade_me_vacancy' );
}

function reade_me_vacancy() {
	?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>
        <table style="width: 100%">
            <tr>
                <th style="width: 50%">Действие</th>
                <th style="width: 50%">Результат</th>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле short_description</td>
                <td style="width: 50%">Краткое описание вакансии для страницы всех <a href="/career"
                                                                                      target="_blank">
                        вакансий</a</td>
            </tr>
            <tr>
                <td>Дополнительное поле city_job</td>
                <td>Город вакансии</td>
            </tr>
        </table>
    </div>
	<?php
}


//  Cookies for COOKIES POLITIC

function set_cookies_confirm() {
	$domain = parse_url( get_home_url(), PHP_URL_HOST );
	$domain = str_replace( 'www.', '', $domain );
	echo json_encode( [ 'success' => setcookie( "CookiesConfirm", 'confirm', time() + 60 * 60 * 24 * 30 * 12, '/; SameSite=Strict', $domain, false, false ) ] );
	wp_die();
}

add_action( 'wp_ajax_set_cookies_confirm', 'set_cookies_confirm' );
add_action( 'wp_ajax_nopriv_set_cookies_confirm', 'set_cookies_confirm' );


/// Cookies for Invitation Banner

function set_cookies_invitationBanner() {
	$domain = parse_url( get_home_url(), PHP_URL_HOST );
	$domain = str_replace( 'www.', '', $domain );
	echo json_encode( [ 'success' => setcookie( "CookiesInvitationBanner", 'confirm', time() + 60 * 60 * 24 * 30 * 12, '/; SameSite=Strict', $domain, false, false ) ] );
	wp_die();
}

add_action( 'wp_ajax_set_cookies_invitationBanner', 'set_cookies_invitationBanner' );
add_action( 'wp_ajax_nopriv_set_cookies_invitationBanner', 'set_cookies_invitationBanner' );


function need_redirect() {
	$detect = new Mobile_Detect;
	if ( $detect->isMobile() || $detect->isTablet() ) {
		echo json_encode( [ 'success' => true ] );
	} else {
		echo json_encode( [ 'success' => false ] );
	}
	wp_die();
}

add_action( 'wp_ajax_need_redirect', 'need_redirect' );
add_action( 'wp_ajax_nopriv_need_redirect', 'need_redirect' );

## удаляет версию WP из преданного URL у скриптов и стилей
add_filter( 'script_loader_src', 'hb_remove_wp_version_from_src' );
add_filter( 'style_loader_src', 'hb_remove_wp_version_from_src' );
function hb_remove_wp_version_from_src( $src ) {
	global $wp_version;
	parse_str( parse_url( $src, PHP_URL_QUERY ), $query );
	if ( ! empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg( 'ver', $src );
	}

	return $src;
}


/**
 * Enqueues script with WordPress and adds version number that is a timestamp of the file modified date.
 *
 * @param string $handle Name of the script. Should be unique.
 * @param string|bool $src Path to the script from the theme directory of WordPress. Example: '/js/myscript.js'.
 * @param array $deps Optional. An array of registered script handles this script depends on. Default empty array.
 * @param bool $in_footer Optional. Whether to enqueue the script before </body> instead of in the <head>.
 *                               Default 'false'.
 */
function enqueue_versioned_script( $handle, $src = false, $deps = array(), $in_footer = false ) {
	$ver = filemtime( get_stylesheet_directory() . $src );
	wp_enqueue_script( $handle . $ver, get_stylesheet_directory_uri() . $src, $deps, $ver, $in_footer );
}

/**
 * Enqueues stylesheet with WordPress and adds version number that is a timestamp of the file modified date.
 *
 * @param string $handle Name of the stylesheet. Should be unique.
 * @param string|bool $src Path to the stylesheet from the theme directory of WordPress. Example: '/css/mystyle.css'.
 * @param array $deps Optional. An array of registered stylesheet handles this stylesheet depends on. Default empty array.
 * @param string $media Optional. The media for which this stylesheet has been defined.
 *                            Default 'all'. Accepts media types like 'all', 'print' and 'screen', or media queries like
 *                            '(orientation: portrait)' and '(max-width: 640px)'.
 */
function enqueue_versioned_style( $handle, $src = false, $deps = array(), $media = 'all' ) {
	$ver = filemtime( get_stylesheet_directory() . $src );
	wp_enqueue_style( $handle . $ver, get_stylesheet_directory_uri() . $src, $deps = array(), $ver, $media );
}

function isset_cookies() {

	echo json_encode( [ 'success' => isset( $_COOKIE[ $_POST['cookies_name'] ] ) ] );
	wp_die();
}

add_action( 'wp_ajax_isset_cookies', 'isset_cookies' );
add_action( 'wp_ajax_nopriv_isset_cookies', 'isset_cookies' );


add_action( 'admin_menu', 'my_plugin_menu' );
function my_plugin_menu() {
	add_options_page( 'Настройки SMTP', 'Настройки SMTP', 'manage_options', 'settings-smtp', 'my_plugin_page' );
}

add_action( 'admin_menu', function () {
	add_menu_page( 'PDF презентация', 'PDF презентация', 'edit_pages', 'settings-pdf', 'pdf_settings', 'dashicons-share' );
	add_submenu_page( 'settings-pdf', 'Настройки', 'Настройки', 'edit_pages', 'settings-pdf', 'pdf_settings' );
	add_submenu_page( 'settings-pdf', 'Emails PDF', 'Emails PDF', 'edit_pages', 'presentations-emails', 'presentations_emails' );
} );

function presentations_emails() { ?>
    <div class="wrap">
        <h1>Управление emails по лид форме с презентацией</h1>
		<?php
		 global $wpdb;
		 try {
		     $tableName = ( new \Netgeme\PdfPresentation() )->getTableName();
		     $users     = $wpdb->get_results( "SELECT * FROM $tableName ORDER BY `created_at` ASC" );
         } catch (Exception $exception){
                  $users = []; ?>
                    <div class="error notice is-dismissible">
                        <p><?php echo $exception->getMessage()?></p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
                    </div>
         <?php } ?>
        <table style="width: 100%">
            <tr>
                <td>#</td>
                <td>Email</td>
                <td>Имя пользователя</td>
                <td>Скачено</td>
                <td>IP</td>
                <td>Дата</td>
                <td>
                    <button type="button" class="button button-primary copy-mails-pdf-button">Скопировать (<?php echo count($users)?>) emails в
                        буфер
                    </button>
                </td>
                <td></td>
            </tr>
			<?php
			$forCopy = '';
			foreach ( $users as $n=>$user ) {
				$forCopy .= "{$user->mail} {$user->name}\n";
				?>
                <tr>
                    <td><?php echo $n+1 ?></td>
                    <td><?php echo $user->mail ?></td>
                    <td><?php echo $user->name ?></td>
                    <td><?php echo $user->count_download ?></td>
                    <td><?php echo $user->ip ?></td>
                    <td><?php echo $user->created_at ?></td>
                    <td>
                        <button type="button" class="button button-primary resend_pdf"
                                data-mail="<?php echo $user->mail ?>" data-reset="0">Переотправить
                        </button>
                    </td>
                    <td>
                        <button type="button" class="button button-primary resend_pdf"
                                data-mail="<?php echo $user->mail ?>" data-reset="1">Сбросить и переотправить
                        </button>
                    </td>
                </tr>
			<?php }
			?>
        </table>
        <div class="hidden">
            <textarea id="copy-mails-pdf"><?php echo $forCopy ?></textarea>
        </div>
    </div>
<?php }

function pdf_settings() {
	if ( $_POST ) {
		if ( ! empty( $_FILES['pdf_Presentation']['tmp_name'] ) ) {
			if ( move_uploaded_file( $_FILES['pdf_Presentation']['tmp_name'], get_stylesheet_directory() . '/pdf/Presentation.pdf' ) ) {
				 ?>
                     <div class="updated notice is-dismissible">
                        <p>Файл обновлен</p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
                    </div>
                <?php
			} else { ?>
                    <div class="error notice is-dismissible">
                        <p>Ошибка загрузки файла</p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
                    </div>
                <?php
			}
		}
		if ( $_POST['pdf_email'] ) {
                $pdf_email = trim($_POST['pdf_email']);
                if ( filter_var( $pdf_email, FILTER_VALIDATE_EMAIL ) ) {
                    update_option( 'pdf_email', $pdf_email );
                    ?>
                     <div class="updated notice is-dismissible">
                        <p>Email обновлен</p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
                    </div>
                <?php
                } else { ?>
                    <div class="error notice is-dismissible">
                        <p>Не корректный email</p>
                        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
                    </div>
                <?php }
            }
    }

    ?>
	 <div class="wrap">
        <h2>Настройки</h2>
        <?php if(file_exists(__DIR__.'/pdf/Presentation.pdf')){ ?>
            <h3><a href="/pdf" target="_blank"> Файл на сервере</a></h3>
        <?php } ?>
           <form enctype="multipart/form-data" method="POST" action="">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="updatePdf_presentation">
                            Обновить PDF презентации
                        </label>
                    </th>
                    <td>
                        <input id="updatePdf_presentation" type="file" accept="application/pdf" name="pdf_Presentation" >
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="pdf_email">Email отправки презентации </label>
                    </th>
                    <td>
                      <input id="pdf_email" type="email" name="pdf_email" value="<?php echo get_option('pdf_email','office@netgamenv.com')?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                        </label>
                    </th>
                    <td>
                      <input type="submit" name="save_pdf" class="button button-primary" value="Обновить">
                    </td>
                </tr>
                </table>
                </form>
<?php }

function my_plugin_page() {
	if ( ! empty( $_POST['smtp'] ) ) {
		foreach ( $_POST['smtp'] as $name => $value ) {
			if ( empty( $name ) ) {
				continue;
			}
			update_option( 'smtp_' . $name, $value );
		}
	}
	?>
    <div class="error notice is-dismissible">
         <p>ОБЯЗАТЕЛЬНО включить доступ для сторонних приложений для аккаунтов <a href="https://myaccount.google.com/u/0/lesssecureapps" target="_blank">здеесь</a></p>
         <button type="button" class="notice-dismiss"><span class="screen-reader-text">Скрыть это уведомление.</span></button>
    </div>
    <div class="wrap">
        <h2>Настройки SMTP</h2>
        <form method="POST" action="">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            Port
                        </label>
                    </th>
                    <td>
                        <input type="number" min="0" step="" name="smtp[port]"
                               value="<?php echo get_option( 'smtp_port', 465 ); ?>"/>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            Secure
                        </label>
                    </th>
                    <td>
                        <select name="smtp[secure]" >
                            <option <?php echo (get_option( 'smtp_secure', 'ttl' )=='ttl')?  'selected': null?> value="ttl">TTL</option>
                            <option <?php echo (get_option( 'smtp_secure', 'ttl' )=='ssl')?  'selected': null?>  value="ssl">SSL</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            Host
                        </label>
                    </th>
                    <td>
                        <input type="text" name="smtp[host]" size="25"
                               value="<?php echo get_option( 'smtp_host', '' ); ?>"/>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            SMTP_EMAIL_LOGIN
                        </label>
                    </th>
                    <td>
                        <input type="text" name="smtp[general_email_login]" size="25"
                               value="<?php echo get_option( 'smtp_general_email_login', '' ); ?>"/>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                            SMTP_EMAIL_PAS
                        </label>
                    </th>
                    <td>
                        <input type="password" name="smtp[general_email_pas]" size="25"
                               value="<?php echo get_option( 'smtp_general_email_pas', '' ); ?>"/>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="num_elements">
                        </label>
                    </th>
                    <td>
                        <input type="submit" value="Сохранить"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
<?php }


add_action( 'admin_menu', 'register_reade_me_partners_submenu_page' );

function register_reade_me_partners_submenu_page() {
	add_submenu_page( 'edit.php?post_type=partners', 'README', 'README', 'edit_posts', 'reade-me-partners', 'reade_me_partners' );
}

function reade_me_partners() {
	?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>
        <table style="width: 100%">
            <tr>
                <th style="width: 50%">Действие</th>
                <th style="width: 50%">Результат</th>
            </tr>
            <tr>
                <td style="width: 50%">Изображение записи</td>
                <td style="width: 50%">Логотип партнера</a</td>
            </tr>
            <tr>
                <td style="width: 50%">Заголовок записи</td>
                <td style="width: 50%">Label для логотипа</a</td>
            </tr>
            <tr>
                <td style="width: 50%">Слаг записи</td>
                <td style="width: 50%">Alt для логотипа</a</td>
            </tr>
            <tr>
                <td style="width: 50%">Дополнительное поле url_partner</td>
                <td style="width: 50%">Ссылка для логотипа</a</td>
            </tr>
        </table>
    </div>
	<?php
}


function pdf_presentation_form() {

	if ( isset( $_POST['g-recaptcha-response'] ) ) {
		$response = $_POST['g-recaptcha-response'];
	}
	if ( ! $response ) {
		echo json_encode( array( 'success' => false, 'error' => 'Please check the the captcha form.' ) );
		wp_die();
	}
	if ( google_validation( $response, SECRETKEY_RECAPTCHA ) ) {
		$mail = $_POST['email_pdf_form'];
		$name = $_POST['name_pdf_form'];
		try {
			$pdf = new \Netgeme\PdfPresentation();
			$pdf->setEmail( $mail )->setName( $name )->sendMail();
			echo json_encode( array( 'success' => true, 'html' => 'Your message has been sent.' ) );
			wp_die();
		} catch ( Exception $exception ) {
			echo json_encode( array( 'success' => false, 'error' => $exception->getMessage() ) );
			wp_die();
		}
	}
 else {
		echo json_encode( array( 'success' => false, 'error' => 'I think you are a robot' ) );
		wp_die();
	}
}
add_action( 'wp_ajax_pdf_presentation_form', 'pdf_presentation_form' );
add_action( 'wp_ajax_nopriv_pdf_presentation_form', 'pdf_presentation_form' );

function resend_pdf_presentation(){
	$email         = $_POST['mail'];
	$reset_counter = ( $_POST['reset'] == 1 ) ? true : false;
	try {
		$pdf = new \Netgeme\PdfPresentation();
		$pdf->setEmail( $email )->reSandEmail( $reset_counter );
		echo json_encode( array( 'success' => true, 'html' => 'Your message has been sent.' ) );
		wp_die();
	} catch ( Exception $exception ) {
		echo json_encode( array( 'success' => false, 'error' => $exception->getMessage() ) );
		wp_die();
	}
}

add_action( 'wp_ajax_resend_pdf_presentation', 'resend_pdf_presentation' );


function str_replace_once($search, $replace, $text)
{
   $pos = strpos($text, $search);
   return $pos!==false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
}

/**
 * @param $name string meta_key AdditionalImag
 * @param $value int id attachment
 */
function true_image_uploader_field( $name, $value = null ) {
	$default = get_template_directory_uri() . '/image/no-image/no-image-article.svg';
	$options = get_option( 'AdditionalImageptions' );
	$w = $options['width_show'];
	$h = $options['height_show'];
	if ( $value ) {
		$image_attributes = wp_get_attachment_image_src( $value, array( $w, $h ) );
		$src              = $image_attributes[0];
		$size_div = '<div id="SizeAdditionalImage">Размеры изображения: <br>' . wp_get_attachment_metadata( $value )['width'] . ' X ' . wp_get_attachment_metadata( $value )['height'] . ' px</div>';
		$add_div = '<div id="AddAdditionalImage">Для вставки в код использывать GetAdditionalImage($id_post) где $id_post id поста (Возвращает  масив с ключами URL и  ALT)</div>';
	} else {
		$src = $default;
		$size_div = null;
		$add_div = null;
	}
	echo '
<div>Добавить картинку</div>
	<div>
		<img data-src="' . $default . '" src="' . $src . '" width="100px" height="100px" />
		<div>
			<input type="hidden" name="' . $name . '" id="IdAdditionalImage" value="' . $value . '" />
			<button type="submit"  class="upload_image_button button">Загрузить</button>
			<button type="submit" class="remove_image_button button">&times;</button>
			'.$size_div.'
			'.$add_div.'
		</div>
	</div>
		<div>
		<img data-src="' . $default . '" src="' . $src . '" width="100px" height="100px" />
		<div>
			<input type="hidden" name="' . $name . '" id="IdAdditionalImage" value="' . $value . '" />
			<button type="submit"  class="upload_image_button button">Загрузить</button>
			<button type="submit" class="remove_image_button button">&times;</button>
			'.$size_div.'
			'.$add_div.'
		</div>
	</div>
	';
}

function AdditionalImags_add_custom_box() {
    if ( in_category( array('insights'), get_the_ID() )) {
	add_meta_box( 'AdditionalImag_sectionid', 'Картинки новости', 'AdditionalImags_meta_box_callback', 'post', 'side' );
	}
}
add_action( 'add_meta_boxes', 'AdditionalImags_add_custom_box' );

// HTML код блока метабокс
function AdditionalImags_meta_box_callback( ) {
	// Используем nonce для верификации
	wp_nonce_field( plugin_basename( __FILE__ ), 'AdditionalImages_noncename' );
	$default_img = get_template_directory_uri() . '/image/no-image/no-image-article.svg';
	$big_value = (int) get_post_meta(get_the_ID(),'_additional_image_big',true);
	$average_value = (int) get_post_meta(get_the_ID(),'_additional_image_average',true);
	$small_value = (int) get_post_meta(get_the_ID(),'_additional_image_small',true);
	$big_src =  (!empty($big_value)) ? wp_get_attachment_image_src( $big_value  )[0]:$default_img;
	$average_src = (!empty($average_value)) ? wp_get_attachment_image_src(  $average_value  )[0]:$default_img;;
	$small_src = (!empty($small_value)) ? wp_get_attachment_image_src( $small_value )[0]:$default_img;;
	?>
	<div>Рекомендуемые размеры формат jpg</div>
	<div>Большое 1884x384</div>
	<div class="additional-images-div">
		<img data-src="<?php echo $default_img?>" src="<?php echo $big_src?>" width="100px" height="100px" />
		<input type="hidden" name="additional_image_big" value="<?php echo $big_value?>">
		<button type="submit"  class="additional-images-upload-button button">Загрузить</button>
		<button type="submit" class="additional-images-remove-button button">&times;</button>
	</div>
	<div>Среднее 768x302</div>
	<div class="additional-images-div">
		<img data-src="<?php echo $default_img?>" src="<?php echo $average_src?>" width="100px" height="100px" />
		<input type="hidden" name="additional_image_average" value="<?php echo $average_value?>">
		<button type="submit"  class="additional-images-upload-button button">Загрузить</button>
		<button type="submit" class="additional-images-remove-button button">&times;</button>
	</div>
	<div>Маленькое 450x450</div>
	<div class="additional-images-div">
		<img data-src="<?php echo $default_img?>" src="<?php echo $small_src?>" width="100px" height="100px" />
		<input type="hidden" name="additional_image_small" value="<?php echo $small_value?>">
		<button type="submit"  class="additional-images-upload-button button">Загрузить</button>
		<button type="submit" class="additional-images-remove-button button">&times;</button>
	</div>
<?php }


// Сохраняем данные, когда пост сохраняется
function AdditionalImages_save_postdata() {
	$post_id = get_the_ID();
	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['AdditionalImages_noncename'], plugin_basename( __FILE__ ) ) ) {
		return;
	}
	// если это автосохранение ничего не делаем
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// проверяем права юзера
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	$types = ['big','average','small'];
	foreach ($types as $type){
		if(!isset($_POST['additional_image_'.$type])){
			delete_post_meta($post_id, '_additional_image_'.$type);
			continue;
		}
		update_post_meta( $post_id, '_additional_image_'.$type, sanitize_text_field($_POST['additional_image_'.$type]) );
	}

}
add_action( 'save_post', 'AdditionalImages_save_postdata' );
