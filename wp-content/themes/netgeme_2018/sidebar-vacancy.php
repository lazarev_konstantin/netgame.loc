<section class="career_form">
    <img class="career_form__img" src="<?php echo get_template_directory_uri() ?>/image/lantern.png" alt="lantern">
    <h2 class="career_form__title">Is it about you and exactly for you?</h2>
    <p class="career_form__text">Please, apply us to <a href="mailto:<?php echo HR_EMAIL ?>" aria-label="send message to NetGame Entertainment hr manager"><?php echo HR_EMAIL ?></a>
        or fill a form bellow</p>

    <div class="form_contact__full">

        <div class="form_contact__wrapper">
            <form class="contactForm_full js-formForCareer" id="sendingCV_form" enctype="multipart/form-data">
                <div class="form_contact__left">
                    <label class="contactForm_label">
                        <input class="contactForm_name" type="text" name="name" id="name" title="your name"
                               placeholder="name">
                        <span class="text_error"></span>
                    </label>
                    <label class="contactForm_label">
                        <input class="contactForm_email" name="mail" id="email" type="email" title="your email"
                               placeholder="email">
                        <span class="text_error"></span>
                    </label>
                    <label class="contactForm_label">
                        <input class="contactForm_tel" name="phone" id="phone" type="tel" title="your phone number"
                               placeholder="phone">
                        <span class="text_error"></span>
                    </label>
                    <input type="hidden" name="subject"
                           value="Job summary <?php echo get_the_title() ?> <?php echo $city ?>">
                    <label class="contactForm_label">
                        <input class="contactForm_cvLink" type="text" name="link" id="link" title="your CV link"
                               placeholder="link to portfolio or cv">
                        <span class="text_error"></span>
                    </label>
                    <div class="load">
                        <span class="contactForm_cvLoad__file">Or upload a file</span>
                        <input class="contactForm_cvLoad" type="file" title="download your resume" name="portfolio_CV"
                               accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf"
                               id="portfolio_CV"/>
                        <label for="portfolio_CV">Select file</label>
                        <button class="btn_resetCV" aria-label="delete CV" type="button"></button>
                        <span class="text_error"></span>
                    </div>

                </div>
                <div class="form_contact__right">
                    <label class="contactForm_label bl_textarea">
                                <textarea class="contactForm_message" id="cover_letter" name="cover_letter"
                                          placeholder="cover letter"
                                          title="your cover letter"></textarea>
                        <span class="text_error"></span>
                    </label>
                    <div class="contactForm_security">
                        <div class="bl_capcha">
                            <div class="g-recaptcha"></div>
                        </div>
                        <button class="contactForm_submit js-submit sendingCV_form_button" type="button" aria-label="send message Apply for position">Apply for position</button>
                    </div>

                </div>
            </form>
        </div>

        <div class="contacts_form_result"></div>
        <div class="js-sucsessContactForm"></div>
    </div>


</section>
