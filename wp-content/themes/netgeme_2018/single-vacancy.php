<?php get_header();

// https://wp-kama.ru/function/wp_get_attachment_image_src
//Размер картинки, данные о которой нужно получить. Отличаться будет ссылка. В этом параметре можно указывать предусмотренные в WordPress размеры (thumbnail, medium, large или full).
$img     = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0];
$title   = get_post()->post_title;
$content = get_post()->post_content;
if ( ! empty( get_post_custom_values( 'city_job' )[0] ) ){
	$city = get_post_custom_values( 'city_job' )[0];
} else {
	$city = 'Дополнительное поле city_job';
}

?>
    <div class="top_element top_career">

        <h1 class="title_customPage"><?php echo $title ?></h1>
        <p class="career_city"><?php echo $city ?></p>

        <picture class="mainImage_career">
            <source media="(max-width: 450px)" srcset="<?php echo get_template_directory_uri() ?>/image/career/careerTop_min.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/image/career/careerTop.jpg"  alt="NetGame Entertament responsible-gaming">
        </picture>
    </div>

    <picture class="decor_coins__a">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__a.png" alt="decor coins">
    </picture>

    <picture class="decor_coins__b">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__b_min.png" alt="decor coins">
    </picture>



    <div class="bl_career__desc">
        <div class="container">
            <article class="bl_ceo">
				<?php echo $content ?>
            </article>
        </div>
		<?php get_sidebar( 'vacancy' ); ?>
    </div>
<?php
get_footer();


