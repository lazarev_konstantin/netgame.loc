<section class="bl_leadForm"> <!-- Отрубишь класс active когда закончишь тестить -->
    <div class="bl_leadForm__top">
        <p class="bl_leadForm__text">Get the latest news and updates from NetGame Entertainment delivered straight to your inbox.</p>
        <h2 class="title_support bl_leadForm__title">Check out our presentation</h2>

        <button class="btn btn_showLeadForm" type="button"
                aria-label="write form for get presentation about company">
            <svg class="svg_letter" viewBox="0 0 21 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20.8625 1.44409L14.8962 7.41034L20.7153 13.2337C20.8915 12.9308 20.9999 12.5836 20.9999 12.2086V2.16494C20.9999 1.9104 20.9475 1.66919 20.8625 1.44409Z"/>
                <path d="M18.9457 0.110596H2.05438C1.71284 0.110596 1.39542 0.202307 1.11176 0.350414L10.08 9.31859C10.3402 9.5788 10.7922 9.5788 11.0478 9.31859L19.9739 0.396885C19.6703 0.219574 19.322 0.110596 18.9457 0.110596Z"/>
                <path d="M0.172306 1.34668C0.0626308 1.59778 0 1.87389 0 2.16502V12.2087C0 12.5777 0.105533 12.9194 0.276568 13.219L6.15849 7.33286L0.172306 1.34668Z"/>
                <path d="M13.9239 8.38302L12.0202 10.2913C11.2206 11.0818 9.92495 11.0948 9.11215 10.2913L7.13085 8.30542L1.31274 14.1194C1.54346 14.2093 1.79222 14.2632 2.05435 14.2632H18.9456C19.2014 14.2632 19.4438 14.2106 19.6698 14.1248L13.9239 8.38302Z"/>
            </svg>
            <span>EMAIL ME NOW</span>
        </button>
    </div>
    <div class="bl_leadForm__bottom">
        <form  class="contactForm_full js-leadForm" method="post" id="lead_form">
            <label class="contactForm_label">
                <input class="contactForm_name" type="text" title="your name" name="name_pdf_form"
                       placeholder="your name">
                <span class="text_error"></span>
            </label>
            <label class="contactForm_label">
                <input class="contactForm_email" type="email" title="your email" name="email_pdf_form"
                       placeholder="your email">
                <span class="text_error"></span>
            </label>
            <div class="contactForm_security">
                <div class="bl_capcha">
                <div class="g-recaptcha"></div>
                </div>
                <button class="contactForm_submit  js-submitLeadForm" type="submit" aria-label="Send user data for get presentation">Send message</button>
            </div>
        </form>
    </div>


    <div class="popup_leadForm hidden">
        <button class="btn_close btn_close__leadForm" type="button" aria-label="close popup lead form"></button>

        <div class="popup_leadForm__animation "> <!-- Сюда надо добавляеть клаасс error_form Если сообщение об отправке отрицательное  -->

            <div class="popup_leadForm__animationLetter">
                <svg class="popup_leadForm__letter" width="56" height="43" viewBox="0 0 56 43"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.736 4.159c7.477 6.39 20.598 17.631 24.454 21.136.517.473 1.073.714 1.65.714.574 0 1.129-.239 1.645-.71 3.86-3.508 16.98-14.75 24.458-21.14a1.152 1.152 0 00.16-1.58C53.23 1.456 51.931.812 50.54.812H5.139c-1.392 0-2.692.644-3.563 1.765a1.152 1.152 0 00.16 1.58zM54.421 7.621a1.122 1.122 0 00-1.21.166 3263.485 3263.485 0 00-16.215 13.946 1.149 1.149 0 00.03 1.761c4.522 3.654 11.336 8.68 16.256 12.253a1.124 1.124 0 001.18.09c.38-.196.618-.59.618-1.02V8.66c0-.446-.258-.852-.659-1.039zM2.397 35.747c4.92-3.572 11.736-8.599 16.256-12.253a1.149 1.149 0 00.031-1.762A3282.413 3282.413 0 002.467 7.788a1.128 1.128 0 00-1.21-.166A1.145 1.145 0 00.6 8.661v26.157c0 .43.238.823.617 1.019a1.123 1.123 0 001.18-.09z"/>
                    <path d="M53.689 38.863c-4.752-3.43-13.505-9.825-18.73-14.104a1.13 1.13 0 00-1.461.025A240.498 240.498 0 0031.006 27c-1.869 1.702-4.463 1.702-6.336-.002a197.014 197.014 0 00-2.49-2.214 1.123 1.123 0 00-1.46-.026C15.514 29.023 6.75 35.426 1.99 38.863a1.151 1.151 0 00-.17 1.711 4.53 4.53 0 003.319 1.47h45.4c1.25 0 2.46-.536 3.32-1.47a1.153 1.153 0 00-.17-1.71z" />
                </svg>

                <svg  class="wing-svg_animate wing-svg__left"  viewBox="0 0 29 21" xmlns="http://www.w3.org/2000/svg"><path d="M.337 7.924a1.187 1.187 0 00-.007 1.65 4.867 4.867 0 006.99.007l-1.38 1.41a1.187 1.187 0 00-.007 1.65 4.867 4.867 0 006.99.007l-1.38 1.41a1.187 1.187 0 00-.007 1.65 4.867 4.867 0 006.99.007l-1.38 1.41a1.187 1.187 0 00-.008 1.65 4.867 4.867 0 006.99.007l2.75-2.805c1.793-1.831 1.793-4.8 0-6.63L21.57 3.93a12.07 12.07 0 00-17.32 0L.338 7.924z" fill="#fff"/></svg>
                <svg  class="wing-svg_animate wing-svg__right" viewBox="0 0 29 21" xmlns="http://www.w3.org/2000/svg"><path d="M27.885 7.924c.445.454.452 1.194.007 1.65a4.867 4.867 0 01-6.99.007l1.38 1.41c.445.454.452 1.194.007 1.65a4.867 4.867 0 01-6.99.007l1.38 1.41c.446.454.452 1.194.008 1.65a4.867 4.867 0 01-6.991.007l1.38 1.41c.446.454.452 1.194.008 1.65a4.867 4.867 0 01-6.99.007l-2.749-2.805c-1.793-1.831-1.793-4.8 0-6.63L6.652 3.93a12.07 12.07 0 0117.32 0l3.913 3.995z" fill="#fff"/></svg>

                <svg class="popup_leadForm__wing-lines" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <symbol id="wing-lines-svg">
                        <rect x="5.72" y="13.24" width="8.84" height="1.56" rx=".78" transform="rotate(-90 5.72 13.24)"/>
                        <rect x="11.44" y="9.6" width="8.84" height="1.56" rx=".78" transform="rotate(-90 11.44 9.6)"/>
                        <rect y="9.6" width="8.84" height="1.56" rx=".78" transform="rotate(-90 0 9.6)" />
                    </symbol>
                </svg>
                <svg  class="wing-lines-svg_animate wing-lines-svg_1"  viewBox="0 0 13 14" xmlns="http://www.w3.org/2000/svg"><use xlink:href="#wing-lines-svg"/></svg>
                <svg  class="wing-lines-svg_animate wing-lines-svg_2"  viewBox="0 0 13 14" xmlns="http://www.w3.org/2000/svg"><use xlink:href="#wing-lines-svg"/></svg>
                <svg  class="wing-lines-svg_animate wing-lines-svg_3"  viewBox="0 0 13 14" xmlns="http://www.w3.org/2000/svg"><use xlink:href="#wing-lines-svg"/></svg>
            </div>
        </div>
    </div>

</section>