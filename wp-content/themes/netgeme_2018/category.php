<?php
/**
 * Template Name: Insights
 * Template Post Type: page
 */
global $wp_query;
get_header();
$type = null;
$type_arg = [];
$permissive_types = ['events', 'releases'];
if (in_array($_GET['type'], $permissive_types)) {
    $tag = $tag = get_term_by('slug', $_GET['type'], 'post_tag');
    if ($tag) {
        $type = '?type=' . $_GET['type'];
        $type_arg = ['tag' => $tag->slug];
    }
}
$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
$arg = array(
    'cat' => get_queried_object()->term_id,
    'posts_per_page' => get_option('posts_per_page'),
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => $paged
);
$arg = array_merge($arg, $type_arg);
$the_query = new WP_Query($arg);
?>


    <div class="top_element top_element__news">
        <picture class="mainImage_Insights">
            <source media="(max-width: 500px)"
                    srcset="<?php echo get_template_directory_uri(); ?>/image/insights/top-picture/insights_netgame_500.jpg">
            <source media="(max-width: 768px)"
                    srcset="<?php echo get_template_directory_uri(); ?>/image/insights/top-picture/insights_netgame_768.jpg">
            <source media="(max-width: 1800px)"
                    srcset="<?php echo get_template_directory_uri(); ?>/image/insights/top-picture/insights_netgame_1800.jpg">
            <img src="<?php echo get_template_directory_uri(); ?>/image/insights/top-picture/insights_netgame.jpg"
                 alt="NetGame Entertainment Insights">
        </picture>

        <h1 class="title_news"><?php single_cat_title() ?></h1>
        <img class="bl_news__titleSvg" src="<?php echo get_template_directory_uri(); ?>/image/insights/news_top.svg"
             alt="NetGame Entertainment Insights main text">


    </div>
    <ul class="bl_breadcrumbs" itemscope="" itemtype="https://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
            <a itemtype="https://schema.org/Thing" itemscope="" itemprop="item" id="home-page"
               href="/"><span itemprop="name">Home</span></a>
            <meta itemprop="position" content="1">
        </li>
        <li class="uk-active" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                    <span itemtype="https://schema.org/Thing" itemscope="" itemprop="item"
                          id="<?php echo get_queried_object()->name ?>"
                          title="Insights"><span itemprop="name"><?php echo get_queried_object()->name ?></span></span>
            <meta itemprop="position" content="2">
        </li>
    </ul>
    <div id="insights" class="bl_news__full">
        <div class="container">

            <button class="btn_filtersMobile" type="button" aria-label="button work with filters">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 40 40"
                     style="enable-background:new 0 0 40 40;" xml:space="preserve">
                            <style type="text/css">.c_svg {
                                    fill: #00E676;
                                    stroke: #000;
                                }</style>
                    <path d="M10 12.5h20c.6 0 1 .4 1 1s-.4 1-1 1H10c-.6 0-1-.4-1-1s.4-1 1-1zM10 19.5h20c.6 0 1 .4 1 1s-.4 1-1 1H10c-.6 0-1-.4-1-1s.4-1 1-1zM10 26.5h20c.6 0 1 .4 1 1s-.4 1-1 1H10c-.6 0-1-.4-1-1s.4-1 1-1z"/>
                    <circle class="c_svg" cx="24.5" cy="13" r="2"/>
                    <circle class="c_svg" cx="14.5" cy="20.5" r="2"/>
                    <circle class="c_svg" cx="24.5" cy="27" r="2"/>
                        </svg>
            </button>

            <div class="bl_news__filters ">
                <button class="btn btn_filterNews <?php echo (!in_array($_GET['type'], $permissive_types)) ? 'active' : null ?>"
                        type="button" data-filter="<?php echo get_category_link(get_queried_object()->cat_ID) ?>"
                        aria-label="button filter All news of Netgame Entertainment">All news
                </button>
                <button class="btn btn_filterNews <?php echo ($_GET['type'] == 'events') ? 'active' : null ?>"
                        type="button"
                        data-filter="<?php echo get_category_link(get_queried_object()->cat_ID) ?>?type=events"
                        aria-label="button filter Events of Netgame Entertainment">Events
                </button>
                <button class="btn btn_filterNews <?php echo ($_GET['type'] == 'releases') ? 'active' : null ?>"
                        type="button"
                        data-filter="<?php echo get_category_link(get_queried_object()->cat_ID) ?>?type=releases"
                        aria-label="button filter Releases of Netgame Entertainment">Releases
                </button>
            </div>
            <ul class="bl_news">
                <?php if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $the_query->the_post(); ?>
                        <li class="bl_news__item">
                            <picture class="bl_news__image">
                                <?php $img = get_the_post_thumbnail_url(get_post());
                                if (!$img) {
                                    $img = get_template_directory_uri() . '/image/no-image/no-image-article.svg';
                                } ?>
                                <img src="<?php echo $img; ?>" width="288" height="180" alt="<?php echo get_the_title() ?>">
                            </picture>

                            <div class="bl_news__information">
                                <h2 class="bl_news__title"><?php echo get_the_title() ?></h2>
                                <time class="bl_news__time"
                                      datetime="<?php echo get_the_date('Y-m-d') ?> "><?php echo get_the_date(get_option('date_format')) ?></time>
                                <div class="bl_news__shortDesc">
                                    <?php
                                    $short_description = get_post_meta(get_post()->ID, "short_description", true);
                                    ?>
                                    <p><?php if (!empty($short_description)) {
                                        echo $short_description;
                                        } else {
                                        echo "<span style='color:#f00;font-size: 20px'>You forgot write <b>short_description</b> field in current post</span>";
                                        } ?>
                                    </p>
                                </div>
                            </div>
                            <a class="btn btn_readNew" href="<?php echo get_permalink() ?>">Read more</a>
                        </li>
                    <?php }
                    wp_reset_postdata();
                } ?>
            </ul>
            <!--    настройка количества записей из админки  -->
            <?php
            $big = 999999999;
            $pagination_args = array(
                'base' => get_pagenum_link(1) . '%_%',
                'format' => 'page/%#%/',
                'show_all' => false,
                'current' => max(1, get_query_var('paged')),
                'total' => $the_query->max_num_pages,
                'end_size' => 1,
                'mid_size' => 1,
                'prev_text' => __('<svg width="15" height="27" viewBox="0 0 15 27" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.9,13.2L0.9,13.2l13,13.2l1.1-1.1L2.9,13.2L15,1.1L13.9,0L0.9,13.2L0.9,13.2z"/>
                    </svg>'),
                'next_text' => __(' <svg width="15" height="27" viewBox="0 0 15 27" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15 13.1L1.9 0 .807 1.092l12.106 12.106L.808 25.303l1.091 1.092L15 13.295l-.097-.097L15 13.1z"/>
                    </svg>'),
                'type' => 'array',
                'add_fragment' => $type,
            );
            $paginate_links = paginate_links($pagination_args);
            if ($paginate_links) {
                echo '<div class="bl_pagination">';

                foreach ($paginate_links as $key => $value) {
                    $value = str_replace('page-numbers', 'bl_pagination__item', $value);
                    $value = str_replace('current', 'active', $value);
                    $value = str_replace_once($type, '', $value);
                    echo $value;
                }

                echo '</div>';
                ?>
            <?php }
            wp_reset_query();
            ?>
        </div>
    </div>
<?php
get_footer();
