<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php // Генерируем тайтл в зависимости от контента с разделителем " | "
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            echo " | $site_description";
        }
        ?>
    </title>
    <?php
    if (is_home() || is_front_page()) {
        $post = get_post(get_option('page_on_front'));
        setup_postdata($post);
    }
    wp_head(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/image/favicon/favicon.ico"
          type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/image/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo get_template_directory_uri(); ?>/image/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo get_template_directory_uri(); ?>/image/favicon/apple-touch-icon-114x114.png">

</head>

<body class="body">
<div class="wrapperBlock">

    <div class="main_content">
        <?php
        $new_page = get_post()->post_name;
        $active = null;
        $home_page = null;

        if (is_front_page() || is_home() || $new_page == 'home') {
            $active = 'active';
            $home_page = 'home_page';
        }

        ?>
        <header id="header" class="header <?php echo $home_page ?>">
            <?php unset($home_page) ?>

            <button class="btn_menu" type="button" aria-label="menu NetGame Entertainment">
                <svg class="ham" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="40" height="40">
                    <path class="line top"
                          d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40"></path>
                    <path class="line middle" d="m 30,50 h 40"></path>
                    <path class="line bottom"
                          d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40"></path>
                </svg>
            </button>
            <div class="header_wrapper">
                <div class="container">
                    <?php if (is_front_page() || is_home() || $new_page == 'home') {
                        echo '<div class="logo_link" aria-label="logo-NetGame-Entertainment">
                        <picture>
                            <source media="(min-width: 2100px)" srcset="' . get_template_directory_uri() . '/image/logoNetgame/netgame_entertament_logo_max_2100.png">
                            <img src="' . get_template_directory_uri() . '/image/logoNetgame/netgame_entertament_logo_max.png" alt="Netgame entertaiment">
                        </picture>
                        </div>';
                    } else {
                        echo '<a class="logo_link" aria-label="logo NetGame Entertainment" href="/">
                        <svg id="netgamenv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 290 250">
                            <symbol id="netgamenv_logo">
                                <path id="Entertainment-text"
                                      d="M60.7 237h2v-11.8l11.9 12.5v-16.5h-2v11.7l-11.9-12.5V237zM46 237h8.5v-1.8H48v-6h6.4v-1.8H48V223h6.5v-1.8H46V237zm41.4-14.1h-3.8v14h-2v-14h-3.8v-1.8h9.5v1.8h.1zm4.5 14.1h8.5v-1.8h-6.5v-6h6.4v-1.8h-6.4V223h6.5v-1.8h-8.5V237zm23.4-11.4c0 2.3-1.5 4.2-3.8 4.4l5 6.9h-2.4l-4.7-6.7h-.8v6.7h-2V221h2.4c1.4 0 2.9 0 4.2.7 1.4.9 2.1 2.4 2.1 3.9zm-6.1-2.7h-.5v5.7h.6c2 0 4.2-.4 4.2-2.9s-2.4-2.8-4.3-2.8zm16.1 0h3.8v-1.8h-9.5v1.8h3.8v14h2v-14h-.1zm19.2 14.1l-1.8-4h-7.3l-1.8 4h-2.1l7.6-16.5 7.6 16.5h-2.2zm-5.4-12.2l-2.8 6.4h5.6l-2.8-6.4zm13.7-3.7h-2V237h2v-15.9zm7.1 15.9h-2v-16.6l11.9 12.5v-11.7h2v16.5l-11.9-12.5V237zm26.3.3l4.8-10.8 1.9 10.5h2l-3.2-16.5-5.4 12.5-5.4-12.5-3.2 16.5h2l1.9-10.5 4.6 10.8zm23-.3h-8.5v-15.9h8.5v1.8h-6.5v4.4h6.4v1.8h-6.4v6h6.5v1.9zm4.4 0h2v-11.8l11.9 12.5v-16.5h-2v11.7l-11.9-12.5V237zm27.7-14.1h-3.8v14h-2v-14h-3.8v-1.8h9.5v1.8h.1z"/>
                                <path id="NetGame-text" class="netgamenv_white"
                                      d="M20.6,201.6h4.9v-29.2L55,203.3v-40.8h-4.9v29l-29.5-30.9V201.6z M61.6,201.6h21.1v-4.5
			H66.5v-14.8h15.8v-4.5H66.5v-11h16.2v-4.5H61.6V201.6z M112.9,166.9h-9.3v34.8h-4.9v-34.8h-9.3v-4.5H113L112.9,166.9L112.9,166.9z
			 M137.4,181.8v4.5h10.2c-0.2,6.4-6.8,11.7-12.9,11.7c-8.5,0-15.3-7.8-15.3-16c0-8.8,6.9-15.9,15.7-15.9c4.8,0,9.5,2.5,12.4,6.4
			l3.4-3.3c-3.9-4.8-9.9-7.6-16-7.6c-11.3,0-20.4,9.3-20.4,20.5c0,10.9,8.9,20.3,19.9,20.3s18.6-8.3,18.6-19v-1.6L137.4,181.8
			L137.4,181.8z M188.2,201.6l-4.5-9.8h-18l-4.5,9.8h-5.3l18.8-40.9l18.8,40.9H188.2z M175.6,171.4l-7,15.9h14L175.6,171.4z
			 M219.4,202.4l11.8-26.7h0.1l4.6,25.9h5.1l-8-40.9l-13.5,30.9L206,160.7l-8,40.9h5.1l4.6-25.9h0.1L219.4,202.4z M268.6,201.6
			h-21.1v-39.2h21.1v4.5h-16.2v11h15.8v4.5h-15.8v14.8h16.2V201.6z"/>
                                <g class="netgament_element__bottom">
                                    <path class="netgamenv_harlequin" d="M145.7 117.5l25.6 25.9h-51.2l25.6-25.9z"/>
                                    <path class="netgamenv_lime" d="M145.7 117.5v25.9h-25.6l25.6-25.9z"/>
                                </g>
                                <symbol id="netgament_element">
                                    <path class="netgamenv_harlequin"
                                          d="M145.1 9L190 54.4l-44.8 45.4-44.9-45.4L145.1 9z"/>
                                    <path class="netgamenv_malachite" d="M145.1 54.4H190l-44.8 45.4-44.9-45.4h44.8z"/>
                                    <path class="netgamenv_lime" d="M145.1 9v90.8l-44.8-45.4L145.1 9z"/>
                                    <path class="netgamenv_mint" d="M124.1 54.4h21v45.4l-44.8-45.4h23.8z"/>
                                    <path class="netgamenv_white"
                                          d="M134.1 54.4s3.3-1.1 6.6-4.4c3.3-3.3 4.4-6.6 4.4-6.6s1.2 3.5 4.4 6.6c3.2 3.2 6.6 4.4 6.6 4.4s-3.3.8-6.6 4.4c-3.3 3.6-4.4 6.6-4.4 6.6s-.6-2.2-3.9-6.1c-3.3-3.8-7.1-4.9-7.1-4.9z"/>
                                    <path fill="#000"
                                          d="M145.1 25.8l28.6 28.6L145.1 83l-28.6-28.6 28.6-28.6zm0 8.5L125 54.4l20.1 20.1 20.1-20.1-20.1-20.1z"/>
                                </symbol>
                                <use xlink:href="#netgament_element" x="0" y="0"/>
                                <use xlink:href="#netgament_element" x="101" y="85" transform="scale(0.77)"/>
                                <use xlink:href="#netgament_element" x="-13" y="85" transform="scale(0.77)"/>
                            </symbol>
                            <use xlink:href="#netgamenv_logo" x="0" y="0"/>
                        </svg>
                    </a>';
                    } ?>

                    <nav class="bl_nav" aria-label="navigation of NetGame Entertainment">
                        <svg class="bl_nav__logo" xmlns="http://www.w3.org/2000/svg" width="66" viewBox="0 0 290 250">
                            <use xlink:href="#netgamenv_logo__min" x="0" y="0"/>
                        </svg>

                        <?php  wp_nav_menu( [
                                'theme_location'  => 'header_menu',
                                'container'       => false,
                                'fallback_cb'     => '',
                                'echo'            => 1,
                                'container_class' => '',
                                'menu_class'      => 'bl_nav__full',
                                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                'depth'           => 1,
                            ]
                        );
                        ?>

                    </nav>
                </div>
            </div>
        </header>

        <main class="main">
