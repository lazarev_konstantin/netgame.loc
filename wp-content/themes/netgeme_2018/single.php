<?php
get_header();
$postID = $post->ID;
$category = get_the_category($postID);

$current_cat_id = $category[0]->cat_ID;
$current_cat_name = $category[0]->name;
update_post_meta($postID, '_count_reviews', (int)get_post_meta(get_the_ID(), '_count_reviews', true) + 1);


$title = get_post()->post_title;
$url = get_permalink(); // ссылка на страницу
$currentTag = 'Netgame';
$facebookCode ='703209607093847';

$summary = get_post_meta(get_post()->ID, "short_description", true); ;
$image_url = wp_get_attachment_url(get_post_thumbnail_id()); // URL изображения
?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0&appId=703209607093847&autoLogAppEvents=1"></script>

    <div class="top_element top_element__newsSingle">

        <!--     Сюда выводить изображения из админки . Создать 3 вида под разный устройства. Рекомендуемые размеры   1884x384 768x302 450x450 формат jpg -->
        <picture class="mainImage_news">
            <source media="(max-width: 450px)"
                    srcset="<?php echo wp_get_attachment_image_src(get_post_meta(get_the_ID(), '_additional_image_small', true), 'full')[0]; ?>">
            <source media="(max-width: 830px)"
                    srcset="<?php echo wp_get_attachment_image_src(get_post_meta(get_the_ID(), '_additional_image_average', true), 'full')[0]; ?>">
            <img src="<?php echo wp_get_attachment_image_src(get_post_meta(get_the_ID(), '_additional_image_big', true), 'full')[0]; ?>"
                 alt="<?php echo $title ?>">
        </picture>

        <div class="single_new__top">
            <h1 class="title_newsSingle"><?php echo $title ?></h1>
            <time class="singleNew__time"
                  datetime="<?php echo get_the_date('Y-m-d') ?>"><?php echo get_the_date(get_option('date_format')) ?></time>
        </div>
    </div>

    <ul class="bl_breadcrumbs bl_breadcrumbs__singleNew" itemscope="" itemtype="https://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
            <a itemtype="https://schema.org/Thing" itemscope="" itemprop="item" id="mainPage" href="/">
                <span itemprop="name">Home</span>
            </a>
            <meta itemprop="position" content="1">
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
            <a itemtype="https://schema.org/Thing" itemscope="" itemprop="item" id="insights"
               href="<?php echo get_category_link($current_cat_id); ?>">
                <span itemprop="name"><?php echo $current_cat_name ?></span>
            </a>
            <meta itemprop="position" content="2">
        </li>
        <li class="uk-active" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
            <span itemtype="https://schema.org/Thing" itemscope="" itemprop="item" id="singleNew"
                  title="<?php echo get_post()->post_title ?>">
                <span itemprop="name"><?php echo get_post()->post_title ?></span>
            </span>
            <meta itemprop="position" content="3">
        </li>
    </ul>
    <div class="contant_wrapper">
        <div class="container">
            <article class="singleNew_article">
                <div class="bl_ceo"><?php echo get_post()->post_content ?></div>

                <div class="singleNew_info">

                    <div class="singleNew_sharing">

                        <a class="btn_sharing btn_sharing__fb" href="https://www.facebook.com/sharer/sharer.php?app_id=<? echo $facebookCode?>&sdk=joey&u=<?php echo $url ?>&p[summary]=<?php echo $summary ?>&display=popup&ref=plugin&src=share_button" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                        <span class="share-button-secondary">
                              <span class="share-button-secondary-content">share on Facebook</span>
                        </span>
                        </a>

                        <a class="btn_sharing btn_sharing__tw"
                           href="http://twitter.com/share?text=<?php echo $title ?>&url=<?php echo $url ?>&hashtags=<?php echo $currentTag ?>"
                           title="Sharing post in Twitter"
                           onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false"
                           target="_parent">
                         <span class="share-button-secondary">
                              <span class="share-button-secondary-content">share on Twitter</span>
                        </span>
                        </a>


                        <a class="btn_sharing btn_sharing__linkedin"
                           href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $url ?>&amp;title=<?php echo $title ?>&p[summary]=<?php echo $summary ?>"
                           onclick="window.open(this.href, 'share-linkedin', 'width=580,height=296');return false;"
                           title="Share on LinkedIn">
                        <span class="share-button-secondary">
                            <span class="share-button-secondary-content">share on LinkedIn</span>
                        </span>
                        </a>


                        <button class="btn_workWithSharing" type="button" aria-label="button sharing buttons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                                <path d="M32.4 18.3l-10.1-10v6h-2.2c-6.6 0-12 4.9-12 10.9v3.1l.9-.9c3.2-3.2 7.8-5.1 12.7-5.1h.7v6l10-10z"/>
                            </svg>
                        </button>
                    </div>
                </div>

                <span class="bl_reviews"><?php echo (int)get_post_meta(get_the_ID(), '_count_reviews', true) ?></span>

            </article>
        </div>
    </div>

    <div class="singleNew_couple">
        <div class="container">
            <?php
            if (isset($category[0])) {
                $next_post_q = new WP_Query(
                    array(
                        'post_type' => 'post',
                        'cat' => $category[0]->term_id,
                        'posts_per_page' => -1
                    ));

                if ($next_post_q->have_posts()) {
                    for ($i = 0; $i < count($next_post_q->get_posts()); $i++) {
                        $this_post = $next_post_q->get_posts()[$i];
                        if ($this_post->ID == get_the_ID()) {

                            if (isset($next_post_q->get_posts()[$i + 1])) {
                                $next_post = $next_post_q->get_posts()[$i + 1];
                            } else {
                                $next_post = $next_post_q->get_posts()[0];
                            }
                            if (isset($next_post_q->get_posts()[$i - 1])) {
                                $prev_post = $next_post_q->get_posts()[$i - 1];
                            } else {
                                $prev_post = $next_post_q->get_posts()[count($next_post_q->get_posts()) - 1];
                            }
                        }
                    }
                }
            } else {
                $next_post = get_next_post(true);

                $prev_post = get_previous_post(true);
            }
            if ($prev_post) {
                echo '<a class="singleNew_couple__prev" href="' . wp_make_link_relative(get_permalink($prev_post->ID)) . '" rel="prev">' . $prev_post->post_title . '</a>';
            }
            if ($next_post) {
                echo '<a class="singleNew_couple__next" href="' . wp_make_link_relative(get_permalink($next_post->ID)) . '" rel="prev">' . $next_post->post_title . '</a>';

            } ?>
        </div>
    </div>

<?php get_sidebar('leadform');

get_footer();
