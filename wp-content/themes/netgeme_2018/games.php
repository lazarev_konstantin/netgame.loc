<?php
/**
 * Template Name: Games Template Page
 * Template Post Type: page
 */
get_header();


$games_page    = get_post();
$my_query_args = array(
	'post_type'      => 'games',
	'post_status'    => 'publish',
	'posts_per_page' => - 1,
	'meta_query'     => array(
		array(
			'key' => 'sort_place',
		)
	),
	'meta_key'       => 'sort_place',
	'orderby'        => 'meta_value_num',
	'order'          => 'ASC'
);

$my_query = new WP_Query( $my_query_args );

$pageGame_title = get_post_custom_values( 'pageGame_title' )[0];

?>
    <div class="top_element top_games">

        <h1 class="title_customPage"><?php echo $pageGame_title; ?></h1>

        <picture class="mainImage_career">
            <source media="(max-width: 450px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/gamesPage/top_GamePage_min.jpg">
            <source media="(max-width: 768px)"
                    srcset="<?php echo get_template_directory_uri() ?>/image/gamesPage/top_GamePage_middle.jpg">
            <img src="<?php echo get_template_directory_uri() ?>/image/gamesPage/top_GamePage.jpg"
                 alt="NetGame Entertament games pictures">
        </picture>

    </div>

    <picture class="decor_coins__a">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__a.png" alt="decor coins">
    </picture>

    <picture class="decor_coins__b">
        <img src="<?php echo get_template_directory_uri() ?>/image/decor_coins__b_min.png" alt="decor coins">
    </picture>

    <div class="bl_fullGames page-games">
        <div class="container">
            <ul class="bl_games__wrapper">
				<?php if ( $my_query->have_posts() ) {
					while ( $my_query->have_posts() ) {
						$my_query->the_post();
						$ID                                  = get_the_ID();
						$post                                = get_post( $ID );
						$game                                = $post;
						$home_page_header_game_images_id     = get_post_custom_values( 'home_page_header_game_images', $game->ID )[0];
						$home_page_header_big_game_images_id = get_post_custom_values( 'home_page_header_big_game_images', $game->ID )[0];
						$game_href                           = get_post_custom_values( 'href_run_demo', $game->ID )[0];
						$big_img                             = wp_get_attachment_image_src( $home_page_header_big_game_images_id, 'full', true )[0];
						$img                                 = wp_get_attachment_image_src( $home_page_header_game_images_id, 'full', true )[0];
						$img_webP                            = str_replace( array( 'png', 'jpg' ), 'webp', $img );
						if ( ! empty( get_post_custom_values( 'header_short_description', $game->ID )[0] ) ) {
							$short_description = get_post_custom_values( 'header_short_description', $game->ID )[0];
						} else {
							$short_description = 'Дополнительное поле short_description';
						}
						if ( has_tag( 'coming-soon', $game->ID ) ) {
							continue;
							$img = get_template_directory_uri() . '/image/slot-games/comming-soon.jpg'
							?>
                            <li class="bl_games__item">
                                <div class="bl_games__item_wrapper">
                                    <p class="bl_games__name"><?php echo $post->post_title; ?></p>
                                    <p class="bl_games__desc"><?php echo $short_description; ?></p>
                                    <picture class="bl_games__image">

										<?php if ( class_exists( '\Netgeme\WebPImg' ) ) {
											echo ( new \Netgeme\WebPImg( $home_page_header_big_game_images_id ) )->getWeb( [ 'media="(min-width: 2400px)"' ] );
										} ?>
                                        <source media="--large"
                                                srcset="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                                data-srcset="<?php echo $big_img ?>">
										<?php if ( class_exists( '\Netgeme\WebPImg' ) ) {
											echo ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
										} ?>
                                        <source srcset="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                                data-srcset="<?php echo $img ?>">
                                        <img class="lazyload"
                                             src="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                             data-src="<?php echo $img ?>"
                                             alt="<?php echo $post->post_title; ?>">
                                    </picture>
                                </div>
                            </li>
						<?php } else {
							?>
                            <li class="bl_games__item">

                                <button class="btn_gameInfo" type="button"><span class="btn_gameInfo__inner"
                                                                                 aria-label="button show short information about <?php echo $post->post_title; ?>"></span>
                                </button>
                                <a class="bl_games__link_inner" href="<?php echo get_permalink() ?>"
                                   aria-label="show full information about <?php echo $post->post_title; ?>">
                                    <picture class="bl_games__image">

										<?php if ( class_exists( '\Netgeme\WebPImg' ) ) {
											echo ( new \Netgeme\WebPImg( $home_page_header_big_game_images_id ) )->getWeb( [ 'media="(min-width: 2400px)"' ] );
										} ?>

                                        <source media="--large"
                                                srcset="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                                data-srcset="<?php echo $big_img ?>">

										<?php if ( class_exists( '\Netgeme\WebPImg' ) ) {
											echo ( new \Netgeme\WebPImg( $home_page_header_game_images_id ) )->getWeb();
										} ?>
                                        <source srcset="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                                data-srcset="<?php echo $img ?>">
                                        <img class="bl_games__image lazyload"
                                             src="<?php echo get_template_directory_uri(); ?>/image/svg-elements/preloadImage.svg"
                                             data-src="<?php echo $img ?>"
                                             alt="<?php echo $post->post_title; ?>">
                                    </picture>
                                </a>
                                <div class="bl_games__item_wrapper">
                                    <p class="bl_games__name"><?php echo $post->post_title; ?></p>
                                    <p class="bl_games__desc"><?php echo $short_description; ?></p>

									<?php if ( ! empty( $game_href ) ) {
										echo '<button class="btn_playGame js-playGame" data-href="' . $game_href . '" type="button" aria-label="button play demo version of ' . $post->post_title . '" >Run Demo</button>';
									}; ?>
                                    <a class="btn_lookAtGame" href="<?php echo get_permalink() ?>"
                                       aria-label="show full information about <?php echo $post->post_title; ?>"
                                       rel="nofollow">Game info<?php if ( ! empty( $game_href ) ) {
											echo '& Demo';
										}; ?></a>
                                </div>
                            </li>
							<?php
						}
					}
				}
				wp_reset_query();
				?>
            </ul>
            <div class="bl_fullGames__description">
                <article class="bl_ceo">
					<?php
					wp_reset_query();
					echo get_post()->post_content ?>
                </article>
            </div>
        </div>
    </div>
<?php get_sidebar( 'contacts' );
get_footer();


