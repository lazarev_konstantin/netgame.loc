<?php
/**
 * Template Name: Privacy-policy
 * Template Post Type: page
 */

get_header(); ?>

    <div class="top_element top_element__policy">

        <picture class="mainImage_policy">
            <source media="(max-width: 450px)" srcset="<?php echo get_template_directory_uri(); ?>/image/privacy-policy/privacy-policy_netgame_min.jpg">
            <source media="(max-width: 830px)" srcset="<?php echo get_template_directory_uri(); ?>/image/privacy-policy/privacy-policy_netgame_middle.jpg">
            <img src="<?php echo get_template_directory_uri(); ?>/image/privacy-policy/privacy-policy_netgame.jpg"  alt="NetGame Entertainment private policy">
        </picture>

        <h1 class="title_customPage"><?php echo get_post()->post_title ?></h1>

    </div>


    <div class="contant_wrapper">
        <div class="container">
            <div class="bl_ceo">
                <?php echo get_post()->post_content ?>
            </div>
        </div>
    </div>


<?php get_footer();
