<?php
/**
 * Template Name: PDF
 * Template Post Type: page
 */
$key = $_GET['key'];
try {
	$pdf  = new \Netgeme\PdfPresentation();
	$file = $pdf->getFile( $key );
	if ( $file ) {
		header( "Content-type:application/pdf" );
		header( "Content-Disposition:attachment;filename=Presentation.pdf" );
		header( 'Cache-control: private' );
		readfile( "{$file}" );
	}
} catch ( Exception $exception ) {
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	nocache_headers();
	include( get_query_template( '404' ) );
	exit();
}
?>
    <script>
        window.close();
    </script>
<?php exit();
