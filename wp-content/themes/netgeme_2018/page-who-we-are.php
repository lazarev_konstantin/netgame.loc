<?php
/**
 * Template Name: Who-we-are
 * Template Post Type: page
 */

get_header();

$postID = $post->ID;
$netgame_motto = get_post_meta( $postID, 'netgame_motto', true );
?>


    <div class="top_element top_element__whoWeAre">

        <picture class="mainImage_whoWeAre">
            <source media="(max-width: 500px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_500.webp" type="image/webp">
            <source media="(max-width: 500px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_500.jpg">
            <source media="(max-width: 768px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_768.webp" type="image/webp">
            <source media="(max-width: 768px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_768.jpg">
            <source media="(max-width: 1800px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_1800.webp" type="image/webp">
            <source media="(max-width: 1800px)" srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background_1800.jpg">
            <source srcset="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background.webp" type="image/webp">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who-we-are-background.jpg" alt="NetGame Entertainment who-we-are" width="1884" height="610" loading="eager">
        </picture>

        <div class="mainBlock_whoWeAre">
            <h1 class="mainBlock_whoWeAre__title"><?php echo get_post()->post_title ?></h1>
            <img class="mainBlock_whoWeAre__phrase"
                 src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/who_we_are.svg"
                 alt="NetGame Entertainment who-we-are main text">
            <p class="mainBlock_whoWeAre__subText"><?php echo $netgame_motto; ?></p>

        </div>


        <picture class="whoWeAre_lantern whoWeAre_lantern__1">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>

        <picture class="whoWeAre_lantern whoWeAre_lantern__2">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>


        <picture class="whoWeAre_lantern whoWeAre_lantern__3">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>


        <picture class="whoWeAre_lantern whoWeAre_lantern__4">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>


    </div>


    <article class="whoWeAre_story">
        <div class="container">
            <h2 class="whoWeAre_title">The NetGame Story</h2>

            <div class="bl_ceo whoWeAre_story__ceo">

                <?php echo get_post()->post_content ?>

            </div>
        </div>
    </article>


    <article class="whoWeAre_mission">

        <div class="whoWeAre_mission__wrapper">
            <div class="whoWeAre_mission__description">
                <h2 class="whoWeAre_title whoWeAre_mission__title">Our Mission</h2>
                <img class="whoWeAre_mission__image"
                     src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/background/image_our-mission.svg"
                     width="344" height="40" loading="lazy"
                     alt="text image Our mission">
                <p>We create games with skill and passion.</p>
                <p>We strive to make every player’s experience exhilarating & unforgettable.</p>
            </div>
        </div>


        <picture class="whoWeAre_lantern whoWeAre_lantern__5">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>


        <picture class="whoWeAre_lantern whoWeAre_lantern__6">
            <img src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/launter.png" alt="lantern design">
        </picture>


    </article>


    <article class="whoWeAre_about">
        <div class="container">


            <div class="whoWeAre_about__section whoWeAre_about__section1">

                <div class="whoWeAre_partImg animate-left">
                    <video class="whoWeAre_video__leprechaun" loop="" muted="" autoplay="">
                        <source src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/leprechaun.mp4"
                                type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
                    </video>
                </div>


                <div class="whoWeAre_partText animate-right">
                    <h2 class="whoWeAre_title whoWeAre_title__left">Our Approach</h2>

                    <p>When creating our games, we take inspiration from real brick and mortar casino. The themes that
                        make you feel like you’re really there are central to us. And since, we’re working on growing
                        our brand globally, adding some special features and themes from the Asian, European, LATAM, and
                        CIS markets have been our focus.</p>
                    <p>The concept of making NetGame`s slots an amazing playing experience lies in creating games that
                        use original mathematical models, exciting art, and unique bonus mechanics. Almost every slot
                        has a Free Spins feature or something else exciting built-in, like Progressive Jackpots,
                        Tumbling reels, Pick'em Bonuses, Re-Spins, Fortune Wheels, Win Both Ways, and more.</p>

                </div>

            </div>


            <div class="whoWeAre_about__section whoWeAre_about__section2">

                <div class="whoWeAre_partImg animate-right">
                    <video class="whoWeAre_video__zeus" loop="" muted="" autoplay="">
                        <source src="<?php echo get_template_directory_uri(); ?>/image/who-we-are/zeus.mp4"
                                type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
                    </video>
                </div>


                <div class="whoWeAre_partText animate-left">
                    <h2 class="whoWeAre_title whoWeAre_title__left">Our Values</h2>
                    <p>We aim to create slots that reflect the diverse group of players who play our games. It’s our mission to make every player feel welcome and that they are represented and heard. Whoever you are and wherever you’re from, NetGame Entertainment is building something for you that we can be proud of.</p>
                    <p>We promise to pursue outstanding products, invest in our players’ passions while building diverse content, and put all of our skills into creating experiences for everyone.</p>
                </div>

            </div>

        </div>

    </article>

<?php get_sidebar('contacts'); ?>


<?php get_footer();
