<?php


namespace Netgeme;


class WebPImg {

	/**
	 * @var int|string
	 */
	protected $attachment_id;
	/**
	 * @var string
	 */
	protected $size = 'full';
	protected  $media;

	/**
	 * WebPImg constructor.
	 *
	 * @param $attachment_id int|string
	 * @param string $size
	 */
	public function __construct( $attachment_id, $size = 'full' ) {
		if ( in_array( $size, [ 'thumbnail', 'medium', 'large', 'full' ] ) ) {
			$this->size = $size;
		}
		if ( ! empty( (int) $attachment_id ) ) {
			$this->attachment_id = $attachment_id;
		}

	}

	/**
     * @param $addParams array
     *
	 * @return string|null
	 */
	public function getWeb($addParams=[]) {
		if ( empty( $this->attachment_id ) ) {
			return null;
		}
		$this->media = null;
		if(is_array($addParams)){
		    $this->media = implode(' ',$addParams);
        }
		if ( $this->existWebP() ) {
			return '<source '.$this->media.' srcset="' . $this->getWebPSrc() . '" type="image/webp">';
		}

		return null;
	}


	/**
	 * @return bool
	 */
	protected function existWebP() {
		if ( empty( $this->attachment_id ) ) {
			return false;
		}
		if ( ! empty( $this->getWebPSrc() ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool|string
	 */
	protected function getImg() {
		$imgSrc     = wp_get_attachment_image_src( $this->attachment_id, $this->size, true )[0];
		$img        = str_replace( get_home_url() . '/', ABSPATH, $imgSrc );
		$arr_format = [ 'image/png', 'image/jpeg' ];
		$format     = @mime_content_type( $img );

		return ( is_file( $img ) && in_array( $format, $arr_format ) ) ? $img : false;
	}

	/**
	 * @return string|null
	 */
	protected function getWebPSrc() {
		$webP = null;
		$img  = $this->getImg();
		if ( is_file( $img . '.webp' ) ) {
			$webP = $img . '.webp';
		}
		elseif ( is_file( str_replace( array( 'png', 'jpg' ), 'webp', $img ) ) ) {
			$webP = str_replace( array( 'png', 'jpg' ), 'webp', $img );
		} else {
			return null;
		}

		return str_replace( ABSPATH, get_home_url() . '/', $webP );
	}

}