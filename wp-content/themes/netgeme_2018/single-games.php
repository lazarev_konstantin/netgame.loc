<?php get_header();
$header_text = get_post_custom_values('header_text')[0];
if (empty($header_text)) {
    $header_text = 'Доп поле header_text';
}
$header_img_id = get_post_custom_values('header_game_images')[0];

$min_game_logo_id = get_post_custom_values('min_game_logo')[0];
$min_game_logo_src = wp_get_attachment_image_src($min_game_logo_id, 'full')[0];

$header_img_table_id = get_post_custom_values('header_game_images_table')[0];
$header_img_mobile_id = get_post_custom_values('header_game_images_mobile')[0];
$header_img_src = wp_get_attachment_image_src($header_img_id, 'full')[0];
$header_img_table_src = wp_get_attachment_image_src($header_img_table_id, 'full')[0];
$header_img_mobile_src = wp_get_attachment_image_src($header_img_mobile_id, 'full')[0];
$header_img_src_webp = str_replace(array('png', 'jpg'), 'webp', $header_img_src);
$header_img_table_src_webp = str_replace(array('png', 'jpg'), 'webp', $header_img_table_src);
$header_img_mobile_src_webp = str_replace(array('png', 'jpg'), 'webp', $header_img_mobile_src);

$left_button_text = get_post_custom_values('left_button_text')[0];
if (empty($left_button_text)) {
    $left_button_text = 'Доп поле left_button_text';
}
$left_button_href = get_post_custom_values('left_button_href')[0];
if (empty($left_button_href)) {
    $left_button_href = 'Доп поле left_button_href';
}
$right_button_text = get_post_custom_values('right_button_text')[0];
if (empty($right_button_text)) {
    $right_button_text = 'Доп поле right_button_text';
}
$right_button_href = get_post_custom_values('right_button_href')[0];
if (empty($right_button_href)) {
    $right_button_href = 'Доп поле right_button_href';
}
$reels = get_post_custom_values('reels')[0];
if (empty($reels)) {
    $reels = 'Доп поле reels';
}
$lines = get_post_custom_values('lines')[0];
if (empty($lines)) {
    $lines = 'Доп поле lines';
}
$max_win = get_post_custom_values('max_win')[0];
if (empty($max_win)) {
    $max_win = 'Доп поле max_win';
}
$volatility = get_post_custom_values('volatility')[0];
if (empty($volatility)) {
    $volatility = 'Доп поле volatility';
}
$bonus_games = get_post_custom_values('bonus_games')[0];
if (empty($bonus_games)) {
    $bonus_games = 'Доп поле bonus_games';
}


$rtp = get_post_custom_values('rtp')[0];
if (empty($rtp)) {
    $rtp = 'Доп поле rtp';
}
$name = get_post_custom_values('name')[0];
if (empty($name)) {
    $name = 'Доп поле name';
}
$game_type = get_post_custom_values('game_type')[0];
if (empty($game_type)) {
    $game_type = 'Доп поле game_type';
}
$realeased = date('F, Y', strtotime(get_post_custom_values('realeased')[0]));
if (empty($realeased)) {
    $realeased = 'Доп поле realeased';
}
$goes = get_post_custom_values('goes')[0];
if (empty($goes)) {
    $goes = 'Доп поле goes';
}
$technologies = get_post_custom_values('technologies')[0];
if (empty($technologies)) {
    $technologies = 'Доп поле technologies';
}
$gameplay_features = get_post_custom_values('gameplay_features')[0];
if (empty($gameplay_features)) {
    $gameplay_features = 'Доп поле gameplay_features';
}
$slot_type = get_post_custom_values('slot_type')[0];
if (empty($slot_type)) {
    $slot_type = 'Доп поле slot_type';
}
$symbols = get_post_custom_values('symbols')[0];
if (empty($symbols)) {
    $symbols = 'Доп поле symbols';
}
$hit_frequency = get_post_custom_values('hit_frequency')[0];
if (empty($hit_frequency)) {
    $hit_frequency = 'Доп поле hit_frequency';
}
$bonus_feature_hit_rate = get_post_custom_values('bonus_feature_hit_rate')[0];
if (empty($bonus_feature_hit_rate)) {
    $bonus_feature_hit_rate = 'Доп поле bonus_feature_hit_rate';
}
$maximum_feature_exposure = get_post_custom_values('maximum_feature_exposure')[0];
if (empty($maximum_feature_exposure)) {
    $maximum_feature_exposure = 'Доп поле maximum_feature_exposure';
}

$FullHD = null;
if (has_tag('fullhd')) {
    $FullHD = 'decor-FullHD';
}
/**
 * ALT изображения по его id ($thumb_id) get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
 */

$internalErrors = libxml_use_internal_errors(true);
$iframe_all = riverslot(get_post_custom_values('href_run_demo')[0]);
$GameSrcForMobile = null;
preg_match_all('/(img|src)=("|\')[^"\'>]+/i', $iframe_all, $media);
unset($data);
$data = preg_replace('/(img|src)("|\'|="|=\')(.*)/i', "$3", $media[0]);
if (isset($data[0])) {
    $GameSrcForMobile = $data[0];
}
if (empty($GameSrcForMobile)) {
    $GameSrcForMobile = "empty game";
}
?>
    <div class="bl_game js-mobSrc" data-mob-src="<?php echo $GameSrcForMobile ?>">

        <div class="bl_game__videoWrapper hidden">
            <div class="bl_video">
                <button class="btn_closeVideo" type="button" aria-label="button close video"></button>
            </div>
        </div>


        <div class="bl_game__wrapper">

            <picture class="bl_game__bigScreen">
                <!--<source media="(max-width: 450px)" srcset="image/mainImage-slot_min.webp" type="image/webp">-->
                <source media="(max-width: 450px)" srcset="<?php echo $header_img_mobile_src ?>">
                <!--<source media="(max-width: 991px" srcset="image/mainImage-slot_middle.webp" type="image/webp">-->
                <source media="(max-width: 991px)" srcset="<?php echo $header_img_table_src ?>">
                <!--<source media="(min-width: 1200px)" srcset="image/mainImage-slot.webp" type="image/webp">-->
                <source media="(min-width: 1200px)" srcset="<?php echo $header_img_src ?>">
                <img src="<?php echo $header_img_src ?>" alt="<?php echo the_title() ?>">
            </picture>


            <div class="bl_game__wrapper-inner">

                <picture class="bl_game__img">

                    <source media="(max-width: 450px)" srcset="<?php echo $min_game_logo_src ?>">

                    <source media="(min-width: 1200px)"
                            srcset="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0] ?>">
                    <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0] ?>"
                         alt="<?php echo the_title() ?>">
                </picture>

                <div class="bl_game__nav">
                    <h1 class="gamePage_title <?php echo $FullHD ?>"><?php echo the_title() ?></h1>
                    <div class="bl_game__desc"><?php echo $header_text ?></div>

                    <div class="bl_game__buttons">
                        <?php
                        if (!empty(get_post_custom_values('href_run_demo')[0])) { ?>
                            <button class="btn_playGame js-playGame"
                                    data-href="<?php echo get_post_custom_values('href_run_demo')[0] ?>" type="button"
                                    aria-label="play demo game of <?php echo the_title() ?>">Run Demo</button>
                        <?php }
                        if (!empty(get_post_custom_values('href_play_video')[0])) {
                            ?>
                            <button class="btn_watchVideo"
                                    data-href="<?php echo get_post_custom_values('href_play_video')[0] ?>" type="button"
                                    aria-label="play video of <?php echo the_title() ?>">
                                Play Video
                            </button>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="bl_game__info_full">
            <div class="bl_game__info">

                <div class="bl_game__info-left">

                    <?php
                    $slider_game_images = get_post_custom_values('slider_game_images')[0];
                    $slider_game_images = json_decode($slider_game_images);
                    $slider_game_images = (array)$slider_game_images;

                    if (!empty($slider_game_images)) { ?>
                        <div class="bl_game__screens">
                            <p class="bl_game__topic">features</p>


                            <?php

                            $first_slide_big = $slider_game_images['big'][0];


                            if (isset($first_slide_big)) { ?>
                                <figure class="bl_game__screens_photo">
                                    <img src="<?php echo wp_get_attachment_image_src($first_slide_big, 'full')[0] ?>"
                                         alt="<?php echo get_post_meta($first_slide_big, '_wp_attachment_image_alt', true) ?>"/>
                                    <figcaption><?php echo get_post_meta($first_slide_big, '_wp_attachment_image_alt', true) ?></figcaption>
                                </figure>
                                <div class="game_screens">
                                    <?php for ($m = 0; $m < count($slider_game_images['big']); $m++) {

                                        $id_img = $slider_game_images['big'][$m];
                                        if (empty($id_img)) continue;
                                        ?>

                                        <img class="game_screens__img"
                                             src="<?php echo wp_get_attachment_image_src($id_img, 'medium')[0] ?>"
                                             data-screen="<?php echo($m) ?>"
                                             alt="<?php echo get_post_meta($id_img, '_wp_attachment_image_alt', true) ?>">
                                    <?php } ?>
                                </div>
                            <?php } ?>


                        </div>
                    <?php }
                    ?>

                    <div class="bl_game__aboutGame">
                        <p class="bl_game__topic">about game</p>
                        <div class="bl_game__description">
                            <?php echo get_post()->post_content ?>
                        </div>
                    </div>

                </div>
                <div class="bl_game__info-right">
                    <div class="bl_game__fullInformation">
                        <div class="bl_game__generalInfo">
                            <p class="bl_game__topic">general information</p>
                            <ul class="bl_game__list">
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Name</span>
                                    <span class="bl_game__val"><?php echo the_title() ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Game type</span>
                                    <span class="bl_game__val"><?php echo $game_type ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Realeased on</span>
                                    <span class="bl_game__val"><?php echo $realeased ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Goes on</span>
                                    <span class="bl_game__val"><?php echo $goes ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Technologies</span>
                                    <span class="bl_game__val"><?php echo $technologies ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Gameplay Features</span>
                                    <span class="bl_game__val"><?php echo $gameplay_features ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="bl_game__gameplay">
                            <p class="bl_game__topic">gameplay</p>
                            <ul class="bl_game__list">
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Slot type</span>
                                    <span class="bl_game__val"><?php echo $slot_type ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Paylines</span>
                                    <span class="bl_game__val"><?php echo $lines ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Reels</span>
                                    <span class="bl_game__val"><?php echo $reels ?></span>
                                </li>

                                <li class="bl_game__item">
                                    <span class="bl_game__param">Maximum Win Base Game</span>
                                    <span class="bl_game__val"><?php echo $max_win ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Volatility</span>
                                    <span class="bl_game__val"><?php echo $volatility ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Symbols</span>
                                    <span class="bl_game__val"><?php echo $symbols ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Bonus Feature</span>
                                    <span class="bl_game__val"><?php echo $bonus_games ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Hit Frequency</span>
                                    <span class="bl_game__val"><?php echo $hit_frequency ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">RTP</span>
                                    <span class="bl_game__val"><?php echo $rtp ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Bonus Feature Hit Rate</span>
                                    <span class="bl_game__val"><?php echo $bonus_feature_hit_rate ?></span>
                                </li>
                                <li class="bl_game__item">
                                    <span class="bl_game__param">Maximum Feature Exposure</span>
                                    <span class="bl_game__val"><?php echo $maximum_feature_exposure ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_sidebar('contacts');
get_footer();


