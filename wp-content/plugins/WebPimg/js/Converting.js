jQuery(document).ready(function ($) {
    $('.ConvertingImg').on("click", function () {
        $('#ConvertingRez').empty();
        var folders = [];
        $('.folders').each(function () {
            if ($(this).prop('checked')) {
                folders.push($(this).val());
            }
        });
        var copies_not_convert;
        if ($('.copies_not_convert').prop('checked')) {
            copies_not_convert = 'not_convert';
        } else {
            copies_not_convert = '';
        }
        var add_type_img_in_name;
        if ($('.add_type_img_in_name').prop('checked')) {
            add_type_img_in_name = 1;
        } else {
            add_type_img_in_name = 0;
        }

        var ajaxscript = {ajax_url: '/wp-admin/admin-ajax.php'};
        $.ajax({
            url: ajaxscript.ajax_url,
            data: {
                action: 'find_img_and_convert_img',
                folders: folders,
                copies_not_convert: copies_not_convert,
                add_type_img_in_name: add_type_img_in_name
            },
            method: 'POST',
            beforeSend: function () {
                $(".wait").show();
                $('#ConvertingRez').empty();
            },
            success: function (response) {
                $('#ConvertingRez').empty();
                if (response == 'OK') {
                    $('#ConvertingRez').html('<p class="bl_webPImg__textSuccess">Конвертирование завершено! Иди, и наслаждайся своими webP картинками</p>');
                } else {
                    $('#ConvertingRez').html('<p class="bl_webPImg__textError">Не удалось дождаться позитивного ответа :( Запустите ещё раз, если не поможет, зови разработчиков!</p>');
                }
            },
            error: function (error) {
                console.log(error)
            },
            complete: function () {
                $(".wait").hide();
            }
        })
    });


    $('.btn_deleteAllWebP').on("click", function () {
        $('#ConvertingRez').empty();
        var ajaxscript = {ajax_url: '/wp-admin/admin-ajax.php'};
        $.ajax({
            url: ajaxscript.ajax_url,
            data: {
                action: 'delate_img_in_webp'
            },
            method: 'POST',
            beforeSend: function () {
                $('.Delatewait').show();
                $('#ConvertingRez').empty();
            },
            success: function (response) {
                if (response.success) {
                    $('#ConvertingRez').html('<p class="bl_webPImg__textSuccess">Все картинки webP удалены </p>');

                } else {
                    $('#ConvertingRez').html('<p class="bl_webPImg__textError">Не удалось дождаться позитивного ответа :( Запустите ещё раз, если не поможет, зови разработчиков!</p>');
                }
            },
            error: function (error) {
                console.log(error)
            },
            complete: function () {
                $(".Delatewait").hide();
            }
        })
    });
});


