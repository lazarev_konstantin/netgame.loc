<?php
/*
Plugin Name: WebPimg
Text Domain: WebPimg
Description: Converting folder images
Version: 2.0
*/


function delate_img_in_webp( $folder = null ) {
	if ( empty( $folder ) ) {
		$folder = $_SERVER["DOCUMENT_ROOT"] . '/' . 'wp-content';
	}
	$arr_format = [ 'image/png', 'image/jpeg' ];
	foreach ( array_diff( scandir( $folder ), array( '..', '.' ) ) as $key => $value ) {
		$file = $folder . '/' . $value;
		if ( is_dir( $file ) ) {
			delate_img_in_webp( $file );
		} else {
			if ( ! in_array( mime_content_type( $file ), $arr_format ) ) {
				if ( preg_match( '/.webp$/i', $file ) ) {
					unlink( $file );
				}
			}
		}
	}
}

function delate_img_in_webp_callback() {
	delate_img_in_webp();
	wp_send_json_success();
	wp_die();
}

add_action( 'wp_ajax_delate_img_in_webp', 'delate_img_in_webp_callback' );

function convert_img_in_webp( $imgName,$add_type_img_in_name ) {
	$arr_format = [ 'image/png', 'image/jpeg' ];
	$add_type_img_in_name = (int)$add_type_img_in_name;

	if ( strstr( $imgName, '.jpg' ) === false && strstr( $imgName, '.png' ) === false ) {
		return false;
	} elseif ( strstr( $imgName, '.jpg.webp' ) === true && strstr( $imgName, '.png.webp' ) === true ) {
		return false;
	} elseif ( ! in_array( mime_content_type( $imgName ), $arr_format ) ) {
		return false;
	} else {
		if ( mime_content_type( $imgName ) == 'image/jpeg' ) {
		    if($add_type_img_in_name==1){
			    $save_as = '.jpg.webp';
            } else {
		        $save_as = '.webp';
            }
			$webPName = str_replace( '.jpg', $save_as, $imgName );
		}
		if ( mime_content_type( $imgName ) == 'image/png' ) {
			if($add_type_img_in_name==1){
				$save_as = '.png.webp';
			} else {
				$save_as = '.webp';
			}
			$webPName = str_replace( '.png', $save_as, $imgName );
		}
		if ( ! empty( $imgName ) && ! empty( $webPName ) ) {
			if ( file_exists( $webPName ) ) {
				return false;
			}
			exec( "cwebp -q 100 " . $imgName . " -o " . $webPName . " " );
//            exec( "convert -colorspace sRGB " . $imgName . " " . $webPName . " " );   \\ Decomitted for OpenServer
		}

		return $webPName;
	}
}

function convert_img( $folder, $not_convert_with_extension,$add_type_img_in_name=1 ) {
	if ( empty( $folder ) ) {
		$folder = wp_get_upload_dir()['basedir'];
	}
	foreach ( array_diff( scandir( $folder ), array( '..', '.' ) ) as $key => $value ) {
		$file = $folder . '/' . $value;
		if ( is_dir( $file ) ) {
			convert_img( $file, $not_convert_with_extension,$add_type_img_in_name );
		} else {
			if ( strstr( $file, '.webp' ) ) {
				continue;
			}
//			if ( strstr( $file, '.png.webp' ) ) {
//				continue;
//			}
			if ( preg_match( '/([0-9]{2,4}+)x([0-9]{2,4}+)\.(jpg|png)+$/si', $file ) == 1 ) {
				if ( $not_convert_with_extension ) {
					continue;
				} else {
					convert_img_in_webp( $file,$add_type_img_in_name);
				}
			} else {
				convert_img_in_webp( $file,$add_type_img_in_name);
			}
		}
	}
}

add_action( 'wp_ajax_find_img_and_convert_img', 'find_img_and_convert_img' );
function find_img_and_convert_img() {
	$rez = 'OK';
	if ( $_POST['copies_not_convert'] == 'not_convert' ) {
		$not_convert_with_extension = true;
	} else {
		$not_convert_with_extension = false;
	}
	if ( $_POST['add_type_img_in_name'] == 1 ) {
		$add_type_img_in_name = 1;
	} else {
		$add_type_img_in_name = 0;
	}
	@set_time_limit( 300 );
	for ( $i = 0; $i < count( $_POST['folders'] ); $i ++ ) {
		$folder = $_POST['folders'][ $i ];
		convert_img( $folder, $not_convert_with_extension,$add_type_img_in_name );
	}
	echo $rez;
	wp_die(); // Необходим для прекращения и возврата ответа
}

function converting_img_to_webp() { ?>
    <div class="bl_webPImg">
        <h1 class="bl_webPImg__title">Конвертируй в webP</h1>
        <p class="bl_webPImg__text_description">Для создания копий jpg и png картинок в формате webP нажмите кнопку
            "Конвертируй webP"</p>
        <div class="bl_webPImg__inputsBlocks">
            <p>*Укажите папки из которых будет производится конвертация</p>
			<?php
			$dir = $_SERVER["DOCUMENT_ROOT"] . '/' . 'wp-content'; ?>
            <label><input type="checkbox" checked disabled class="folders"
                          value="<?php echo $dir . '/' . 'uploads'; ?>">uploads</label>
			<?php
			foreach ( array_diff( scandir( $dir ), array( '..', '.' ) ) as $key => $value ) {
				$file = $dir . '/' . $value;
				if ( is_dir( $file ) ) {
					if ( $value == 'uploads' ) {
						continue;
					}
					?>
                    <label>
                        <input type="checkbox" class="folders"
                               value="<?php echo $file ?>"><?php echo $value ?>
                    </label>
					<?php
				}
			} ?>
        </div>
        <div class="bl_webPImg__corrected">
            <label><input type="checkbox" class="copies_not_convert">Не пережимать копии картинок</label>
        </div>
        <div class="bl_webPImg__corrected">
            <label><input type="checkbox" class="add_type_img_in_name">Добавлять расширение в имя файла (имя будет $file_name.$type.webp)</label>
        </div>
        <div class="bl_half">
            <button class="ConvertingImg" type="submit">Конвертируй webP</button>
        </div>
        <div class="bl_half">
            <button class="btn_deleteAllWebP" type="submit">Удалить все картинки webP</button>
        </div>
    </div>

    <div class="Delatewait" style="display: none">
        <img class="bl_webPImg__loader_img" src=" <?php echo plugins_url(); ?>/WebPimg/img/giphy.gif" alt="webPImg preloader">
        <div class="bl_webPImg__loader_text">Не закрывай окно, пока мы не удалим все webP</div>
    </div>

    <div class="wait" style="display: none">
        <img class="bl_webPImg__loader_img" src=" <?php echo plugins_url(); ?>/WebPimg/img/webPImg_preloader.gif "
             alt="webPImg preloader">
        <div class="bl_webPImg__loader">
            <span>l</span>
            <span>o</span>
            <span>a</span>
            <span>d</span>
            <span>i</span>
            <span>n</span>
            <span>g</span>
        </div>
        <div class="bl_webPImg__loader_text">
            Не закрывай окно, мы перерисовываем твои картинки!
        </div>
    </div>
    <div id="ConvertingRez"></div>
	<?php
}

add_action( 'admin_menu', function () {
	add_menu_page( 'Конвертер картинок в webP', 'Конвертер картинок в webP', 7, 'converting_img_to_webp', 'converting_img_to_webp', plugins_url( 'ico.png', __FILE__ ) );
} );

add_action( 'admin_enqueue_scripts', function () {
	wp_register_script( 'jquery', plugins_url( 'js/jquery.js',__FILE__ ));
	wp_deregister_script( 'jquery-ui' );
	wp_register_script( 'jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js');
	wp_enqueue_script( 'jquery-ui' );
//
//
//	wp_register_script( 'ConvertingJquery', plugins_url( 'js/jquery.js', __FILE__ ), array('jquery'), false, true );
//	wp_enqueue_script( 'ConvertingJquery' );
	wp_register_script( 'Converting', plugins_url( 'js/Converting.js', __FILE__ ), array('jquery','jquery-ui'), false, true );
	wp_enqueue_script( 'Converting' );
	wp_register_style( 'Converting', plugins_url( 'css/Styles.css', __FILE__ ) );
	wp_enqueue_style( 'Converting' );
} );


