<?php
/*
Plugin Name: Sort All Entries
Text Domain: Sort All Entries
Description: The DiJust plugin's Sort All Entries
Version: 1.0
*/


add_action( 'admin_enqueue_scripts', function () {
	wp_register_style( 'SortAllEntries', plugins_url( 'css/Styles.css', __FILE__ ) );
	wp_enqueue_style( 'SortAllEntries' );
	wp_register_style( 'jquery-ui', plugins_url( 'css/jquery-ui.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-ui' );

	wp_register_script( 'jquery', plugins_url( 'js/jquery.js', __FILE__ ) );
	wp_enqueue_script( 'jquery' );
	wp_register_script( 'jquery-ui', plugins_url( 'js/jquery-ui.js', __FILE__ ), array( 'jquery' ), false, true );
	wp_enqueue_script( 'jquery-ui' );
	wp_register_script( 'SortAllEntries', plugins_url( '/js/SortAllEntries.js', __FILE__ ), array( 'jquery-ui' ), false, true );
	wp_enqueue_script( 'SortAllEntries' );

} );


register_uninstall_hook( __FILE__, 'SortAllEntriesUuninstall' );
register_activation_hook( __FILE__, 'SortAllEntriesInstall' );

//добавление дополнительного поля сортировки для всех записей
function SortAllEntriesInstall() {
	$query = new WP_Query( array(
		'post_type'      => 'games',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
	) );
	if ( $query->have_posts() ) {
		$i = 1;
		while ( $query->have_posts() ) {
			$query->the_post();
			update_post_meta( get_the_ID(), 'sort_place', $i );
			$i ++;
		}
	}
}

//удаление дополнительного поля сортировки для всех записей
function SortAllEntriesUuninstall() {
	global $wpdb;
	$meta_key = 'sort_place'; // название мета-ключа который будем удалять
	$wpdb->query(
		$wpdb->prepare( "DELETE FROM $wpdb->postmeta WHERE meta_key = %s", $meta_key )
	);
}

add_action( 'admin_menu', function () {
	add_menu_page( 'Порядок сортировки', 'Порядок сортировки', 7, 'SortAllEntries', 'sort_all_entries', '
dashicons-randomize' );
} );

function sort_all_entries() {
	if ( ! empty( $_POST['SortElementPlace'] ) ) {
		foreach ( $_POST['SortElementPlace'] as $place => $post_id ) {
			update_post_meta( $post_id, 'sort_place', $place + 1 );
		}
	}

	$sortedList = getSortedList();
	?>
    <p class="readMe">Получение масива с ID записей с назначеным порядком необходимо вызвать функцию getSortedList()</p>
    <div class="bl_SAE">
    <form method="post">
        <ol class="BodySortAllEntries">
			<?php
			foreach ( $sortedList as $id ) { ?>
                <li class="SortElement">
					<?php echo get_the_title( $id ) ?>
                    <input type="hidden" name="SortElementPlace[]" value="<?php echo $id ?>">
                </li>
				<?php
			}
			$query = new WP_Query( array(
				'post_type'      => 'games',
				'posts_per_page' => - 1,
				'post_status'    => 'publish',
			) );
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					if ( in_array( get_the_ID(), $sortedList ) ) {
						continue;
					}
					?>
                    <li class="SortElement">
						<?php echo get_the_title() ?>
                        <input type="hidden" name="SortElementPlace[]" value="<?php echo get_the_ID() ?>">
                    </li>
				<?php }
			} ?>
        </ol>
        <button class="btn_saveSEP" type="submit">Сохранить</button>
    </form>
    </div>
	<?php wp_reset_query();
}


/**
 * Возвращает масив ID записей
 *
 * @return array
 */
function getSortedList() {
	$id    = [];
	$query = new WP_Query( array(
		'post_type'      => 'any',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
		'meta_query'     => array(
			array(
				'key' => 'sort_place',
			)
		),
		'meta_key'       => 'sort_place',
		'orderby'        => 'meta_value_num',
		'order'          => 'ASC'
	) );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id[] = get_the_ID();
		}
	}

	return $id;
}

