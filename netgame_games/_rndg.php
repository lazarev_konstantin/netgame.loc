<?php

//define('CASINO_URL', 'https://adm.html-srv.com');    // 
define('CASINO_URL', 'http://expo.servupdate.com'); // 
define('CASINO_ID', '8c5023ea-f023-49e5-badd-280450783ee1'); //Новый Казино айди
define('CASINO_SECRET', '6zHjjaCKboKDtpt2kGFKYG3T9b_emJtKPrjmdMF2TwIXkOP1JGvd4U0tg4gQi0mV'); //новый сикрет

class RiverLauncher {
    const COOKIE_UUID_KEY = '__river_uuid';

    private $url;
    private $id;
    private $secret;

    public function __construct($url = CASINO_URL, $id = CASINO_ID, $secret = CASINO_SECRET){
        $this->url = rtrim($url, '/');
        $this->id = $id;
        $this->secret = $secret;
    }

    public function getGameCode(){
        $query = http_build_query(array('uuid' => $this->getUuid(), 'partner' => 968574));
        $url = sprintf('%s/api/demo/?%s', $this->url, $query);

        $response = @file_get_contents($url);
        $json = $this->decode($response);

        //var_dump($json); exit;

        return $json->data->code;
    }

    public function getGameUrl($gameId){
        if((int)$gameId == 0){
            throw new Exception('RiverLauncher::getGameUrl() error. GameID is not valid.');
        }

        $packet = array('casino_id' => $this->id, 'game' => $gameId);

        $response = $this->call($this->sign($packet), $packet);
        $json = $this->decode($response);

        return $json->launch_game;
    }

    private function decode($response){
        if(empty($response)){
            throw new Exception('RiverLauncher::decode() error. Empty response.');
        }

        $json = json_decode($response);
        if(json_last_error() != JSON_ERROR_NONE){
            throw new Exception(json_last_error_msg(), json_last_error());
        }

        return $json;
    }

    private function call($sign, $packet){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url . '/online/demo/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-REQUEST-SIGN: {$sign}"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($packet));
        $result = curl_exec($ch);
        curl_close($ch);

        //var_dump($result); exit;

        return $result;
    }

    private function sign($packet){
        $packet = is_array($packet) ? json_encode($packet) : $packet;
        return hash_hmac('sha256', $packet, $this->secret);
    }

    private function getUuid(){
        if(empty($_COOKIE[self::COOKIE_UUID_KEY])){
            $uuid = uniqid('');
            setcookie(self::COOKIE_UUID_KEY, $uuid, time() + 60 * 60 * 24 * 30, '/', "", false, true);
            return $uuid;
        }
        return $_COOKIE[self::COOKIE_UUID_KEY];
    }
}

function isMobile(){
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
}

function riverslot($game){
    $slot_arr_riverslot = array(
        'golden-sculls'		    =>	'176',
        'diamond-shot'              =>	'216',
        'jungle-2'                  =>	'235',
        'magic-dragons'             =>	'189',
        'magic-tree'                =>	'248',
        'dragon-sevens'             =>	'257',
        'space-rocks-2'             =>	'277',
        'fortune-cash'              =>	'174',
        'african-king'              =>	'254',
        'thunder-strike'             =>	'256',
        'crazy-scientist'           =>	'',
        'red-hot-chili-sevens'      =>	'258',
        'golden-fruits'             =>	'146',
        'disco-fruits'              =>	'121',
        'wolf-reels'                =>	'241',
        'hit-in-vegas'               =>	'247',
        'frosty-fruits'             => '289',
        'cleo-s-heart'               =>  '273',
        'fruits-fury'               =>  '261',
        'bananas'                   =>  '329',
        'frosty-fruits'             =>  '289'
        );

    foreach ($slot_arr_riverslot as $game_v => $urln){
        if ($game == $game_v){	
            //$url = (new RiverLauncher())->getGameUrl($urln); // FUN Currency
            $code = (new RiverLauncher())->getGameCode();
            $quality = isMobile() ? 'hd' : 'high';
            //$url = "https://games-cdn.html-srv.com/enter.html?game={$urln}&userId={$code}&wshost=wss://ws.html-srv.com:9443&quality={$quality}&lang=en";
            //$url = "http://games-cdn.html-srv.com/enter.html?game={$urln}&userId={$code}&wshost=ws://linux-dev.servupdate.com:38087&quality={$quality}&lang=en&noframe=yes";
            $url = "http://html-free.com/enter.html?game={$urln}&userId={$code}&wshost=ws://linux-dev.servupdate.com:38087&quality={$quality}&lang=en&noframe=yes";
            $content = '<iframe frameborder="0" scrolling="no" width="100%" height="100%" src="'.$url.'" vspace="0" hspace="0" marginwidth="0" marginheight="0" seamless allowfullscreen></iframe>';

            return  $content;
        }
    }
}
