<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if ( ! empty( trim( $_GET['game'] ) ) ) {
	include 'rndg.php';
	$game = trim( $_GET['game'] );
	if ( ! empty( riverslot($game) ) ) {
		echo riverslot($game) ;
	} else {
		echo "No Game Named $game in Our DB";
	}
}